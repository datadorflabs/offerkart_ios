//
//  HorizontalCategoryCell.swift
//  OfferKart
//
//  Created by Sajin M on 12/11/2020.
//

import UIKit

protocol MainCategoryDelegate{
    
    func tappedMainCategory(category:mainCategoryData)
}

class HorizontalCategoryCell: UITableViewCell {

    @IBOutlet weak var horizontalCategory: UICollectionView!
    
    @IBOutlet weak var lblTitle: UILabel!
    var delegate:MainCategoryDelegate?
    
    var title:String?{
        didSet{
            
            self.lblTitle.text = title?.uppercased()
        }
        
    }
    
    var categoryData:[mainCategoryData]?{
        
        didSet{
            self.horizontalCategory.delegate = self
            self.horizontalCategory.dataSource = self
            self.horizontalCategory.reloadData()
            
        }
    }
    

}
extension HorizontalCategoryCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categoryData?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: horizontalMainCell, for: indexPath) as! HorizontalCategoryCollectionCell
        
        
        if let info = self.categoryData?[indexPath.row]{
            
            if let imgUrl = info.categoryImage{
                
                let url = URL(string: imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? null)
                let phImage = UIImage(named: ph)
                
                cell.imgCategory?.kf.setImage(with: url, placeholder: phImage)
                cell.lblTitle.text = info.mainCategory
            }
        
        
    }
        
        return cell
    
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: 100.0, height: self.horizontalCategory.frame.size.height / 2) 
            }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        if let category = categoryData?[indexPath.row]{
            
            delegate?.tappedMainCategory(category: category)
        }
        
        
    }
    
}
