//
//  SubCategoryCell.swift
//  Nesto OMAN
//
//  Created by Sajin M on 28/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit


protocol SubcategoryProductDelegate {
    
    func getProductDetails(product:Product)
    func loadMoreProduts()
}

class SubCategoryCell: UICollectionViewCell {
    
    
    @IBOutlet weak var productListCollectionView: UICollectionView!
    
    var delegate:SubcategoryProductDelegate?
    var totalProductCount:String?
    var isLoadMore:Bool = true
    
    var productData:[Product]?{
        
        didSet{
            
           // print(productData)
            productListCollectionView.delegate = self
            productListCollectionView.dataSource = self
            productListCollectionView.reloadData()
            if !isLoadMore{
                
                productListCollectionView.setContentOffset(.zero, animated: false)
            }
            
           // productListCollectionView.setContentOffset(.zero, animated: false)
            
        }
        
    }
    
    
   
    
    
}
extension SubCategoryCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
       
        return self.productData?.count ?? 0
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
 
        if let productData = self.productData?[indexPath.row]{
                        
            delegate?.getProductDetails(product: productData)
            
        }
        

    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: horizontalCollectionCell, for: indexPath) as! HorizontalCollectionCell
            
        
            
            if let info = self.productData?[indexPath.row]{
                
                if let title = info.productName{
                    
                    cell.lblProductName.text = title
                    
                }
                

                
                if let weightType = info.weight{
                    
                    if let weightQty = info.qtyWeight{
                        
                        cell.lblWeight.text = weightQty + null + weightType
                    }
                    
                }
                
                
                
                if let off = info.offerPrice {
                    
                    if Double(off)! > 0.0{
                        
                        if let offer = info.offerPercentage{
                            
                            cell.lblOff.text = offer + "%"
                        }
                        
                        
                        
                        
                        cell.lblOff.isHidden = false
                        cell.offerTagView.isHidden = false
                        cell.lblPrice.isHidden = false
                        

                        if let price = info.price {
                                        
                                        let priceStr =  currency + price
                                        
                                         cell.lblPrice.attributedText = priceStr.strikeThrough()
                                        
                                    }
                        
                        if let offerPrice = info.offerPrice{
                                           
                                           cell.lblOfferPrice.text =  currency + offerPrice
                                           
                                       }
                        
                    }else{
                        
                        
                        cell.lblOff.isHidden = true
                        cell.offerTagView.isHidden = true
                        cell.lblPrice.isHidden = true
                        
                       
                        if let offerPrice = info.offerPrice{
                            
                            if Double(offerPrice)! > 0.0{
                                
                                cell.lblPrice.isHidden = false
                            
                            if let price = info.price{
                                
                                let priceStr =  currency + price
                                
                                cell.lblPrice.attributedText = priceStr.strikeThrough()
                                cell.lblOfferPrice.text = currency + offerPrice
                                
                            }
                                
                            }else{
                                
                                cell.lblPrice.isHidden = true
                                
                                if let price = info.price{
                                
                                    cell.lblOfferPrice.text = currency + price
                                }else{
                                    
                                    if let price = info.groceryProductPrice{
                                        
                                        cell.lblOfferPrice.text = currency + price
                                        
                                    }
                                    
                                }
                                
                                
                            }
                            
                        }else{
                            
                            if let price = info.price{
                                
                                cell.lblOfferPrice.text = currency + price
                            }
                            
                            
                            
                        }
                        
                  
                    }
                    
                    
                }else{
                    
                    
                    
                    if let offerPrice = info.offerPrice{
                        
                        if let price = info.price{
                            
                            let priceStr =  currency + price
                            
                            cell.lblPrice.attributedText = priceStr.strikeThrough()
                            cell.lblOfferPrice.text = currency + offerPrice
                            
                        }
                        
                    }else{
                        
                        if let price = info.price{
                            
                            cell.lblOfferPrice.text = currency + price
                        }
                        
                        cell.offerTagView.isHidden = true
                        cell.offerTagView.isHidden = true
                        
                    }
                    
                    
                }
                

                

                
                if let imgUrl = info.productImage{
                    
                    let url = URL(string: imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? null)
                     let phImage = UIImage(named: ph)
                    
                     cell.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                    
                    
                }
                
                
            }
        
        if indexPath.row == ((productData?.count ?? 0 )/2) - 1{
            
            if let total = Int(self.totalProductCount ?? "0"){
                
                if productData!.count < total {
                    
                     delegate?.loadMoreProduts()
                }
                
            }
      
            
            
        }
             
       
     return cell
            
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
               
              
               
               let width = (collectionView.frame.size.width - 5 * 2) / 2
        let height = width * 1.35 //ratio
               return CGSize(width: width, height: height)
               
           
    }
    
    
    
    
}
