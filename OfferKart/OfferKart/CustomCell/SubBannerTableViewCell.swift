//
//  SubBannerTableViewCell.swift
//  OfferKart
//
//  Created by Sajin M on 16/11/2020.
//

import UIKit
import SwiftyGif

protocol subBannerDelegate {
    
    func tappedSubBanner(data:SubBannersData)
}

class SubBannerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgBanner: UIImageView!
    
    var bannerData:SubBannersData?{
        
        didSet{
            setBanners()
            
        }
        
    }
    
    var delegate:subBannerDelegate?
    var index:Int?
    var isCampaign:Int?
    
   
    
    @IBAction func bannerTapped(_ sender: Any) {
        
   
            
            if let banner = self.bannerData{
                
                delegate?.tappedSubBanner(data: banner)
            }
            
      
    }
    

    
    func setBanners(){
        
        if let imgUrl = bannerData?.data {
            
            
            let url = URL(string: imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? null)
            let phImage = UIImage(named: ph)
            self.imgBanner?.contentMode = .redraw
            
            if let url = url {
                
               // self.imgBanner?.setGifFromURL(url, customLoader: nil)
                
                self.imgBanner?.kf.setImage(with: url, placeholder: phImage)
            }
           
            
        }
        
        
    }

}
