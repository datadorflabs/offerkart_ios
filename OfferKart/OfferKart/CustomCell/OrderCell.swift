//
//  OrderCell.swift
//  Nesto OMAN
//
//  Created by Sajin M on 03/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import UIKit
import BadgeSwift

class OrderCell: UITableViewCell {
    
    
    @IBOutlet weak var lblOrderTitle: UILabel!
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblOrderAmount: UILabel!
    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var lblOrderCount: BadgeSwift!
    
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
