//
//  BottomCategoryCell.swift
//  OfferKart
//
//  Created by Sajin M on 17/01/2021.
//

import UIKit

protocol BottomCategoryDelegate{
    
    func tappedCategory(catId:String,brandUrl:String,name:String)
}

class BottomCategoryCell: UITableViewCell {

    @IBOutlet weak var horizontalCollectionView: UICollectionView!

    
    var index:Int?
    var delegate:BottomCategoryDelegate?
   
    
    var subCatData:SubBottomData?{
        didSet{
            
            self.horizontalCollectionView.delegate = self
            self.horizontalCollectionView.dataSource = self
            self.horizontalCollectionView.reloadData()
            
        }
        
    }

}

extension BottomCategoryCell:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
       
        
        if let count = self.subCatData?.data?.count{
            
            return count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BrandCell, for: indexPath) as! HorizontalBrandCell
        
        
        if let info = self.subCatData?.data?[indexPath.row]{
            
            if let imgUrl = info.subCategoryImage{
                
                let url = URL(string: imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? null)
                let phImage = UIImage(named: ph)
                
                cell.imgBrand?.kf.setImage(with: url, placeholder: phImage)
               
            }
        
        
    }
        
        return cell
    
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        if let brandCode = self.subCatData?.data?[indexPath.row].subCategoryId{
            
            delegate?.tappedCategory(catId:brandCode,brandUrl:subCatData?.data?[indexPath.row].subCategoryImage ?? null,name:subCatData?.data?[indexPath.row].subCategoryName ?? null )
        }
        
        
    }
    
}
