//
//  BrandTableCell.swift
//  OfferKart
//
//  Created by Sajin M on 13/01/2021.
//

import UIKit

protocol BrandDelegate{
    
    func tappedBrand(brandCode:String,brandUrl:String,name:String)
}

class BrandTableCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var horizontalCollectionView: UICollectionView!

    
    var index:Int?
    var delegate:BrandDelegate?
    var title:String?{
        didSet{
            
            self.lblTitle.text = title?.uppercased()
            
        }
    }
    
    var brandData:Brands?{
        didSet{
            
        
            self.horizontalCollectionView.delegate = self
            self.horizontalCollectionView.dataSource = self
            self.horizontalCollectionView.reloadData()
            
        }
        
    }
   

}


extension BrandTableCell:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
       
        
        if let count = self.brandData?.data?.count{
            
            return count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BrandCell, for: indexPath) as! HorizontalBrandCell
        
        
        if let info = self.brandData?.data?[indexPath.row]{
            
            if let imgUrl = info.brandLogo{
                
                let url = URL(string: imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? null)
                let phImage = UIImage(named: ph)
                
                cell.imgBrand?.kf.setImage(with: url, placeholder: phImage)
               
            }
        
        
    }
        
        return cell
    
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        if let brandCode = brandData?.data?[indexPath.row].brandCode{
            
            delegate?.tappedBrand(brandCode:brandCode,brandUrl:brandData?.data?[indexPath.row].brandBanner ?? null,name:brandData?.data?[indexPath.row].brandName ?? null )
        }
        
        
    }
    
}
