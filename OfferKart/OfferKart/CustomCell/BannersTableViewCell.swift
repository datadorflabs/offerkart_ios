//
//  BannersTableViewCell.swift
//  OfferKart
//
//  Created by Sajin M on 08/11/2020.
//

import UIKit
import FSPagerView
import Kingfisher

protocol PagerBannerDelegate {
    
    func bannerTapped(data:BannerData)
}

class BannersTableViewCell: UITableViewCell {
    
    @IBOutlet weak var bannerView: FSPagerView!{
        didSet {
            self.bannerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: bannerCell)
        }
    }
    
    var delegate:PagerBannerDelegate?
    var banners:Banners?{
        didSet{
           // KingfisherManager.shared.cache.clearCache()
            self.bannerView.delegate = self
            self.bannerView.dataSource = self
            self.bannerView.reloadData()

            
        }
        
    }
    

    
}

extension BannersTableViewCell: FSPagerViewDelegate,FSPagerViewDataSource {
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.banners?.data?.count ?? 0
    }
    
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        
        if let info = self.banners?.data?[index]{
            
            delegate?.bannerTapped(data: info)
            
        }
        
    }
  
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        
        
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: bannerCell, at: index)
        
       
      
        if let info = self.banners?.data?[index]{
            
            if let imgUrl = info.bannerImage {
                
                
                let url = URL(string: imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? null)
                let phImage = UIImage(named: ph)
                cell.imageView?.contentMode = .scaleAspectFill
                cell.imageView?.layer.cornerRadius = 4
                cell.imageView?.kf.setImage(with: url, placeholder: phImage, options: [.transition(.fade(0.2))])
                
            }
            
            
        }
        
        return cell
        
        
    }
    
    
    
}
