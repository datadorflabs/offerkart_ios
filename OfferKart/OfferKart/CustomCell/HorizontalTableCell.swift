//
//  HorizontalTableCell.swift
//  OfferKart
//
//  Created by Sajin M on 08/11/2020.
//

import UIKit
import Kingfisher


protocol HorizontalCellDelegate {
    
    func tappedProduct(product:Product)
    func addProduct(product:Product)
    func viewAllList(at index:Int,isCampaign:Int,id:String,isOffer:Bool,heading:String)
    
}

class HorizontalTableCell: UITableViewCell, HorizontalAddDelegate{
    

    
    @IBOutlet weak var lblTitle: UILabel!
    
    
    @IBOutlet weak var horizontalCollectionView: UICollectionView!
    
    var title:String?{
        didSet{
            
            self.lblTitle.text = title?.uppercased()
            
        }
    }
    
    var delegate:HorizontalCellDelegate?
    var index:Int?
    var subCatId:String?
    var isCampaign:Int?
    var isOffer:Bool = false
   
    
    var productData:[Product]?{
        didSet{
            
            self.horizontalCollectionView.delegate = self
            self.horizontalCollectionView.dataSource = self
            self.horizontalCollectionView.reloadData()
            
        }
        
    }
    
    @IBAction func viewAllPressed(_ sender: Any) {
        
        //print(self.subCatId!)
        
        if let id = self.subCatId {
            
            if let index = self.index{
                
                if let campaign = self.isCampaign{
                    
                    delegate?.viewAllList(at:index,isCampaign: campaign,id: id,isOffer: self.isOffer,heading:title ?? null)
                }
            
            
            }
        }
        
    }
    
    
    
    func addTapped(at index: Int) {
        if let product = self.productData?[index]{
            
            delegate?.addProduct(product: product)
        }
    }
    

    
}

extension HorizontalTableCell:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.productData?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: horizontalCollectionCell, for: indexPath) as! HorizontalCollectionCell
    
        
        if let info = self.productData?[indexPath.row]{
            
            if let title = info.productName{
                
                cell.lblProductName.text = title.uppercased()
                
            }
            
            if let imgUrl = info.productImage{
                
              //  KingfisherManager.shared.cache.clearCache()
                
                let url = URL(string: imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? null)
                let phImage = UIImage(named: ph)
                
                cell.imgProduct?.kf.setImage(with: url, placeholder: phImage, options: [.transition(.fade(0.2))])
                
                
            }
            
            
            if let weightType = info.weight{
                
                if let weightQty = info.qtyWeight{
                    
                    cell.lblWeight.text = weightQty + null + weightType
                }
                
            }
            
            
            if let off = info.offerPrice{
                
                if Double(off)! > 0.0{
                    
                    if let offer = info.offerPercentage{
                        
                        cell.lblOff.text = offer + "%"
                    }
                    
                    
                  
         
                    if let price = info.groceryProductPrice{
                        
                        let priceStr = currency + price
                        
                        cell.lblPrice.attributedText = priceStr.strikeThrough()
                        
                    }
                    
                    if let offerPrice = info.offerPrice{
                        
                        cell.lblOfferPrice.text =  currency + offerPrice
                        
                    }
                    
                    cell.offerTagView.isHidden = false
                    cell.lblOff.isHidden = false
                    cell.lblPrice.isHidden = false
                    
                }else{
                    
                    cell.offerTagView.isHidden = true
                    cell.lblOff.isHidden = true
                    cell.lblPrice.isHidden = true
                    
                     if let price = info.groceryProductPrice{
                    
                    cell.lblOfferPrice.text =  currency + price
                   
                    
                    }
                }
                
                
            }else{
                
                cell.lblOff.isHidden = true
                cell.lblPrice.isHidden = true
                cell.offerTagView.isHidden = true
                if let price = info.groceryProductPrice{
               
               cell.lblOfferPrice.text =  currency + price
              
               
               }
            }
            
            cell.index = indexPath.row
            cell.delegate = self
        
        
        
    }
    return cell
    
    
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let product = productData?[indexPath.row]{
            
            delegate?.tappedProduct(product: product)
            
        }
        
        
    }
    
    
  
    
    
    
}
