//
//  SubCatCollectionCell.swift
//  OfferKart
//
//  Created by Sajin M on 02/12/2020.
//

import UIKit

class SubCatCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProduct: UILabel!
    
    
}
