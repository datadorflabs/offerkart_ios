//
//  HorizontalBrandCell.swift
//  OfferKart
//
//  Created by Sajin M on 12/01/2021.
//

import UIKit

class HorizontalBrandCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imgBrand: UIImageView!
    
}
