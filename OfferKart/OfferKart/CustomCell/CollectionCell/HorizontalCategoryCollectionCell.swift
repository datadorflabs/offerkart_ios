//
//  HorizontalCategoryCollectionCell.swift
//  OfferKart
//
//  Created by Sajin M on 12/11/2020.
//

import UIKit

class HorizontalCategoryCollectionCell: UICollectionViewCell {
    
  
    
    @IBOutlet weak var imgCategory: CurvedImage!
    @IBOutlet weak var lblTitle: UILabel!
    
}
