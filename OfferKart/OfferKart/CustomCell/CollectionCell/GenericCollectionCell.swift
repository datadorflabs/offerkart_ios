//
//  GenericCollectionCell.swift
//  OfferKart
//
//  Created by Sajin M on 30/11/2020.
//

import UIKit

class GenericCollectionCell: UICollectionViewCell {
    
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var bgView: CurvedView!
    
}
