//
//  HorizontalCollectionCell.swift
//  OfferKart
//
//  Created by Sajin M on 12/11/2020.
//

import UIKit

protocol HorizontalAddDelegate {
    func addTapped(at index:Int)
}

class HorizontalCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblOfferPrice: UILabel!
    @IBOutlet weak var imgProduct: CurvedImage!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblOff: UILabel!
    @IBOutlet weak var lblWeight: UILabel!

    @IBOutlet weak var offerStackView: UIStackView!
    @IBOutlet weak var offerTagView: UIImageView!
    
    var product:Product?
    var delegate:HorizontalAddDelegate?
    var index:Int?
    
    @IBAction func tappedAddCart(_ sender: Any) {
        
        guard let index = self.index else{
            return
        }
        
        delegate?.addTapped(at:index)
    }
    
    
}
