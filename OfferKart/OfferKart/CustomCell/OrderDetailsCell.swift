//
//  OrderDetailsCell.swift
//  OfferKart
//
//  Created by Sajin M on 16/01/2021.
//

import UIKit
import JKSteppedProgressBar

class OrderDetailsCell: UITableViewCell {
    
   
    @IBOutlet weak var progressBar: SteppedProgressBar!
    @IBOutlet weak var productListTableview: UITableView!
    @IBOutlet weak var lblShipment: UILabel!
    @IBOutlet weak var lblDelveryDate: UILabel!
    
    
    var inset: UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 10)
    }
    
    @IBOutlet weak var listHeightConstant: NSLayoutConstraint!
    var cartData:[Product]?{
        didSet{
            
            self.productListTableview.delegate = self
            self.productListTableview.dataSource = self
            self.productListTableview.reloadData()
           
            if let count = cartData?.count{
            
                
                listHeightConstant.constant = CGFloat(102 * count)
            }

        }
        
    }
    var deliveryType:String?{
        
        didSet{
            
            configureTitleProgressBar()
        }
    }
    
    var deliveryStatus:String?{
        didSet{
            
            configureTitleProgressBar()
            updateProgress()
        }
    }
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //configureTitleProgressBar()
    }
    
    func configureTitleProgressBar() {
       
       // progressBar.insets = inset
        
        if let method = self.deliveryType{
            
            if method == "3"{
                progressBar.titles = ["Ordered", "Packed", "Ready to collect", "Collected"]
            }else{
                
                if let status = self.deliveryStatus{
                    if status == "4" ||  status == "6"{
                        
                        progressBar.titles = ["Ordered", "Packed", "Out for delivery", "Delivered"]
                        
                    }else if status == "5"{
                        
                        progressBar.titles = ["Ordered", "Cancelled"]
                    }
                }else{
                    
                    progressBar.titles = ["Ordered", "Packed", "Ready for delivery", "Delivered"]
                }
                
               
                
            }
            
        }
        
        
//        progressBar.activeImages = [
//            UIImage(named: "check")!,
//            UIImage(named: "check")!,
//            UIImage(named: "check")!,
//            UIImage(named: "check")!
//
//        ]
  //      progressBar.tintActiveImage = true
        //updateButtons(1)
    }
    
    
    func updateProgress(){
        
        switch self.deliveryStatus{
                
        case "2":
            self.updateButtons(2)
            break
        case "3":
            self.updateButtons(3)
            break
        case "4":
            self.updateButtons(4)
        case "5":
            self.updateButtons(2)
        case "6":
            self.updateButtons(3)

        default:
            self.updateButtons(1)
            
        }
        
        
    }
    
    func updateButtons(_ currentTab: Int) {
       
        progressBar.currentTab = currentTab
       // currentTabLabel.text = "\(currentTab)"
    }

   
}
extension OrderDetailsCell:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return self.cartData?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.productListTableview.dequeueReusableCell(withIdentifier: cartCell) as! CartTableViewCell
        
        
        if let info = self.cartData?[indexPath.row] {
            
            if let title = info.productName{
                
                cell.lblTitle.text = title
                
            }
            
            
           

            
            
            if let quantity = info.qty{
                
                if let price = info.productPrice{
                    
                    let priceValue = Double(price)!.roundToDecimal(3)
                    
                    cell.cellPrice = priceValue
                    
                    cell.lblPrice.text = currency + "\(priceValue)"
                    
                    cell.lblCount.text = "x " + quantity
                    
                    
                    let totalPrice = (Double(price)!.roundToDecimal(3) * Double(quantity)!).roundToDecimal(3)
                    
                    cell.lblTotalAmount.text = currency + "\(totalPrice)"
                    
                   
                
                }
                
                }
            
            
            
            if let weight = info.qtyWeight{

                if let weightType = info.weight{

                    cell.lblWeight.text = weight + " " + weightType

                }


            }
            
//            if indexPath.row == 0{
//                
//                cell.lblMessage.isHidden = false
//            }else{
//                cell.lblMessage.isHidden = true
//                
//            }
            
            
            if let imgUrl = info.productImage{
                
                let url = URL(string: imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? null)
                let phImage = UIImage(named: ph)
                
                cell.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                
                
            }
            
            
            
        }
        
//        cell.cellIndex = indexPath.row
//        cell.delegate = self
        
        return cell
    }
    
    
    
}

