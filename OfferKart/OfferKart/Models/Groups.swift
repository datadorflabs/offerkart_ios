//
//  Groups.swift
//  OfferKart
//
//  Created by Sajin M on 08/11/2020.
//

import Foundation


struct Groups:Codable {
    
    var titleHead:String?
    var data:[groupData]?
    
}

struct groupData:Codable {
    var groupId:String?
    var groupName:String?
    var groupImage:String?
}
