//
//  ProductDetail.swift
//  OfferKart
//
//  Created by Sajin M on 19/11/2020.
//

import Foundation

struct ProductDetail:Codable{
    
    var status:Bool
    var message:String
    var ProductDetails:ProductDetailsData?
    let Colors:[productColor]?
    let Sizes:[productSize]?
    let RelativeProducts:relatedProducts?
}


struct ProductDetailsData:Codable{
    
    let id:String?
    let brandId:String?
    let brandName:String?
    let articleID:String?
    let productName:String?
    let descriptions:String?
    let specification:String?
    let offerPrice:String?
    let offerPercentage:String?
    let qtyWeight:String?
    let weight:String?
    let stockCount:String?
    let productImageArr:[String]?
    let productVideoLink:String?
    let productImageSize:String?
    let productImageColor:String?
    let itemCondition:String?
    let price:String?
    let deliveryOn:String?
    let isExpressDelivery:Bool
    let isFavourite:String?
    

}


struct productColor:Codable{
    
    let productId:String?
    let articleID:String?
    let colorId:String?
    let name:String?
    let code:String?
    let isAvailable:Bool?
    
   
}

struct productSize:Codable {
    
    let productId:String?
    let articleID:String?
    let sizeId:String?
    let name:String?
    let code:String?
    
}

struct relatedProducts:Codable{
    
    let titleHead:String?
    let data:[Product]?
    
}

