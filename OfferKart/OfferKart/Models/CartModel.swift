//
//  CartModel.swift
//  OfferKart
//
//  Created by Sajin M on 01/12/2020.
//

import Foundation

struct CartModel:Codable {
    
    var status:Bool
    var message:String
    var products:[Product]?
    var dataCount:String?
    var minPurchaseAmount:String?
    var deliveryTime:String?
    var isFreshItem:Int?
    var deliveryCharge:String?
    
   
}


