//
//  ProductListModel.swift
//  OfferKart
//
//  Created by Sajin M on 28/12/2020.
//

import Foundation


struct ProductListModel:Codable{
    
    var status:Bool
    var message:String
    var dataCount:String?
    var totalCount:String?
    var totalPages:String?
    var products:[Product]?
    var favouriteProducts:[Product]?
}
