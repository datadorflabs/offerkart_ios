//
//  Product.swift
//  OfferKart
//
//  Created by Sajin M on 08/11/2020.
//

import Foundation

struct Product:Codable{
    
    var productId:String?
    var id:String?
    var brandName:String?
    var articleID:String?
    var productName:String?
    var offerPrice:String?
    var offerPercentage:String?
    var qtyWeight:String?
    var weight:String?
    var descriptions:String?
    var isFavourite:String?
    var productImage:String?
    var itemCondition:String?
    var price:String?
    var groceryProductPrice:String?
    var qty:String?
    var productPrice:String?
    
}
