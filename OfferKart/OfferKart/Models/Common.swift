//
//  Common.swift
//  OfferKart
//
//  Created by Sajin M on 25/12/2020.
//

import Foundation


struct Common:Codable {
    var status:Bool
    var message:String?
    var regId:String?
    var otp:String?
    var customerId:String?
    var discount:String?
    var promoCodeId:String?
}
