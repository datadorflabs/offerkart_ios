//
//  SignUpModel.swift
//  OfferKart
//
//  Created by Sajin M on 14/01/2021.
//

import Foundation

struct SignUpModel:Codable {

    
        var status:Bool
        var message:String
        var customerId:String?
        var verify:Bool
    
    
}
