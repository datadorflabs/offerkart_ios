//
//  SubCategoryModel.swift
//  OfferKart
//
//  Created by Sajin M on 03/12/2020.
//

import Foundation



struct SubCategoryModel:Codable {
    
    var status:Bool
    var message:String
    var banner:banners?
    var groups:group?
    var itemKeys:[String]?
    var deals:dealsData?
    
}


struct banners:Codable{
    
    var titleHead:String?
    var data:String?
    var groups:group?
    var itemKeys:[String]?
 
}

struct group:Codable{
    
    var titleHead:String?
    var data:[groupData]?
    
}


struct dealsData:Codable {
    var titleHead:String?
    var data:[Product]?
}


