//
//  Banners.swift
//  OfferKart
//
//  Created by Sajin M on 08/11/2020.
//

import Foundation


struct Banners:Codable {
    let titleHead:String?
    let data:[BannerData]?
}

struct BannerData:Codable{
    
    let id:String?
    let title:String?
    let isCategoryBanner:String?
    let subCategoryId:String?
    let subCategory:String?
    let ProductID:String?
    let bannerImage:String?
    let campaignId:String?
    let isCampaign:String
    
}


struct SubBannersData:Codable {
    
    let titleHead:String?
    let subCategoryId:String?
    let data:String?
    let isCampaign:Int?
    let campaignId:String?
    
}

struct SubCategoryData:Codable{

    let subCategoryId:String?
    let subCategoryName:String?
    let subCategoryImage:String?
    
}

struct SubBottomData:Codable{
    
    let data:[SubCategoryData]?
}
