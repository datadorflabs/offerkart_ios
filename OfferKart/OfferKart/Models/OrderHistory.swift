//
//  OrderHistory.swift
//  Nesto OMAN
//
//  Created by Sajin M on 03/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation


struct OrderHistory:Codable {
    
    var status:Bool
    var message:String
    var customerHistory:[OrderData]?
    var dataCount:String?
    var totalCount:String?
    
}


struct OrderData:Codable{
    
    
    var orderId:String?
    var orderIdSys:String?
    var totItems:String?
    var groceryId:String?
    var netAmount:String?
    var storeName:String?
    var orderStatus:String?
    var deliveryDate:String?
    
}
