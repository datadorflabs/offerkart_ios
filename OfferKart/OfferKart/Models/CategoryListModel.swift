//
//  CategoryListModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 28/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation

struct CategoryListModel:Codable{
    
    var status:Bool
    var message:String
    var count:Int?
    var arrSubCategory:[CategoryListData]?
    
}

struct CategoryListData:Codable{
    
    
    var id:Int
    var category:String
    
    
}
