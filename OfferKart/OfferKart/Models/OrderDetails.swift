//
//  OrderDetails.swift
//  Nesto OMAN
//
//  Created by Sajin M on 07/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation


struct OrderDetails:Codable {
    

    var status: Bool
    var message: String
    var shipAddress:String?
    var orderId: String?
    var orderStatus:String?
    var dataCount:String?
    var netAmount:String?
    var deliveryCharge:String?
    var orderPromoDiscount:String?
    var paymentMethod:String?
    var deliveryMethod:String?
    var pickingLocation:String?
    var orderDetails:[Product]?
    var deliveryOrderDetails:[orderDetailsData]?
    
}


struct orderDetailsData:Codable{
    
     var deliveryId: String?
     var deliveryStatus:String?
     var deliveryDate:String?
     var products:[Product]?
     
}
     


