//
//  Home.swift
//  OfferKart
//
//  Created by Sajin M on 01/11/2020.
//

import Foundation

struct Home:Codable {
    
    var status:Bool
    var message:String
    var itemKeys:[String]?
    var mainCategory:Category?
    var specialOffers:ProductData?
    var banners:Banners?
    var HomescreenBannerOne:SubBannersData?
    var HomescreenBannerTwo:SubBannersData?
    var HomescreenBannerThree:SubBannersData?
    var productsOne:ProductData?
    var productsTwo:ProductData?
    var productsThree:ProductData?
    var productsFour:ProductData?
    var productsFive:ProductData?
    var productsSix:ProductData?
    var productsNine:ProductData?
    var productsTen:ProductData?
    var campaignsOne:ProductData?
    var campaignsTwo:ProductData?
    var campaignsThree:ProductData?
    var campaignsFour:ProductData?
    var campaignsFive:ProductData?
    var subCategoryBanner:SubBannersData?
    var SubcategoryBottom:SubBottomData?
    var brands:Brands?
    var cartCount:String?
    var callno:String?
    var chat_no:String?
    
}


struct ProductData:Codable {
    var titleHead:String?
    var isCampaign:Int?
    var subCategoryId:String?
    var data:[Product]?
}
