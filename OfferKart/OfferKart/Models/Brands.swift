//
//  Brands.swift
//  OfferKart
//
//  Created by Sajin M on 13/01/2021.
//

import Foundation

struct Brands:Codable {
    
    let titleHead:String?
    let data:[BrandData]?
    
}


struct BrandData:Codable{
    
    let id:String
    let brandName:String?
    let brandCode:String?
    let brandLogo:String?
    let brandBanner:String?
    
}
