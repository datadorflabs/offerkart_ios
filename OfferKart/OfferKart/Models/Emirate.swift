//
//  Emirate.swift
//  OfferKart
//
//  Created by Sajin M on 30/12/2020.
//

import Foundation

struct Emirate:Codable {
    var status:Bool
    var message:String
    var emirates:[emiratesData]?
}
