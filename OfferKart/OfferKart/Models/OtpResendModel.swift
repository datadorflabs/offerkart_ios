//
//  OtpResendModel.swift
//  OfferKart
//
//  Created by Sajin M on 14/01/2021.
//

import Foundation


struct OtpResendModel:Codable {

        var status:Bool
        var otp:String?
        var message:String
  
}
