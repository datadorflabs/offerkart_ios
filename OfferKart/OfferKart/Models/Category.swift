//
//  Category.swift
//  OfferKart
//
//  Created by Sajin M on 11/11/2020.
//

import Foundation

struct Category:Codable {
    
    let titleHead:String?
    let data:[mainCategoryData]?
    
    
}


struct mainCategoryData:Codable{
    
    let id:Int?
    let groceryId:String?
    let mainCategory:String?
    let description:String?
    let categoryImage:String?
}
