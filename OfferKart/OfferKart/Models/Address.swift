//
//  Address.swift
//  OfferKart
//
//  Created by Sajin M on 16/12/2020.
//

import Foundation


struct Address:Codable {
    
    var status:Bool
    var message:String
    var deliverableKm:String?
    var dataCount:String?
    var arrAdress:[Addresses]?

}


struct Addresses:Codable{
    
    let addressId:String?
    let addressTitle:String?
    let customerName:String?
    let houseName:String?
    let houseNoVillaNo:String?
    let landMark:String?
    let customerArea:String?
    let customerMobile:String?
    let latitude:String?
    let longitude:String?
    let isDeliverable:String?
    let customerEmirate:String?
    
}


struct emiratesData:Codable{
    
    let id:String
    let emirate:String?
}
