//
//  Login.swift
//  OfferKart
//
//  Created by Sajin M on 01/12/2020.
//

import Foundation

struct LoginModel:Codable {
    
    var status:Bool
    var message:String
    var customer_details:[CustomerData]?
    var user_exist:Bool
    
}

struct CustomerData:Codable {
    
    var id:Int?
    var firstname:String?
    var lastname:String?
    var user_name:String?
    var adress:String?
    var gender:String?
    var email_id:String?
    var phone_1:String?
    var makani_no_1:String?
    var Authorization:String?
    
}
