//
//  DeliveryMethod.swift
//  OfferKart
//
//  Created by Sajin M on 16/12/2020.
//

import Foundation

struct DeliveryMethod:Codable {
    
    var status:Bool
    var message:String
    var result:[Methods]?
    var arrAvailableTimeSlotes:[Slots]?
    var storeAddress:String?
    var addressLine_1:String?
    var addressLine_2:String?
    var addressLine_3:String?
    var storeLatitude:String?
    var storeLongitude:String?
}


struct Methods:Codable {
    
    let id:String
    let method:String?
    let additionalCharge:String?
    let isMethodActive:Bool
}


struct Slots:Codable {
    
    let id:String
    let slotDate:String?
    let timeslotes:String?
    let alertMessage:String?
}
