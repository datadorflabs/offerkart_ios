//
//  ApiManager.swift
//  Riddle Book
//
//  Created by Sajin M on 6/24/19.
//  Copyright © 2019 Sweans. All rights reserved.
//

import Foundation




class ApiManager {
    
    private init () {}
    static let shared = ApiManager()
    
    let ApiHandler = RestHandler()
    
    
    func getHeader() -> [String:String] {
        
        if let token = Defaults.string(forKey:"token") {
        
            let tokenString = token
            
            return ["Authorization":tokenString]
        }
        
        return ["":""]
        
        
    }
    
    func getHeaders() -> [String:String] {
        
        if let token = Defaults.string(forKey:"token") {
        
            let tokenString = token
            
            return ["Authorization":tokenString]
        }
        
        return ["Authorization":""]
        
        
    }
    
    
    func getLogin(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,LoginModel?,String) -> Void))  {
        
        
        
        ApiHandler.request(toURL: url, method: .post,parameters: withParameter) { (response) in
            
            
            
            do {
                
                if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                    
                    
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(LoginModel.self, from:
                                                    response.data!)
                    
                    
                    DispatchQueue.main.async {
                        completion(true,model,model.message)
                    }
                    
                    
                }else{
                    
                    DispatchQueue.main.async {
                        completion(false,nil,cannotConnect)
                    }
                    
                }
                
                
                
            } catch let parsingError {
                
                print(parsingError)
                
                let model:LoginModel? = nil
                
                DispatchQueue.main.async {
                    completion(false,model,cannotConnect)
                }
                
            }
            
            
            
        }
        
        
    }
    
    
    func forgotPasswordData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,Common?,String) -> Void))  {



             ApiHandler.request(toURL: url, method: .post,parameters: withParameter) { (response) in



                 do {

                   if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {


                     let decoder = JSONDecoder()
                     let model = try decoder.decode(Common.self, from:
                         response.data!)


                     DispatchQueue.main.async {
                        completion(model.status,model,model.message ?? wentWrong)
                     }


                   }else{

                       DispatchQueue.main.async {
                           completion(false,nil,cannotConnect)
                       }

                   }



                 } catch let parsingError {

                    print(parsingError)

                     let model:Common? = nil

                     DispatchQueue.main.async {
                         completion(false,model,cannotConnect)
                     }

                 }



             }


         }
    
    
    func checkVersion(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool) -> Void))  {
          
          
          
          ApiHandler.request(toURL: url, method: .get,parameters: withParameter) { (response) in
              
              
              
              do {
                  
                if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                 
                  let decoder = JSONDecoder()
                  let model = try decoder.decode(Common.self, from:
                      response.data!)
                  DispatchQueue.main.async {
                    completion(model.status)
                  }
                }else{
                    
                    
                    
                    DispatchQueue.main.async {
                        completion(true)
                                       }
                    
                    
                }
            
                  
              } catch let parsingError {
                 
                print(parsingError)
                  
                  DispatchQueue.main.async {
                      completion(true)
                  }
                  
              }
              
              
              
          }
          
          
      }
    
    func completeVerificationWithFcm(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,VerifyUser?,String) -> Void))  {
          
          
           
                ApiHandler.request(toURL: url, method: .post,parameters: withParameter) { (response) in
                    
                    
                    
                    do {
                        
                      if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                       
                        let decoder = JSONDecoder()
                        let model = try decoder.decode(VerifyUser.self, from:
                            response.data!)
                        DispatchQueue.main.async {
                            
                            completion(model.status,model,model.message)
                        }
                      }else{
                          
                          let model:VerifyUser? = nil
                          
                          DispatchQueue.main.async {
                            completion(model?.status ?? false,model,cannotConnect)
                                             }
                          
                          
                      }
                      
                        
                        
                    } catch let parsingError {
                       
                        
                      print(parsingError)
                        let model:VerifyUser? = nil
                        
                        DispatchQueue.main.async {
                            completion(false,model,cannotConnect)
                        }
                        
                    }
                    
                    
                    
                }
                
          
          
      }
    
    func updatePasswordData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,String) -> Void))  {
          
        
            ApiHandler.request(toURL: url, method: .post,parameters: withParameter) { (response) in
                
                
                
                do {
                    
                  if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                   
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Common.self, from:
                        response.data!)
                    DispatchQueue.main.async {
                        completion(true,model.message ?? wentWrong)
                    }
                  }else{
                      
                    
                      
                      DispatchQueue.main.async {
                              completion(false,cannotConnect)
                                         }
                      
                      
                  }
                  
                    
                    
                } catch let parsingError {
                    
                    
                  print(parsingError)
                    
                    DispatchQueue.main.async {
                        completion(false,cannotConnect)
                    }
                    
                }
                
                
                
            }
            
          
          
      }
    
    func sendSMS(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,OtpResendModel?,String) -> Void))  {
          
          
       
          
          ApiHandler.request(toURL: url, method:.post, parameters: withParameter) { (response) in
              
              
              
              do {
                  
                if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                 
                  let decoder = JSONDecoder()
                  let model = try decoder.decode(OtpResendModel.self, from:
                      response.data!)
                  DispatchQueue.main.async {
                      completion(true,model,model.message)
                  }
                }else{
                    
                    let model:OtpResendModel? = nil
                    
                    DispatchQueue.main.async {
                            completion(false,model,cannotConnect)
                                       }
                    
                    
                }
                
                  
                  
              } catch let parsingError {
                 
                  
                print(parsingError)
                    let model:OtpResendModel? = nil
                  DispatchQueue.main.async {
                      completion(false,model,wentWrong)
                  }
                  
              }
              
              
              
          }
          
          
      }
    
    
    func getSignUp(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,SignUpModel?,String) -> Void))  {
        
        
        
        ApiHandler.request(toURL: url, method: .post,parameters: withParameter) { (response) in
            
            
            
            do {
                
              if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
               
                let decoder = JSONDecoder()
                let model = try decoder.decode(SignUpModel.self, from:
                    response.data!)
                DispatchQueue.main.async {
                    completion(true,model,model.message)
                }
              }else{
                  
                  DispatchQueue.main.async {
                        completion(false,nil,cannotConnect)
                                     }
                  
                  
              }
              
                
                
            } catch let parsingError {
                let model:SignUpModel? = nil
                
              print(parsingError)
                
                DispatchQueue.main.async {
                    completion(false,model,cannotConnect)
                }
                
            }
            
            
            
        }
        
        
        
        
    }
    
    
    func getHomeData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,Home?,String) -> Void))  {
        
        
        
        ApiHandler.request(toURL: url, method: .get,parameters: withParameter) { (response) in
            
            
            
            do {
                
                if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                    
                    
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Home.self, from:
                                                    response.data!)
                    
                    
                    DispatchQueue.main.async {
                        completion(model.status,model,model.message)
                    }
                    
                    
                }else{
                    
                    DispatchQueue.main.async {
                        completion(false,nil,cannotConnect)
                    }
                    
                }
                
                
                
            } catch let parsingError {
                
                print(parsingError)
                
                let model:Home? = nil
                
                DispatchQueue.main.async {
                    completion(false,model,cannotConnect)
                }
                
            }
            
            
            
        }
        
        
    }
    
    func promotionData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,Common?,String) -> Void))  {
          

     
            ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers: getHeader()) { (response) in
                
                print(self.getHeader())
                
                do {
                    
                  if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                   
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Common.self, from:
                        response.data!)
                    DispatchQueue.main.async {
                        completion(true,model,model.message ?? wentWrong)
                    }
                  }else{
                      
                    
                  
                    let model:Common? = nil
                      
                      DispatchQueue.main.async {
                              completion(false,model,cannotConnect)
                                         }
                      
                      
                  }
                  
                    
                    
                } catch let parsingError {
                    
                    
                  print(parsingError)
                    
                    let model:Common? = nil
                    
                    DispatchQueue.main.async {
                        completion(false,model,cannotConnect)
                    }
                    
                }
                
                
                
            }
            
          
          
      }
    
    
    func confirmFcmData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,String) -> Void))  {
          

     
            ApiHandler.request(toURL: url, method: .post,parameters: withParameter) { (response) in
                
          
                
                do {
                    
                  if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                   
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Common.self, from:
                        response.data!)
                    DispatchQueue.main.async {
                        completion(true,model.message ?? wentWrong)
                    }
                  }else{
                      
                    
                      
                      DispatchQueue.main.async {
                              completion(false,cannotConnect)
                                         }
                      
                      
                  }
                  
                    
                    
                } catch let parsingError {
                    
                    
                  print(parsingError)
                    
                    DispatchQueue.main.async {
                        completion(false,cannotConnect)
                    }
                    
                }
                
                
                
            }
            
          
          
      }
    
    
    func confirmOrderData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,String) -> Void))  {
          

     
            ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers: getHeaders()) { (response) in
                
          
                
                do {
                    
                  if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                   
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Common.self, from:
                        response.data!)
                    DispatchQueue.main.async {
                        completion(true,model.message ?? wentWrong)
                    }
                  }else{
                      
                    
                      
                      DispatchQueue.main.async {
                              completion(false,cannotConnect)
                                         }
                      
                      
                  }
                  
                    
                    
                } catch let parsingError {
                    
                    
                  print(parsingError)
                    
                    DispatchQueue.main.async {
                        completion(false,cannotConnect)
                    }
                    
                }
                
                
                
            }
            
          
          
      }
    
    
    func getWishListProductListData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,ProductListModel?,String) -> Void))  {
        
        
        
        ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers: getHeader()) { (response) in
            
            
            
            do {
                
              if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
               
                let decoder = JSONDecoder()
                let model = try decoder.decode(ProductListModel.self, from:
                    response.data!)
                DispatchQueue.main.async {
                    completion(true,model,model.message)
                }
              }else{
                  
                  let model:ProductListModel? = nil
                  
                  DispatchQueue.main.async {
                          completion(false,model,cannotConnect)
                                     }
            
              }
              
                
                
            } catch let parsingError {
                let model:ProductListModel? = nil
                
              print(parsingError)
                
                DispatchQueue.main.async {
                    completion(false,model,cannotConnect)
                }
                
            }
            
            
            
        }
        
        
    }
    
    
    func getCategoryProductListData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,ProductListModel?,String) -> Void))  {
        
        
        
        ApiHandler.request(toURL: url, method: .get,parameters: withParameter) { (response) in
            
            
            
            do {
                
              if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
               
                let decoder = JSONDecoder()
                let model = try decoder.decode(ProductListModel.self, from:
                    response.data!)
                DispatchQueue.main.async {
                    completion(model.status,model,model.message)
                }
              }else{
                  
                  let model:ProductListModel? = nil
                  
                  DispatchQueue.main.async {
                          completion(false,model,cannotConnect)
                                     }
            
              }
              
                
                
            } catch let parsingError {
                let model:ProductListModel? = nil
                
              print(parsingError)
                
                DispatchQueue.main.async {
                    completion(false,model,cannotConnect)
                }
                
            }
            
            
            
        }
        
        
    }
    
    
    func EmirateListData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,Emirate?,String) -> Void))  {



        ApiHandler.request(toURL: url, method: .get,parameters: withParameter) { (response) in



            do {

              if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {


                let decoder = JSONDecoder()
                let model = try decoder.decode(Emirate.self, from:
                    response.data!)


                DispatchQueue.main.async {
                   completion(model.status,model,model.message)
                }


              }else{

                  DispatchQueue.main.async {
                      completion(false,nil,cannotConnect)
                  }

              }



            } catch let parsingError {

               print(parsingError)

                let model:Emirate? = nil

                DispatchQueue.main.async {
                    completion(false,model,cannotConnect)
                }

            }



        }


    }
    
    func CategoryListData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,CategoryListModel?,String) -> Void))  {



        ApiHandler.request(toURL: url, method: .get,parameters: withParameter) { (response) in



            do {

              if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {


                let decoder = JSONDecoder()
                let model = try decoder.decode(CategoryListModel.self, from:
                    response.data!)


                DispatchQueue.main.async {
                   completion(model.status,model,model.message)
                }


              }else{

                  DispatchQueue.main.async {
                      completion(false,nil,cannotConnect)
                  }

              }



            } catch let parsingError {

               print(parsingError)

                let model:CategoryListModel? = nil

                DispatchQueue.main.async {
                    completion(false,model,cannotConnect)
                }

            }



        }


    }
    
    func cancelOrderData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,String) -> Void))  {
          
        
            ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers: getHeader()) { (response) in
                
                
                
                do {
                    
                  if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                   
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Common.self, from:
                        response.data!)
                    DispatchQueue.main.async {
                        completion(true,model.message ?? wentWrong)
                    }
                  }else{
                      
                    
                      
                      DispatchQueue.main.async {
                              completion(false,cannotConnect)
                                         }
                      
                      
                  }
                  
                    
                    
                } catch let parsingError {
                    
                    
                  print(parsingError)
                    
                    DispatchQueue.main.async {
                        completion(false,cannotConnect)
                    }
                    
                }
                
                
                
            }
            
          
          
      }
    
    func OrderDetailData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,OrderDetails?,String) -> Void))  {
        
        
         
              ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers: getHeader()) { (response) in
                  
                print(self.getHeader())
                  
                  do {
                      
                    if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                     
                      let decoder = JSONDecoder()
                      let model = try decoder.decode(OrderDetails.self, from:
                          response.data!)
                      DispatchQueue.main.async {
                          
                          completion(true,model,model.message)
                      }
                    }else{
                        
                        let model:OrderDetails? = nil
                        
                        DispatchQueue.main.async {
                                completion(false,model,cannotConnect)
                                           }
                        
                        
                    }
                    
                      
                      
                  } catch let parsingError {
                     
                      
                    print(parsingError)
                      let model:OrderDetails? = nil
                      
                      DispatchQueue.main.async {
                          completion(false,model,cannotConnect)
                      }
                      
                  }
                  
                  
                  
              }
              
        
        
    }
    
    
    func getOrderData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,OrderHistory?,String) -> Void))  {
          
  
                ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers: getHeader()) { (response) in
                    
                    
                    do {
                        
                        print(self.getHeader())
                        
                      if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                       
                        let decoder = JSONDecoder()
                        let model = try decoder.decode(OrderHistory.self, from:
                            response.data!)
                        DispatchQueue.main.async {
                            
                            completion(true,model,model.message)
                        }
                      }else{
                          
                          let model:OrderHistory? = nil
                          
                          DispatchQueue.main.async {
                                  completion(false,model,cannotConnect)
                                             }
                          
                          
                      }
                      
                        
                        
                    } catch let parsingError {
                       
                        
                      print(parsingError)
                        let model:OrderHistory? = nil
                        
                        DispatchQueue.main.async {
                            completion(false,model,cannotConnect)
                        }
                        
                    }
                    
                    
                    
                }
                
          
          
      }
    
    
    
    func DeliveryAddress(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,Address?,String) -> Void))  {
        
        ApiHandler.request(toURL: url, method:.post, parameters: withParameter, headers: getHeader()) { (response) in
            
            do {
                
                if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                    
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Address.self, from:
                                                    response.data!)
                    
                    DispatchQueue.main.async {
                        completion(true,model,model.message)
                    }
                    
                }else{
                    
                    DispatchQueue.main.async {
                        completion(false,nil,cannotConnect)
                    }
                    
                }
                
      
            } catch let parsingError {
                
                print(parsingError)
                
                let model:Address? = nil
                
                DispatchQueue.main.async {
                    completion(false,model,cannotConnect)
                }
                
            }
            
            
            
        }
        
        
    }
    
    
    func DeliveryMethods(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,DeliveryMethod?,String) -> Void))  {
        
        ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers: getHeader()) { (response) in
            
            do {
                
                if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                    
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(DeliveryMethod.self, from:
                                                    response.data!)
                    
                    DispatchQueue.main.async {
                        completion(model.status,model,model.message)
                    }
                    
                }else{
                    
                    DispatchQueue.main.async {
                        completion(false,nil,cannotConnect)
                    }
                    
                }
                
      
            } catch let parsingError {
                
                print(parsingError)
                
                let model:DeliveryMethod? = nil
                
                DispatchQueue.main.async {
                    completion(false,model,cannotConnect)
                }
                
            }
            
            
            
        }
        
        
    }
    
    
    func doAddToCat(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,Cart?,String) -> Void))  {
        
        ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers: getHeaders()) { (response) in
            
            do {
                
                if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                    
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Cart.self, from:
                                                    response.data!)
                    
                    DispatchQueue.main.async {
                        completion(model.status,model,model.message)
                    }
                    
                }else{
                    
                    DispatchQueue.main.async {
                        completion(false,nil,cannotConnect)
                    }
                    
                }
                
      
            } catch let parsingError {
                
                print(parsingError)
                
                let model:Cart? = nil
                
                DispatchQueue.main.async {
                    completion(false,model,cannotConnect)
                }
                
            }
            
            
            
        }
        
        
    }
    
    
    public func cartUpdate(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
            
            guard let url = URL(string:Api.cartUpdate) else {return}
            
            
            ApiManager.shared.confirmOrderData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, message) in
                
                if isSuccess{
                    
                    completion(true,message)
                    
                }
                    
                else {
                    completion(false,message)
                }
            })
     }
    
    
    func getProductDetails(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,ProductDetail?,String) -> Void))  {
        
        ApiHandler.request(toURL: url, method: .get,parameters: withParameter) { (response) in
            
            do {
                
                if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                    
                    
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(ProductDetail.self, from:
                                                    response.data!)
                    
                    
                    DispatchQueue.main.async {
                        completion(model.status,model,model.message)
                    }
                    
                    
                }else{
                    
                    DispatchQueue.main.async {
                        completion(false,nil,cannotConnect)
                    }
                    
                }
                
                
                
            } catch let parsingError {
                
                print(parsingError)
                
                let model:ProductDetail? = nil
                
                DispatchQueue.main.async {
                    completion(false,model,cannotConnect)
                }
                
            }
            
            
            
        }
        
        
    }
    
    func getSubCategoryData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,SubCategoryModel?,String) -> Void))  {
        
        ApiHandler.request(toURL: url, method: .get,parameters: withParameter) { (response) in
            
            do {
                
                if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                    
                    
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(SubCategoryModel.self, from:
                                                    response.data!)
                    
                    
                    DispatchQueue.main.async {
                        completion(model.status,model,model.message)
                    }
                    
                    
                }else{
                    
                    DispatchQueue.main.async {
                        completion(false,nil,cannotConnect)
                    }
                    
                }
                
                
                
            } catch let parsingError {
                
                print(parsingError)
                
                let model:SubCategoryModel? = nil
                
                DispatchQueue.main.async {
                    completion(false,model,cannotConnect)
                }
                
            }
            
            
            
        }
        
        
    }
    
    
    func cartListData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,CartModel?,String) -> Void))  {
        
        
        
        ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers: getHeaders()) { (response) in
            
            
            
            do {
                
               
                
                if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                    
                    
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(CartModel.self, from:
                                                    response.data!)
                    
                    
                    DispatchQueue.main.async {
                        completion(model.status,model,model.message)
                    }
                    
                    
                }else{
                    
                    DispatchQueue.main.async {
                        completion(false,nil,cannotConnect)
                    }
                    
                }
                
                
                
            } catch let parsingError {
                
                print(parsingError)
                
                let model:CartModel? = nil
                
                DispatchQueue.main.async {
                    completion(false,model,cannotConnect)
                }
                
            }
            
            
            
        }
        
        
    }
    
    
    
    
    
    
}






