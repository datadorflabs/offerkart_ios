//
//  SubCategoryViewController.swift
//  OfferKart
//
//  Created by Sajin M on 01/12/2020.
//

import UIKit
import BadgeSwift

class SubCategoryViewController: BaseViewController {
    
    
    @IBOutlet weak var categoryCollectionView: DynamicCollectionView!
    
    @IBOutlet weak var dealsCollectionView: UICollectionView!
    @IBOutlet weak var imgBanner: CurvedImage!
    @IBOutlet weak var lblCategoryTitle: UILabel!
    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var lblBadge: BadgeSwift!
    @IBOutlet weak var lblDeals: UILabel!
    @IBOutlet weak var searchView: UIView!
    
    @IBOutlet weak var dealViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var categoryHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var contentHeightConstraint: NSLayoutConstraint!
    
    var mainCatID:String?
    var categoryTitle:String?
    var categories:group?
    var deals:[Product]?
    var totalSearchCount:String?
    var customerId = null

    var searchFullArray = [Product]()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let id = Defaults.string(forKey: "customerId"){
            
            self.customerId = id
            
        }
        initialSetup()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
    
           setBadge()
            
     
    }
    
    
    func initialSetup(){
        self.searchView.isHidden = true
        self.lblDeals.isHidden = true
        self.lblBadge.isHidden = true
        
        if let title = self.categoryTitle{
            
        self.lblCategoryTitle.text = title
       
        self.categoryCollectionView.isHidden = true
        if isConnected(){
            
            if let id = self.mainCatID{
                
                 getCategory(catId:id)
                
            }
       
        }
    }
        
        self.searchTableView.tableFooterView = UIView(frame:.zero)
        self.txtSearch.delegate = self
        
        
    }
    
    func setBadge(){
        
        if let count = Defaults.string(forKey: "badge"){
            
            if count != "0"{
                
                self.lblBadge.isHidden = false
                self.lblBadge.text = count
                
            }else{
                self.lblBadge.isHidden = true
                
            }
        }
    }
    
    @IBAction func viewCart(_ sender: Any) {
        
        let CartScene = CartViewController.instantiate(fromAppStoryboard: .Main)
        
        if let navigator = self.navigationController {
            
            navigator.pushViewController(CartScene, animated: true)
        }
        
    }
    
    
    
    func search(product:String,limit:String){
        
 
        
        self.STProgress.show()
        
        
        let params = SearchParam(searchKey: product, limit: limit,customerId: self.customerId).Values
        
        ApiRequest.searchItem(withParameter: params) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                
                if let info = self.ApiRequest.MainCategoryProductsResponse?.products{
                    
                    
                    
                    if info.count > 0{
                        
                        self.totalSearchCount = self.ApiRequest.MainCategoryProductsResponse?.totalCount
                        
                        self.searchFullArray = info
                        self.searchTableView.delegate = self
                        self.searchTableView.dataSource = self
                        self.searchTableView.reloadData()
                        self.searchView.isHidden = false
                        
                        
                    }
                    
                    
                    
                }
                
                
                
                
            }
            
        }
        
        
    }
    
    @IBAction func searchCancelPressed(_ sender: Any) {
        
        self.searchView.isHidden = true
    }
        
        
    @IBAction func backPressed(_ sender: Any) {
        self.backNavigation()
    }
    
    
    func getCategory(catId:String){
    
            
             self.STProgress.show()
            
            let cartParam = groupParameters(mainCatId:catId).Values
            
            self.ApiRequest.getSubcategory(withParameter:cartParam) { (isSuccess,message) in
                              
                               self.STProgress.dismiss()
                                             
                                             if isSuccess {
                                             
                                                if let info = self.ApiRequest.GroupResponse{
                                                    
                                                    self.categories = info.groups
                                                    
                                                    if let imgUrl = info.banner?.data{
                                                                                             
                                                        let url = URL(string: imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? null)
                                                        let phImage = UIImage(named: ph)
                                                                                             
                                                        self.imgBanner?.kf.setImage(with: url, placeholder: phImage)
                                                                                            
                                                            }
                                                    
                                                    if let deals = info.deals{
                                                        if let title = deals.titleHead{
                                                            self.lblDeals.isHidden = false
                                                            self.lblDeals.text = title.uppercased()
                                                        }
                                                        
                                                        if deals.data?.count ?? 0 > 0{
                                                            self.deals = deals.data
                                                            self.dealsCollectionView.delegate = self
                                                            self.dealsCollectionView.dataSource = self
                                                            self.dealsCollectionView.reloadData()
                                                        }
                                                        
                                                        
                                                        
                                                    }
                                                    
                                  
                                                       if self.categories?.data?.count ?? 0 > 0 {
                                                        
                                                        self.categoryCollectionView.delegate = self
                                                        self.categoryCollectionView.dataSource = self
                                                        self.categoryCollectionView.reloadData()
                                                        self.categoryCollectionView.layoutIfNeeded()
                                                        self.categoryCollectionView.isHidden = false
                                                       
                                                    }
                                                    
                                                
                                                                            
                                                    
                                                }
                                            
                                              
                                             }else{
                                                                                                                      
                                                self.showAlert(message: wentWrong)
                                                                                                                                                 
                                                                                                                                                    }
                }
               
               }
   

}

extension SubCategoryViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == dealsCollectionView{
            
            return self.deals?.count ?? 0
        }
        
        return self.categories?.data?.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if collectionView == dealsCollectionView{
            
            let cell = dealsCollectionView.dequeueReusableCell(withReuseIdentifier: horizontalCollectionCell, for: indexPath) as! HorizontalCollectionCell
        
            
            if let info = self.deals?[indexPath.row]{
                
                if let title = info.productName{
                    
                    cell.lblProductName.text = title.uppercased()
                    
                }
                
                if let imgUrl = info.productImage{
                    
                    let url = URL(string: imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? null)
                    let phImage = UIImage(named: ph)
                    
                    cell.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                    
                    
                }
                
                if let weightType = info.weight{
                    
                    if let weightQty = info.qtyWeight{
                        
                        cell.lblWeight.text = weightQty + null + weightType
                    }
                    
                }
                
                
                if let off = info.offerPercentage{
                    
                    if Double(off)! > 0.0{
                        
                        cell.lblOff.text = off + "%"
                        
             
                        if let price = info.groceryProductPrice{
                            
                            let priceStr = currency + price
                            
                            cell.lblPrice.attributedText = priceStr.strikeThrough()
                            
                        }
                        
                        if let offerPrice = info.offerPrice{
                            
                            cell.lblOfferPrice.text =  currency + offerPrice
                            
                        }
                        
                    }else{
                        
                        
                         if let price = info.groceryProductPrice{
                        
                        cell.lblOfferPrice.text =  currency + price
                       
                        
                        }
                    }
                    
                    
                }
                
                cell.index = indexPath.row
               // cell.delegate = self
            
            
            
        }
        return cell
        
        
        }
 
        
        let cell = self.categoryCollectionView.dequeueReusableCell(withReuseIdentifier: subCatCell, for: indexPath) as! SubCatCollectionCell
        
        if let info = self.categories?.data?[indexPath.row]{
            
            cell.lblProduct.text = info.groupName
           
            
            if let imgUrl = info.groupImage{
                                          
                let url = URL(string: imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? null)
                                           let phImage = UIImage(named: ph)
                                          
                                           cell.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                                          
                                          
                                      }
            
            
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.categoryCollectionView{
        
        if let info = self.categories?.data?[indexPath.row]{
            
           
                       let LoadCategoryScene = LoadFromCategoryViewController.instantiate(fromAppStoryboard: .Main)

                               if let navigator = self.navigationController {

                                if let groupID = info.groupId{

                                    if let title = info.groupName{

                                         LoadCategoryScene.category_Title = title
                                    }


                                    LoadCategoryScene.groupId =  groupID
                                    //LoadCategoryScene.flagKey = "1"
                                    navigator.pushViewController(LoadCategoryScene, animated: true)

                                }



                               }
                 
                     }
                   
        }
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           
          
           
        if collectionView == categoryCollectionView {
            
            let width = (self.view.frame.size.width - 6 * 3) / 3
            let height = width * 1.3 //ratio
            return CGSize(width: width, height: height)
            
        }
        
        return CGSize(width: 147, height: 260)
       
}

}

extension SubCategoryViewController:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.searchTableView{
            return self.searchFullArray.count
        }
        
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == searchTableView{
            
            
            let cell = searchTableView.dequeueReusableCell(withIdentifier: searchCell, for: indexPath) as! SearchCell
            
            
            
            let info = self.searchFullArray[indexPath.row]
            
            cell.lblTitle.text = info.productName
            
            if let imgUrl = info.productImage{
                
                let url = URL(string: imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? null)
                let phImage = UIImage(named: ph)
                
                cell.itemPic?.kf.setImage(with: url, placeholder: phImage)
                
                
            }
            
            
            if (indexPath.row == self.searchFullArray.count - 1) {
            
                var count = 10
                let totalCount = Int(self.totalSearchCount ?? "10")!
                if self.searchFullArray.count < totalCount {
                    count = self.searchFullArray.count + 10
                    self.search(product: self.txtSearch.text!, limit: "\(count)")
                    
                }
            }
            
            return cell
        }
        
        return UITableViewCell()

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        if tableView == searchTableView{
            
            let info = searchFullArray[indexPath.row]
            

            let ProductDetailsScene = ProductDetailsVC.instantiate(fromAppStoryboard: .Main)
            
            if let navigator = self.navigationController {


                ProductDetailsScene.product = info
                ProductDetailsScene.isSearch = true
//                ProductDetailsScene.similarProducts = searchFullArray
                navigator.pushViewController(ProductDetailsScene, animated: true)



            }
            
            DispatchQueue.main.async {
                
                self.txtSearch.text = null
                self.searchView.isHidden = true
                self.txtSearch.resignFirstResponder()
                
            }
            
               
            
        }
        
        
        
        
    }
}

extension SubCategoryViewController:UITextFieldDelegate{
    
 
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        
        if self.txtSearch.text?.count ?? 0 > 2{
            
            let count = 10
            
            self.search(product:self.txtSearch.text!, limit: "\(count)")
            
        }
        return true
    }
    

    
}
