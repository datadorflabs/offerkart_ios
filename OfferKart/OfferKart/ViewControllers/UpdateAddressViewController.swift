//
//  UpdateAddressViewController.swift
//  OfferKart
//
//  Created by Sajin M on 25/12/2020.
//

import UIKit
import GoogleMaps
import CoreServices


class UpdateAddressViewController: BaseViewController {
    
    
    @IBOutlet weak var txtNme: UITextField!
    @IBOutlet weak var txtBuilding: UITextField!
    @IBOutlet weak var txtRoomNo: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtArea: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtSubCode: UITextField!
    
    @IBOutlet weak var lblLocation: UILabel!
    
    @IBOutlet weak var mapContainerView: UIView!
    
    var addNewsAddress:Bool? = true
  
    
    var mapView: GMSMapView!
    var okMarker: GMSMarker?
    var UserMarker: GMSMarker?

    var OkView: UIImageView?
    var address:Addresses?

    var currentLat:Double = 0.0
    var currentlong:Double = 0.0
    var markerLong:Double = 0.0
    var markerLat:Double = 0.0
    
    var addressTitle:String? = "Home"
    var customerId:String?
    var isEdit:Bool = false
    var isHome:Bool? = true
    
    var flag:String? = "1"
    var isMarkerDragged:Bool = false
    var addressId:String?
    var deliverableArea:Double = 10.0
    var setCircle:Int = 10000
    var emirateList:[emiratesData]?
    var camera = GMSCameraPosition()
    var cityId:String?
    
    var cityPicker = UIPickerView()
    var subCodePicker = UIPickerView()
    
    private var locationManager:CLLocationManager?
    
    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var imgOffice: UIImageView!
    @IBOutlet weak var mapContainerHeight: NSLayoutConstraint!
    
    let marker = GMSMarker()
    
    
  

    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        initialSetup()
    }
    
    func initialSetup(){
        
        let house = UIImage(named: "marker")!
        let markerView = UIImageView(image: house)
        marker.iconView = markerView
        marker.isDraggable = true
        
//        if Defaults.string(forKey: "deliverableArea") != nil {
//
//            if let area = Defaults.string(forKey: "deliverableArea"){
//
//
//
//                 self.deliverableArea = Double(area)!
//
//                self.setCircle =  Int(self.deliverableArea * 1000)
//
//
//
//            }
//
//        }
//
        
        getEmirates()
         
        self.txtSubCode.inputView = subCodePicker
        self.txtCity.inputView = cityPicker
        self.txtNme.delegate = self
        self.txtRoomNo.delegate = self
        self.txtBuilding.delegate = self
        self.txtMobile.delegate = self
        
        self.cityPicker.dataSource = self
        self.cityPicker.delegate = self
        self.subCodePicker.delegate = self
        self.subCodePicker.dataSource = self
        
        if let flag = self.flag{
            
            self.flag = flag
            
        }
        

        
        guard let custid = Defaults.string(forKey: "customerId") else{
            
            return
        }
        
        
//        if let shopLat = Defaults.string(forKey: "shopLat"){
//
//            self.currentLat = Double(shopLat)!
//
//
//        }
//        if let shopLang = Defaults.string(forKey: "shopLong"){
//
//            self.currentlong = Double(shopLang)!
//        }
//
//
        
      
        
        self.customerId = custid
        
        if let add = self.address{
            
            if let phone = add.customerMobile{
                
                if phone.count > 7{
                    
                   
                    let subStart = phone.index(phone.startIndex, offsetBy: 0)
                    let subEnd = phone.index(phone.endIndex, offsetBy: -7)
                    let subRange = subStart..<subEnd
                    
                    
                    let phoneStart = phone.index(phone.startIndex, offsetBy: 2)
                    let phoneEnd = phone.index(phone.endIndex, offsetBy: 0)
                    let phoneRange = phoneStart..<phoneEnd
                   
                    
                    self.txtSubCode.text = String(phone[subRange])
                    self.txtMobile.text = String(phone[phoneRange])
                    
                    //str[range]  // play
                    
                    
                    
                }else{
                    
                    self.txtMobile.text = add.customerMobile

                }
                
            }
            
            if let name = add.customerName{
                
                if let addressId = add.addressId{
                    
                    self.addressId = addressId
                }
                
                if let area = add.customerArea{
                    
                    self.txtArea.text = area
                }
                
                
                
                
                if let type = add.addressTitle{
                    
                    if type == "Home"{
                        
                        self.imgOffice.image = UIImage(named: "ic_radio")
                        self.imgHome.image = UIImage(named: "ic_radio_sel")
                        self.addressTitle = "Home"
                        self.isHome = true
                        
                        
                    }else{
                        
                        
                        self.imgOffice.image = UIImage(named: "ic_radio_sel")
                        self.imgHome.image = UIImage(named: "ic_radio")
                        self.addressTitle = "Office"
                        self.isHome = false
                        
                        
                    }
                    
                    
                   
         }
         

                self.txtNme.text = name.capitalizingFirstLetter()
                self.txtRoomNo.text = add.houseNoVillaNo
                self.txtBuilding.text = add.houseName?.capitalizingFirstLetter()
             
                if let long = add.longitude {
                    
                    self.markerLong = Double(long) ?? 0.0
                    self.currentlong = self.markerLong
                    
                    if let lat = add.latitude {
                        
                        self.markerLat = Double(lat) ?? 0.0
                        self.currentLat = self.markerLat
                        
                    }
                    
                }
                
                
            }
            
            
        }
        
        

        
        self.mapContainerHeight.constant =
            (UIScreen.main.bounds.height/2) + 100
        
    
        
        camera = GMSCameraPosition.camera(withLatitude:self.currentLat,
                                              longitude: self.currentlong,
                                                 zoom: 12)
        self.mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.mapContainerHeight.constant), camera: camera)

             
        if (self.currentLat > 0.0) && (self.currentlong > 0.0) {  // user marker
            
            let camera = GMSCameraPosition.camera(withLatitude: self.currentLat, longitude: self.currentlong, zoom: 14.0)

            self.mapView?.animate(to: camera)
        
            marker.position = CLLocationCoordinate2D(latitude: self.currentLat, longitude: self.currentlong)
            reverseGeocoding(marker: marker)
            marker.map = mapView
            self.marker.map = mapView

        }else{
            
      
            locationManager = CLLocationManager()
            locationManager?.delegate = self
            locationManager?.requestAlwaysAuthorization()
            
        
        }
        
        
            
           
        self.mapContainerView.addSubview(mapView)
         mapView.delegate = self
        self.mapContainerView.addSubview(mapView)

//        let house = UIImage(named: "marker")!
//        let markerView = UIImageView(image: house)
//        OkView = markerView
//


       // okMarker = marker
        

//        let circleCenter = CLLocationCoordinate2D(latitude: self.currentLat, longitude: self.currentlong)
//        let circ = GMSCircle(position: circleCenter, radius: CLLocationDistance(self.setCircle))
//
//        circ.fillColor = UIColor(red:161/255, green: 242/255, blue: 124/255, alpha:0.5)
//        circ.map = mapView
//        circ.strokeColor = UIColor.green
        
        
        
        
        
    }
    
    
    // MARK: - reverse geo
    
    func reverseGeocoding(marker: GMSMarker) {
        let geocoder = GMSGeocoder()
        let coordinate = CLLocationCoordinate2DMake(Double(marker.position.latitude),Double(marker.position.longitude))
        
        self.currentlong = marker.position.longitude
        self.currentLat = marker.position.latitude
        
        var currentAddress = String()
        
                    let camera = GMSCameraPosition.camera(withLatitude:Double(marker.position.latitude),
                                                                longitude:Double(marker.position.longitude),
                                                                   zoom: 14)
                          self.mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.mapContainerHeight.constant), camera: camera)

        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            if let address = response?.firstResult() {
                let lines = address.lines! as [String]
                                
                currentAddress = lines.joined(separator: "\n")
                self.lblLocation.text = currentAddress
            }
            
            
            
//            marker.position = CLLocationCoordinate2D(latitude: marker.position.latitude, longitude: marker.position.longitude)
//            marker.map = self.mapView
//            self.marker.map = self.mapView
            
            
           
//            let firsLocation = CLLocation(latitude: self.currentLat, longitude: self.currentlong)
//            let secondLocation = CLLocation(latitude: marker.position.latitude, longitude: marker.position.longitude)
//            let distance = firsLocation.distance(from: secondLocation) / 1000
            
//            if self.isMarkerDragged {
//
//                if distance > self.deliverableArea{
//
//                self.lblLocation.text = "Sorry!, We won't be able to deliver here"
//                self.lblLocation.textColor = UIColor.red
//            }else{
//
//                self.lblLocation.textColor = UIColor.black
//                 self.lblLocation.text = currentAddress
//
//            }
//
//            }
//
//

        }
    }
    

    
    func getEmirates(){
        
        self.ApiRequest.getEmirateList(withParameter:[:]) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                if let info = self.ApiRequest.EmirateResponse?.emirates{
                    
                    self.emirateList = info
                   if self.emirateList?.count ?? 0 > 0 {
                          
//                        self.txtCity.text = self.emirateList?[0].emirate
//                        self.cityId = self.emirateList?[0].id ?? "1"
                        self.cityPicker.dataSource = self
                        self.cityPicker.delegate = self
                        
                    }
                }
               
                
            }
    }
    }
    
    
    func updateAddress(name:String,landMark:String,houseName:String,mobile:String,addressTitle:String,area:String,customerId:String,long:String,lat:String,cityId:String){
        
        
        self.STProgress.show()
        
        
        var param = [String:String]()
        var urlStr = Api.addAddress
        
        if !self.isEdit {
            
            guard let cityId = self.cityId else{
                
                showAlert(message: selectEmirate)
                return
            }
            
            param = AddressParams(customerId:customerId, customer_name: name.capitalizingFirstLetter(), house_name: houseName.capitalizingFirstLetter(), land_mark: null, customer_area: area.capitalizingFirstLetter(), latitude: lat, longitude: long, customer_city:cityId, customer_mobile: mobile, addressTitle: self.addressTitle!, roomNo:landMark).Values
            
            
        }else{
            
            
            guard let addressId = self.addressId else{
                
                return
                
            }
            
            urlStr = Api.updateAddress
            
            guard let cityId = self.cityId else{
                
                showAlert(message: selectEmirate)
                return
            }
            
            param = UpdateAddressParams(addressId:addressId, customerId:customerId , customer_name: name.capitalizingFirstLetter(), house_name: houseName.capitalizingFirstLetter(), land_mark:null, customer_area: area.capitalizingFirstLetter(), latitude: lat, longitude: long, customer_city:cityId, customer_mobile: mobile, addressTitle: self.addressTitle!,roomNo:landMark).Values
            
        }
        
        

        self.ApiRequest.updateAddressData(url:urlStr,withParameter:param) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
        
                DispatchQueue.main.async {
                    
                     self.showAlert(message: addressUpdated)
                    
                }
                self.backNavigation()
                 
                
            }else{
                
                self.showAlert(message: message)
                
            }
        }
        
    }
    
    
    
    
    @IBAction func saveAddressPressed(_ sender: Any) {
        
        if isConnected(){
            
            
            
            guard let name = self.txtNme.text , let roomNo = self.txtRoomNo.text, let building = self.txtBuilding.text, let mobile = self.txtMobile.text, !name.isEmpty, !roomNo.isEmpty, !building.isEmpty, !mobile.isEmpty else{
                
                showAlert(message: fillAllFields)
                return
            }
            
            guard let cityId = self.cityId else{
                
                showAlert(message: selectEmirate)
                return
            }
           
                
                guard let subCode = self.txtSubCode.text else{
                    
                    return
                }
                
                let mobileStr = subCode + mobile

                self.updateAddress(name: name, landMark: roomNo, houseName: building, mobile: mobileStr, addressTitle: self.addressTitle!, area:self.txtArea.text ?? null, customerId: self.customerId!, long:  "\(self.currentlong)", lat: "\(self.currentLat)", cityId: cityId)
                
       
        }
        
        
        
        
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.backNavigation()
    }
    
    @IBAction func homePressed(_ sender: Any) {
    
               
               self.imgOffice.image = UIImage(named: "ic_radio")
               self.imgHome.image = UIImage(named: "ic_radio_sel")
               self.addressTitle = "Home"
               self.isHome = true
         

    }
    
    @IBAction func officePressed(_ sender: Any) {
     
            self.imgOffice.image = UIImage(named: "ic_radio_sel")
            self.imgHome.image = UIImage(named: "ic_radio")
            self.addressTitle = "Office"
            self.isHome = false
           
        
    }
    

}
extension UpdateAddressViewController:GMSMapViewDelegate{
    
    
 func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
    mapView.isMyLocationEnabled = addNewsAddress!
 }
 
 func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {

  
//  if (gesture) {
//   mapView.selectedMarker = marker
//  }
 }
 
 func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
  mapView.isMyLocationEnabled = addNewsAddress!
  return false
 }
 
 func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
  print("COORDINATE \(coordinate)") // when you tapped coordinate
 }
 
 func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
    
    
//  mapView.isMyLocationEnabled = true
//  mapView.selectedMarker = nil
    
    
  return false
 }
    
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        
        self.isMarkerDragged = true
       
       
           }
       
       func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
           
           
       }
       
       func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
      
        
          reverseGeocoding(marker: marker)
         
           //self.polyline.map = nil;
//           print("marker dragged to location: \(marker.position.latitude),\(marker.position.longitude)")
//          let locationMobi = CLLocation(latitude: marker.position.latitude, longitude: marker.position.longitude)
           
//        self.markerLat = marker.position.latitude
//        self.markerLong = marker.position.longitude

       }

    
}
extension UpdateAddressViewController:CLLocationManagerDelegate
{
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        if let location = locations.last {

        let camera = GMSCameraPosition.camera(withLatitude: (location.coordinate.latitude), longitude: (location.coordinate.longitude), zoom: 14.0)

        self.mapView?.animate(to: camera)
        
           
        marker.position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        marker.isDraggable = true
        marker.map = mapView
        self.marker.map = mapView

        reverseGeocoding(marker: marker)
        
       self.locationManager?.delegate = nil
            
       
        }
        
        //Finally stop updating location otherwise it will come again and again in this delegate
        self.locationManager?.stopUpdatingLocation()

    }
    
   
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("location manager authorization status changed")
        
        switch status {
        case .authorizedAlways:
            
            locationManager?.startUpdatingLocation()
            locationManager?.delegate = self
            
           // retriveCurrentLocation()
            print("user allow app to get location data when app is active or in background")
        case .authorizedWhenInUse:
            
            locationManager?.startUpdatingLocation()
            locationManager?.delegate = self
            
            print("user allow app to get location data only when app is active")
        case .denied:
            
             locationManager?.requestAlwaysAuthorization()
            
            print("user tap 'disallow' on the permission dialog, cant get location data")
        case .restricted:
            
           
            print("parental control setting disallow location data")
        case .notDetermined:
            
            locationManager?.requestAlwaysAuthorization()
            print("the location permission dialog haven't shown before, user haven't tap allow/disallow")
        default:
            break
        }
    }
    
    
    
    
}

extension UpdateAddressViewController:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.txtMobile{
        // get the current text, or use an empty string if that failed
        let currentText = textField.text ?? ""

        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: currentText) else { return false }

        // add their new text to the existing text
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

        // make sure the result is under 16 characters
        return updatedText.count <= 7
        }
        return true
    }

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1

        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }

        return true
    }
    
    
    
    
}

extension UpdateAddressViewController : UIPickerViewDelegate,UIPickerViewDataSource{

func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
}

func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    
    if pickerView == cityPicker{
        return self.emirateList!.count
    }
    
    if pickerView == subCodePicker{
        
        return subCodes.count
    }
    
    return 0
}
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
          
    
        if pickerView == cityPicker{
        self.txtCity.text = self.emirateList?[row].emirate
            
            if let id = self.emirateList?[row].id{
                
                self.cityId = id
            }
            
        
        }
        
        if pickerView == subCodePicker{
        
            self.txtSubCode.text = subCodes[row]
            
        }
       
      }
      
      

        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

    
            if pickerView == cityPicker{
            return emirateList?[row].emirate
            }
            
            return subCodes[row]
           

        }
    
    
    
}
