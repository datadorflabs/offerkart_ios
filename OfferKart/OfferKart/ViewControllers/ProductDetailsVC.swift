//
//  ProductDetailsVC.swift
//  OfferKart
//
//  Created by Sajin M on 19/11/2020.
//

import UIKit
import FSPagerView
import Kingfisher
import BadgeSwift
import ImageScrollView
import AVFoundation
import WebKit
import youtube_ios_player_helper

class ProductDetailsVC: BaseViewController {
    
    @IBOutlet weak var tagView: CurvedView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblOfferPrice: UILabel!
    @IBOutlet weak var lblActualPrice: UILabel!
    @IBOutlet weak var lblSave: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var btnDescription: UIButton!
    @IBOutlet weak var btnSpecification: UIButton!
    
    @IBOutlet weak var sizeCollectionView: UICollectionView!
    @IBOutlet weak var colorCollectionView: UICollectionView!
    @IBOutlet weak var relativeCollectionView: UICollectionView!
    
    @IBOutlet weak var txtCount: UITextField!
    @IBOutlet weak var lblBadge: BadgeSwift!
    
    @IBOutlet weak var sizeColorView: CurvedView!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var sizeView: UIView!
    
    @IBOutlet weak var lblDeliveryDate: UILabel!
    @IBOutlet weak var expressView: UIView!
    @IBOutlet weak var btnWishList: UIButton!
    @IBOutlet weak var imageScrollView: ImageScrollView!
    @IBOutlet weak var imageScrollParentView: UIView!
    @IBOutlet weak var descriptionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionView: CurvedView!
    @IBOutlet weak var sizeColorViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var sizeViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var colorViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var videoView: CurvedView!
    @IBOutlet weak var videoViewHeight: NSLayoutConstraint!
    @IBOutlet weak var imgThumbNail: UIImageView!
    
    @IBOutlet weak var relativeTopHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var videoSpaceHeightConstant: NSLayoutConstraint!
    
   // @IBOutlet weak var videoWebView: WKWebView!
    @IBOutlet weak var videoPlayer: YTPlayerView!
    
    
    var selectedColorId:String?
    var selectedSizeId:String?
    var minCount = 1
    var selectedItemPrice = 0.0
    var quantity:String? = "1"
    var productId:String?
    var uuidToken:String? = null
    var customerId:String = null
    var weight:String?
    var articleID:String?
    var actualColorId:String = null
    var actualSizeId:String = null
    var colorProducrId:String?
    var sizeProductId:String?
    var isSearch:Bool = false
    var isMakeFavourite:Bool = false
    var productID:String?
    
    
    
    
    @IBOutlet weak var bannerView: FSPagerView!{
        didSet {
           // 
            self.bannerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: bannerCell)
        }
    }
    
 
    @IBOutlet weak var pageControl: UIPageControl!
    
    var product:Product?
    var banners:[String]?
    var size:[productSize]?
    var color:[productColor]?
    var relatedProducts:[Product]?

    override func viewDidLoad() {
        super.viewDidLoad()

        initialSetup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        setBadge()
        
    }
    
    func setBadge(){
        
        if let count = Defaults.string(forKey: "badge"){
            
            if count != "0"{
                
                self.lblBadge.isHidden = false
                self.lblBadge.text = count
                
            }else{
                self.lblBadge.isHidden = true
                
            }
        }
    }
    
    func initialSetup(){
        
       
        self.lblBadge.isHidden = true
        self.imageScrollParentView.isHidden = true
        self.lblSave.isHidden = true
        self.tagView.isHidden = true
        self.expressView.isHidden = true
        imageScrollView.setup()
        
        if isConnected(){
            
           
            
            if isSearch{
                
                if let id = self.product?.productId{
                    
                    self.productID = id
                    self.getProductData(id: id)
                }
                
            }
            
            guard let id = self.product?.id , !id.isEmpty else{
                
                return
            }
            self.productID = id
            self.getProductData(id: id)
            
            
        }
        
    }
    
    @IBAction func imageScrollDismiss(_ sender: Any) {
        self.imageScrollParentView.isHidden = true
    }
    
    
    @IBAction func backPressed(_ sender: Any) {
        self.backNavigation()
    }
    
    func getVideoId(url:String) -> String{
        
    
        do {
            let regex = try NSRegularExpression(pattern: "(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)", options: NSRegularExpression.Options.caseInsensitive)
            let match = regex.firstMatch(in: url, options: NSRegularExpression.MatchingOptions.reportProgress, range: NSMakeRange(0, url.lengthOfBytes(using: String.Encoding.utf8)))
            let range = match?.range(at: 0)
            let youTubeID = (url as NSString).substring(with: range!)
            return youTubeID
        } catch {
        }
        
        
        return ""
        
    }
    
    
    @IBAction func descriptionPressed(_ sender: Any) {
        self.btnDescription.setTitleColor(.black, for: .normal)
        self.btnSpecification.setTitleColor(.lightGray, for: .normal)
        self.txtDescription.text = self.ApiRequest.ProductDetailResponse?.ProductDetails?.descriptions
    }
    
    @IBAction func specificationPressed(_ sender: Any) {
        
        self.btnDescription.setTitleColor(.lightGray, for: .normal)
        self.btnSpecification.setTitleColor(.black, for: .normal)
        self.txtDescription.attributedText = self.ApiRequest.ProductDetailResponse?.ProductDetails?.specification?.htmlToAttributedString
    }
    
    @IBAction func tappedAddToCart(_ sender: Any) {
        
        if let productId = self.productID{
            
            if let id = Defaults.string(forKey: "customerId"){
                
                self.customerId = id
            }
            if let uid = Defaults.string(forKey:UID){
                
                self.uuidToken = uid
            }
            
            if let weight = product?.weight{
                
                self.weight = weight
            }
            
            if let quantity = self.txtCount.text{
                
                self.quantity = quantity
            }
            
            
            let param = CartParams(customerId:self.customerId,uuId:self.uuidToken!,weight:self.weight!,quantity:self.quantity!,productId:productId).Values
            
            self.addToCart(params:param)
        }
        
        
    }
    
    @IBAction func wishListPressed(_ sender: UIButton) {
        
        if let customerId = Defaults.string(forKey: "customerId"){
            
            guard let id = self.productID else{
                return
            }
            
            
            switch sender.tag {
            case 0:
                
                self.addtoWishList(productId:id)
               
             case 1:
                
                self.removeWishList(productId:id)
                
            default:
                break
            }
 
            
        }else{
            
           
                self.STAlert.alert(title:"", message: pleaseLogin )
                    .action(.destructive("Login")){
                        
                        let loginScene = LoginViewController.instantiate(fromAppStoryboard: .Main)
                        
                        if let navigator = self.navigationController {
                            
                            navigator.pushViewController(loginScene, animated: true)
                            
                            
                            
                        }
                        
                        
                }.action(.cancel("Cancel"))
                    .show(on: self)
                
            
            
        }
        
        // if   sender.tag == 0
        
        
        
    }
    
    
    func addtoWishList(productId:String){
    
        guard let customerId = Defaults.string(forKey: "customerId") else {
            
            showAlert(message: pleaseLogin)
            
            return
            
        }
        
        
        self.STProgress.show()
            
               
        let params = FavParams(customerId: customerId, productId: productId).Values
               
               
               ApiRequest.AddtoWishList(withParameter: params) { (isSuccess,message) in
                   
                   self.STProgress.dismiss()
                   
                   if isSuccess {
                       
                        self.isMakeFavourite = true
                        self.setWishListView(status: 1)
                       
                    
                       
                       
                   }
                   
               }
               
               
        
        
        
    }
    
    func removeWishList(productId:String){
        
        self.STProgress.show()
            
               
        let params = FavParams(customerId: customerId, productId: productId).Values
               
               
               ApiRequest.RemoveWishList(withParameter: params) { (isSuccess,message) in
                   
                   self.STProgress.dismiss()
                   
                   if isSuccess {
                       
                    
                    self.setWishListView(status: 0)
                     self.isMakeFavourite = true
                       
                       
                       
                   }
                   
               }
               

        
    }
    
    func setWishListView(status:Int)  {
    
        switch status {
        case 1:
            
            
            self.btnWishList.setImage(UIImage(named: "ic_wish"), for: .normal)
            self.btnWishList.tag = 1
            
        case 0:
          
            self.btnWishList.setImage(UIImage(named: "ic_wishList"), for: .normal)
            self.btnWishList.tag = 0
            
         default:
            break
            
        }
        
        
    }
    
    @IBAction func increaseCount(_ sender: Any) {
        
        self.minCount = minCount + 1
        
       // let totalPrice = (selectedItemPrice.roundToDecimal(3) * Double(self.minCount)).roundToDecimal(3)
        self.txtCount.text = "\(self.minCount)"
        // self.lblCount.text = "x " + "\(self.minCount)"
        // self.lblTotalAmount.text =  currency + "\(totalPrice)"
        self.quantity = "\(self.minCount)"
       // self.price = "\(totalPrice)"
        
        
    }
    
    @IBAction func decreaseCount(_ sender: Any) {
        
        if self.minCount > 1{
            
            self.minCount  =  self.minCount - 1
           // let totalPrice = (selectedItemPrice.roundToDecimal(3) * Double(self.minCount)).roundToDecimal(3)
            self.txtCount.text = "\(self.minCount)"
            // self.lblCount.text = "x " + "\(self.minCount)"
            // self.lblTotalAmount.text =  currency + "\(totalPrice)"
           // self.price = "\(totalPrice)"
            self.quantity = "\(self.minCount)"
            
        }
        
    }
    
    // MARK: - ADD CART
    
 
    
    func addToCart(params:[String:String]){
        
        self.STProgress.show()
        
        
        ApiRequest.addTocart(withParameter: params) { (isSuccess,cartCount,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                
                DispatchQueue.main.async {
                    
                    
                    self.lblBadge.text = cartCount
                    self.lblBadge.isHidden = false
                    Defaults.set(cartCount, forKey: "badge")
                    
                    self.showToast(message: productAdded)
                    

                    
                }
                
           
                
            }else{
                
                self.showAlert(message: message)
                
            }
            
        }
                
    }
    
    // MARK: - VIEW CART
    
    @IBAction func viewCart(_ sender: Any) {
        
        let CartScene = CartViewController.instantiate(fromAppStoryboard: .Main)
        
        if let navigator = self.navigationController {
            
            navigator.pushViewController(CartScene, animated: true)
        }
        
        
    }
    
    
    
    // MARK: - GET PRODUCT
    
    func changeVariant(params:[String:String]){
    
//        
//        self.STProgress.show()
//        
//        
//        
//        
       // ApiRequest.viewProductDetailsWithID(withParameter:params) { (isSuccess,message) in
            
        ApiRequest.getProductDetails(withParameter:params) { (isSuccess,message) in
        self.STProgress.dismiss()

        if isSuccess {
         
            self.setupView()
        }else{
            

            self.showAlert(message:message)
            
        }
    }
        
        
    }
    
    func getProductData(id:String){
    
        self.STProgress.show()
        ApiRequest.getProductDetails(withParameter:["productId":id] ) { (isSuccess,message) in
        self.STProgress.dismiss()

        if isSuccess {
         
            self.setupView()
        }
    }
    }
    
    func setupView(){
        
        if let info = self.ApiRequest.ProductDetailResponse{
            
          
        if let produtData = self.ApiRequest.ProductDetailResponse?.ProductDetails{
        
        if produtData.productImageArr?.count ?? 0 > 0{
            
            
            self.pageControl.numberOfPages = produtData.productImageArr!.count
            self.banners = produtData.productImageArr
            self.bannerView.delegate = self
            self.bannerView.dataSource = self
            self.bannerView.reloadData()
            
            
        }
            
        
            
            if let favorite = produtData.isFavourite{
                                 
                                 if favorite == "1"{
                                     setWishListView(status: 1)
                                     
                                 }else{
                                     
                                     setWishListView(status: 0)
                                 }
                                 
                             }
            
            if produtData.isExpressDelivery{
                self.expressView.isHidden = false
            }else{
                
                self.expressView.isHidden = true
            }
            
            if let delDate = produtData.deliveryOn{
                self.lblDeliveryDate.text = delDate.getDayMonth() + " if you order now!"
            }
            
            if let title = produtData.productName{
                
                self.lblProductName.text = title
                
                if let weightType = produtData.weight{
                    
                    if let weightQty = produtData.qtyWeight{
                        
                        let qty = weightQty + "" + weightType
                        
                        self.lblProductName.text = title + " - " + qty
                        
                    }
                    
                }
                
               
            }
            
            
            if let video = produtData.productVideoLink{
                
                if video == null{
                    self.videoView.isHidden = true
                    self.videoViewHeight.constant = 0
                    self.relativeTopHeightConstant.constant = 0
                    self.videoSpaceHeightConstant.constant = 0
                }else{
                    
                
                    videoPlayer.load(withVideoId: getVideoId(url: video), playerVars: ["playsinline": "1"])
                    self.videoPlayer.delegate = self

                    //loadYoutube(videoID: video)
                   // self.videoPlayer.delegate = self
                   // self.videoPlayer.loadVideo(byURL: video, startSeconds: 1)
                    self.relativeTopHeightConstant.constant = 7
                    self.videoSpaceHeightConstant.constant = 7
                    self.videoView.isHidden = false
                    self.videoViewHeight.constant = 139
                    
                    
                }
            }else{
                
                self.videoView.isHidden = true
                self.videoViewHeight.constant = 0
                self.relativeTopHeightConstant.constant = 0
                self.videoSpaceHeightConstant.constant = 0
                
            }
            
            if let articleId = produtData.articleID{
                
                self.articleID = articleId
            }
            
            
            if let color = produtData.productImageColor{
                
                self.selectedColorId = color
                self.actualColorId = color
            }
            
            if let size = produtData.productImageSize{
                
                self.selectedSizeId = size
                self.actualColorId = size
            }
            
            
            if let spec = produtData.specification{
                
                self.txtDescription.attributedText = spec.htmlToAttributedString
                
                if let desc = produtData.descriptions{
                    
                    if spec.count == 0 && desc.count == 0{
                        
                        self.descriptionHeightConstraint.constant = 0
                        self.descriptionView.isHidden = true
                    }else{
                        self.descriptionHeightConstraint.constant = 277
                        self.descriptionView.isHidden = false
                        
                    }
                    
                }
                
               
                
            }
            
            
           
            
            if let colors = info.Colors, let size = info.Sizes {
                
                            if colors.count > 0 &&  size.count > 0 {
                
                
                                self.sizeColorView.isHidden = false
                                self.sizeColorViewHeightConstraint.constant = 139
                             
                                
                                    self.size = info.Sizes
                                    self.sizeCollectionView.delegate = self
                                    self.sizeCollectionView.dataSource = self
                                    self.sizeCollectionView.reloadData()
                           
                                    self.color = info.Colors
                                    self.colorCollectionView.delegate = self
                                    self.colorCollectionView.dataSource = self
                                    self.colorCollectionView.reloadData()
                               
                
                            }else if size.count > 0{
                
                                self.sizeColorView.isHidden = false
                                self.sizeColorViewHeightConstraint.constant = 80
                                self.sizeViewHeightConstant.constant = 67
                                self.colorViewHeightConstant.constant = 0
                                self.colorView.isHidden = true
                                self.sizeView.isHidden = false
                                self.size = info.Sizes
                                self.sizeCollectionView.delegate = self
                                self.sizeCollectionView.dataSource = self
                                self.sizeCollectionView.reloadData()
                
                            }else if colors.count > 0{
                
                                self.sizeColorView.isHidden = false
                                self.sizeColorViewHeightConstraint.constant = 80
                                self.sizeViewHeightConstant.constant = 0
                                self.colorViewHeightConstant.constant = 67
                                self.colorView.isHidden = false
                                self.sizeView.isHidden = true
                                self.color = info.Colors
                                self.colorCollectionView.delegate = self
                                self.colorCollectionView.dataSource = self
                                self.colorCollectionView.reloadData()
                
                            }
                
                            else{
                
                                self.sizeColorView.isHidden = true
                                self.sizeColorViewHeightConstraint.constant = 0
                            }
                
                
                
            }
            
            
            
//            if colors!.count > 0 &&  info.Sizes!.count > 0 {
//
//
//                self.sizeColorView.isHidden = false
//                self.sizeColorViewHeightConstraint.constant = 139
//
//            }else if (info.Sizes?.count)!  > 0{
//
//                self.sizeColorView.isHidden = false
//                self.sizeColorViewHeightConstraint.constant = 80
//                self.sizeViewHeightConstant.constant = 67
//                self.colorViewHeightConstant.constant = 0
//                self.colorView.isHidden = true
//                self.sizeView.isHidden = false
//
//            }else if info.Colors!.count  > 0{
//
//                self.sizeColorView.isHidden = false
//                self.sizeColorViewHeightConstraint.constant = 80
//                self.sizeViewHeightConstant.constant = 0
//                self.colorViewHeightConstant.constant = 67
//                self.colorView.isHidden = false
//                self.sizeView.isHidden = true
//
//            }
//
//            else{
//
//                self.sizeColorView.isHidden = true
//                self.sizeColorViewHeightConstraint.constant = 0
//            }
            
            
        

           
        
            if let actualPrice = produtData.price{
                
                self.lblActualPrice.attributedText = (currency + actualPrice).strikeThrough()
                
                if let offerPrice = produtData.offerPrice{
                    
                    self.lblOfferPrice.text = currency + offerPrice
                    
                    self.lblActualPrice.isHidden = false
                    if Double(offerPrice)! > 0 {
               
                if Double(actualPrice)! > Double(offerPrice)!{
                    
                    
    
                    let saved = Double(actualPrice)! - Double(offerPrice)!
                    self.lblSave.text = "You have saved " + currency + "\(saved.roundToDecimal(3))" + " on this product"
                  
                    self.lblSave.isHidden = false
                    
                }else{
                    
                    self.lblSave.isHidden = true
                }
                    
                    }else{
                        
                        if let price =  produtData.price{
                            
                            self.lblOfferPrice.text = currency + price
                            self.lblSave.isHidden = true
                            self.lblActualPrice.isHidden = true
                        }
                        
                        
                        
                    }
                }
                    
                    
            
                if let category = produtData.brandName{
                    self.tagView.isHidden = false
                    self.lblCategory.text = category
                }else{
                    
                    self.tagView.isHidden = true
                }
            
            
        }
        }
        
        if info.RelativeProducts?.data?.count ?? 0 > 0{
            
            self.relatedProducts = info.RelativeProducts?.data
            self.relativeCollectionView.delegate = self
            self.relativeCollectionView.dataSource = self
            self.relativeCollectionView.reloadData()
            
        }
        
        

        
        
        }
        
}
    
    
//    func loadYoutube(videoID:String) {
//            guard
//                let youtubeURL = URL(string: videoID)
//                else { return }
//        self.videoWebView.load( URLRequest(url: youtubeURL) )
//        }
    
    
    
    
    func addProduct(product: Product,productId:String?) {
        
        if let productId = productId{
            
            self.productID = productId
            
            if let id = Defaults.string(forKey: "customerId"){
                
                self.customerId = id
            }
            if let uid = Defaults.string(forKey:UID){
                
                self.uuidToken = uid
            }
            
            if let weight = product.weight{
                
                self.weight = weight
            }
            
            
            let param = CartParams(customerId:self.customerId ?? null,uuId:self.uuidToken ?? null,weight:self.weight ?? "1 Pc",quantity:"1",productId:productId).Values
            
            self.addToCart(params:param)
        }
    }
        
        
}
   


extension ProductDetailsVC: FSPagerViewDelegate,FSPagerViewDataSource {
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.banners?.count ?? 0
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        
        guard let myImage = pagerView.cellForItem(at: index)?.imageView?.image else {
            return
            
        }
        self.imageScrollView.display(image: myImage)
        self.imageScrollParentView.isHidden = false
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageControl.currentPage = targetIndex
    }
    

   
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        
        
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: bannerCell, at: index)
        
       
      
        if let imgUrl = self.banners?[index]{
            
           
            let url = URL(string: imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? null)
                let phImage = UIImage(named: ph)
                cell.imageView?.contentMode = .scaleAspectFit
                cell.imageView?.kf.setImage(with: url, placeholder: phImage)
          
        }
        
        return cell
        
        
    }
    
    
    
}

extension ProductDetailsVC:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.sizeCollectionView{
            
            
            return self.size!.count
            
        }
        if collectionView == self.colorCollectionView{
            
            return self.color!.count
            
        }
        
        if collectionView == self.relativeCollectionView{
            
            return self.relatedProducts?.count ?? 0
        }
        
        return 0
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView == self.relativeCollectionView{
            
           
        
            if let product = self.relatedProducts?[indexPath.row]{
      
            let ProductDetailsScene = ProductDetailsVC.instantiate(fromAppStoryboard: .Main)
            
            if let navigator = self.navigationController {
                
                ProductDetailsScene.product = product
            
             
                navigator.pushViewController(ProductDetailsScene, animated: true)
                
              }
            }
            
        }
        
        
        if collectionView == self.sizeCollectionView{
            
            
            guard let id = size?[indexPath.row].code else{
                
                return
            }
            
            if let productId = size?[indexPath.row].productId{
                
                self.productID = productId
            }
            
            self.selectedSizeId = id
            
            DispatchQueue.main.async {
                self.sizeCollectionView.reloadData()
            }
            
            let param =  ChangeProduct(articleID: self.articleID!, productColorId: self.selectedColorId ?? self.actualColorId, productSizeId:id).Values
            
            self.changeVariant(params:param)
            
        }
        if collectionView == self.colorCollectionView{
            
            guard let id = color?[indexPath.row].code else{
                
                return
            }
            
            if let productId = color?[indexPath.row].productId{
                
                self.productID = productId
            }
            
            self.selectedColorId = id
            
            DispatchQueue.main.async {
                self.colorCollectionView.reloadData()
            }

            
            let param =  ChangeProduct(articleID: self.articleID!, productColorId:id, productSizeId: self.selectedSizeId ?? self.actualSizeId).Values
            self.changeVariant(params:param)
        }
        
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       
    
        if collectionView == self.colorCollectionView{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: genericCollectionCell, for: indexPath) as! GenericCollectionCell
        
            if let info = color?[indexPath.row]{
                
                if let name = info.name{
                    
                    cell.lblTitle.text = name
                    
                    if info.code == selectedColorId {
                    
                        cell.bgView.backgroundColor = Colors.lightBlue
                        cell.lblTitle.textColor = Colors.OKBlue
                        self.selectedColorId = info.code
                        
                    }else{
                        
                        cell.bgView.backgroundColor = .white
                    }
                }
                
                
                
               
            }
            
        
            return cell
            
        }
        
        if collectionView == self.sizeCollectionView{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: genericCollectionCell, for: indexPath) as! GenericCollectionCell
            
            if let info = size?[indexPath.row]{
                
                cell.lblTitle.text = info.name
                
                if info.code == selectedSizeId {
                
                    cell.bgView.backgroundColor = Colors.lightBlue
                    cell.lblTitle.textColor = Colors.OKBlue
                    self.selectedSizeId = info.code
                    
                }else{
                    
                    cell.bgView.backgroundColor = .white
                }
            }
            
            
            return cell
        }
        
        if collectionView == self.relativeCollectionView{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: horizontalCollectionCell, for: indexPath) as! HorizontalCollectionCell
        
            
            if let info = self.relatedProducts?[indexPath.row]{
                
                if let title = info.productName{
                    
                    cell.lblProductName.text = title.uppercased()
                    
                }
                
                if let imgUrl = info.productImage{
                    
                    let url = URL(string: imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? null)
                    let phImage = UIImage(named: ph)
                    
                    cell.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                    
                    
                }
                
                if let weightType = info.weight{
                    
                    if let weightQty = info.qtyWeight{
                        
                        cell.lblWeight.text = weightQty + null + weightType
                    }
                    
                }
                
                
                if let off = info.offerPrice{
                    
                    
                    cell.lblPrice.isHidden = false
                    
                    if Double(off)! > 0.0{
                        
                        if let offer = info.offerPercentage{
                            
                            cell.lblOff.text = offer + "%"
                        }
                        
                       
                        
             
                        if let price = info.price{
                            
                            let priceStr = currency + price
                            
                            cell.lblPrice.attributedText = priceStr.strikeThrough()
                            
                        }
                        
                        if let offerPrice = info.offerPrice{
                            
                            cell.lblOfferPrice.text =  currency + offerPrice
                            
                        }
                        
                        cell.offerTagView.isHidden = false
                        cell.lblOff.isHidden = false
                        cell.lblPrice.isHidden = false
                        
                    }else{
                        
                        cell.offerTagView.isHidden = true
                        cell.lblOff.isHidden = true
                        cell.lblPrice.isHidden = true
                        
                         if let price = info.groceryProductPrice{
                        
                        cell.lblOfferPrice.text =  currency + price
                       
                         }else{
                            
                            if let price = info.price{
                                cell.lblOfferPrice.text =  currency + price
                            }
                            
                         }
                    }
                    
                    
                }else{
                    
                    
                    if let price = info.price{
                        cell.lblOfferPrice.text =  currency + price
                    }
                    
                    cell.lblPrice.isHidden = true
                    
                    
                    
                }
                
                cell.index = indexPath.row
                cell.delegate = self
            
            
            
        }
            
            return cell
        }
        
        return UICollectionViewCell()
    }
    

}

extension ProductDetailsVC:HorizontalAddDelegate{
    func addTapped(at index: Int) {
        
        if let info = self.relatedProducts?[index]{
            
            if let id = info.productId{
                self.addProduct(product: info,productId: id)
                
            }else{
                
                if let id = info.id{
                    self.addProduct(product: info,productId: id)
                    
                }
                
                
            }
            
            
        }
        
    }
    
}
    
extension ProductDetailsVC: YTPlayerViewDelegate {
    func playerViewPreferredWebViewBackgroundColor(_ playerView: YTPlayerView) -> UIColor {
        return UIColor.black
    }
    
//    func playerViewPreferredInitialLoading(_ playerView: YTPlayerView) -> UIView? {
////        let customLoadingView = UIView()
////        Create a custom loading view
////        return customLoadingView
//    }
}



