//
//  WishlistViewController.swift
//  OfferKart
//
//  Created by Sajin M on 23/01/2021.
//

import UIKit
import BadgeSwift

class WishlistViewController: BaseViewController {
    
    
  
    @IBOutlet weak var lblBadge: BadgeSwift!
    
    @IBOutlet weak var productCollectionView: UICollectionView!

    var ListProducts:[Product] = []
    

    var categoryTitle:String?
    var bannerUrl:String?
    
    var customerId:String? = "0"


    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblBadge.isHidden = true

        getProducts()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func viewCart(_ sender: Any) {
        
        let CartScene = CartViewController.instantiate(fromAppStoryboard: .Main)
        
        if let navigator = self.navigationController {
            
            navigator.pushViewController(CartScene, animated: true)
        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
     
       
         if let count = Defaults.string(forKey: "badge"){
             
             if count != "0"{
                 
                 self.lblBadge.isHidden = false
                 self.lblBadge.text = count
                 
             }else{
                  self.lblBadge.isHidden = true
                 
           
          }
         }else{
            
             self.lblBadge.isHidden = true
            
        }
         
     }
    
    
    func getProducts(){
        
        let ApiEndPoint = Api.myWishList
       
        self.STProgress.show()
        
        guard let custId = Defaults.string(forKey: "customerId") else{
            
             return
        }
        
        let params = WishListParams(customerId:custId).Values
                 
        ApiRequest.getWishListProducts(endPoint: ApiEndPoint,withParameter: params) { (isSuccess,message) in
                               
                                self.STProgress.dismiss()
                                              
                                              if isSuccess {
                                                  
                                                  if let data = self.ApiRequest.MainCategoryProductsResponse {
                                                    
                                                   
                                                      
                                                    self.ListProducts.append(contentsOf: data.favouriteProducts!)
                                                    
                                                    
                                                    if self.ListProducts.count > 0{
                                                        
                                                        self.productCollectionView.delegate = self
                                                        self.productCollectionView.dataSource = self
                                                        self.productCollectionView.reloadData()
                                                        
                                                        
                                                    }
                                                      
                                                  }
                                              
                                         
                                             
                                               
                                              }
                
                }
                
                
             
             
         }
    
    
    @IBAction func backPressed(_ sender: Any) {
        self.backNavigation()
    }

    
}

extension WishlistViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    

        
        return self.ListProducts.count
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if self.ListProducts[indexPath.row] != nil{
    
        let product = self.ListProducts[indexPath.row]
  
        let ProductDetailsScene = ProductDetailsVC.instantiate(fromAppStoryboard: .Main)
        
        if let navigator = self.navigationController {
            
            ProductDetailsScene.product = product
            
            if let title = product.productName{
                
                ProductDetailsScene.title = title
            }
                
         
            navigator.pushViewController(ProductDetailsScene, animated: true)
            
            
        }
        }
        
        
        
    }
      
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: horizontalCollectionCell, for: indexPath) as! HorizontalCollectionCell
        
    
        if self.ListProducts[indexPath.row] != nil{
            
           let info = self.ListProducts[indexPath.row]
            
            if let title = info.productName{
                
                cell.lblProductName.text = title
                
            }
            

            
            
            if let off = info.offerPercentage {
                
                if Double(off)! > 0.0{
                    
                    cell.lblOff.text = off + "%"
                    cell.offerTagView.isHidden = false
                    cell.lblPrice.isHidden = false
                    

                    if let price = info.price {
                                    
                                    let priceStr =  currency + price
                                    
                                     cell.lblPrice.attributedText = priceStr.strikeThrough()
                                    
                                }
                    
                    if let offerPrice = info.offerPrice{
                                       
                                       cell.lblOfferPrice.text =  currency + offerPrice
                                       
                                   }
                    
                }else{
                    
                    cell.offerTagView.isHidden = true
                   
                    if let offerPrice = info.offerPrice{
                        
                        if Double(offerPrice)! > 0.0{
                            
                            cell.lblPrice.isHidden = false
                        
                        if let price = info.price{
                            
                            let priceStr =  currency + price
                            
                            cell.lblPrice.attributedText = priceStr.strikeThrough()
                            cell.lblOfferPrice.text = currency + offerPrice
                            
                        }
                            
                        }else{
                            
                            cell.lblPrice.isHidden = true
                            
                            if let price = info.price{
                            
                                cell.lblOfferPrice.text = currency + price
                            }
                            
                            
                        }
                        
                    }else{
                        
                        if let price = info.price{
                            
                            cell.lblOfferPrice.text = currency + price
                        }
                        
                        
                        
                    }
                    
              
                }
                
                
            }else{
                
                
                
                if let offerPrice = info.offerPrice{
                    
                    if let price = info.price{
                        
                        let priceStr =  currency + price
                        
                        cell.lblPrice.attributedText = priceStr.strikeThrough()
                        cell.lblOfferPrice.text = currency + offerPrice
                        
                    }
                    
                }else{
                    
                    if let price = info.price{
                        
                        cell.lblOfferPrice.text = currency + price
                    }
                    
                    cell.offerTagView.isHidden = true
                    cell.offerTagView.isHidden = true
                    
                }
                
                
                

                
            }
            

//                if let weight = info.weight {
//
//                    if let weightQty = info.qtyWeight{
//
//                        let weightStr = weightQty + " " + weight
//
//                        cell.lblWeight.text = weightStr
//
//
//                    }
//
//                }
            
            if let imgUrl = info.productImage{
                
                let url = URL(string: imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? null)
                 let phImage = UIImage(named: ph)
                
                 cell.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                
                
            }
            
            
        }
        
        

        
        return cell
       
  
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
               
              
               
               let width = (collectionView.frame.size.width - 5 * 2) / 2
        let height = width * 1.35 //ratio
               return CGSize(width: width, height: height)
               
           
    }
    
    
}


