//
//  OrderDetailsViewController.swift
//
//
//  Created by Sajin M on 07/07/2020.
//  Copyright © 2020 Daradorf. All rights reserved.
//

import UIKit
import BadgeSwift

class OrderDetailsViewController: BaseViewController {
    
    
    var groceryName:String?
    var orderSysId:String?
    var totalValue:Double = 0.00
    var orderId:String?
    var delFee:Double = 0.00
    var discount:Double = 0.00
    var deliveryDate:String?
    var netAmount:String?
    var itemCount:String?
    var status:String?
    
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblStore: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblSysId: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var btnReturn: CMButton!
    @IBOutlet weak var cancelView: UIView!
    
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblShippingFee: UILabel!
    @IBOutlet weak var lblPromoDiscount: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblPaymentType: UILabel!
    
    @IBOutlet weak var lblDelAddress: UILabel!
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var deliveryTitle: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblQty: BadgeSwift!
    
    var orderData:[orderDetailsData]?
    var orderProducts:[Product]?
    var orderDetail:OrderDetails?
    
     @IBOutlet weak var cartListTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        
        initalSetup()
        // Do any additional setup after loading the view.
    }
    
    
    
    func initalSetup(){
        
        self.btnCancel.isHidden = true
        

        
        self.btnReturn.isHidden = true
        
        cartListTableView.isHidden = true
        cartListTableView.rowHeight = UITableView.automaticDimension
        cartListTableView.estimatedRowHeight = 100

                    
                    if isConnected(){
                        
                      
                        
                        if let id = self.orderId{
                            
                        
                              getCartData(orderId:id)
                            
                        }else{
                            
                            DispatchQueue.main.async {
                                self.showAlert(message: wentWrong)
                            }
                            
                            self.backNavigation()
                        }
                        
                                
                            
                                   }
         
        
    }
    
    
    @IBAction func helpPressed(_ sender: Any) {
        
        guard let phone = Defaults.string(forKey: "callNumber") else{

               return
           }

         
        callNumber(phoneNumber: phone)

          
           
    }
    
    private func callNumber(phoneNumber:String) {

        if let phoneCallURL = URL(string: "telprompt://\(phoneNumber)") {

            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                     application.openURL(phoneCallURL as URL)

                }
            }
        }
    }
    
    
    
    @IBAction func returnPolicyPressed(_ sender: Any) {
        
//
//        let ReturnScene = ReturnViewController.instantiate(fromAppStoryboard: .Main)
//
//                                                if let navigator = self.navigationController {
//
//
//
//                                                    ReturnScene.groceryCity = self.lblCity.text
//                                                    ReturnScene.groceryTitle = self.lblStore.text
//
//
//                                                    navigator.pushViewController(ReturnScene, animated: true)
//
//
//
//                                                }
//
    }
    
    
    
    @IBAction func cancelOrderPressed(_ sender: Any) {
        
        
                   self.STAlert.alert(title:confirm, message: confimMessage )
                                     .action(.destructive("Continue")){
                                        
//                                        guard let custId = Defaults.string(forKey: "customerId") else{
//                                                   return
//
//                                               }
//
                                        guard let orderId = self.orderId else{
                                            return
                                                                                          
                                                                                      }
                                        
                                        
                                         
                                        self.cancelOrder(orderId: orderId)
                                      
                                         
                                 }.action(.cancel("Cancel"))
                                     .show(on: self)
        
        
    }
    
    
    func cancelOrder(orderId:String){
        
        guard let customerId = Defaults.string(forKey: "customerId") else{
         return
            
        }
        
          self.STProgress.show()

        let params = CancelProductParameters(orderId: orderId,customerId:customerId).Values

                      ApiRequest.cancelOrder(withParameter: params) { (isSuccess,message) in

                        self.STProgress.dismiss()

                                      if isSuccess {

                                         NotificationCenter.default.post(name: Notification.Name(MyOrderReload), object: nil)

                                        DispatchQueue.main.async {
                                                      self.showAlert(message: message)
                                                      }

                                        self.backNavigation()


                                      }else{


                                        DispatchQueue.main.async {
                                                      self.showAlert(message: message)
                                                      }

                                        self.backNavigation()


                                          }




                                      }
        
        }
        

    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
    }
    
   
    func getCartData(orderId:String){
        
        
        guard let customerId = Defaults.string(forKey: "customerId") else{
         return
            
        }
        
        
        self.STProgress.show()
        
       
        
        self.ApiRequest.getOrderDetails(withParameter:["orderId":orderId,"customerId":customerId]) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                
                
                if let info = self.ApiRequest.OrderDetailsResponse?.deliveryOrderDetails{
                    
                    self.orderDetail = self.ApiRequest.OrderDetailsResponse
                    
                      self.orderData = info
                    
                    for shipment in info{
                        
                        
                        if shipment.products?.count ?? 0 > 0{
                        
                        for product in shipment.products!{
                        
                        if let price = product.price {
                            
                            if let qty = product.qty{
                                
                                let priceValue = Double(price)! * Double(qty)!
                                self.totalValue = self.totalValue + priceValue
                              
                                
                            }
                        }else{
                            
                            if let price = product.productPrice {
                                
                                if let qty = product.qty{
                                    
                                    let priceValue = Double(price)! * Double(qty)!
                                    self.totalValue = self.totalValue + priceValue
                                  
                                    
                                }
                            }
                            
                            
                            
                        }
                            
                        }
                
                        }
                    }
                    
                    if let status = self.orderDetail?.orderStatus{
                        
                        if status == "1"{
                            
                            self.btnCancel.isHidden = false
                        }else{
                            
                            self.btnCancel.isHidden = true
                        }
                    }else{
                        
                        self.btnCancel.isHidden = true
                        
                    }
                    
                    
                    if let method = self.ApiRequest.OrderDetailsResponse?.deliveryMethod{
                        
                        if method == "3"{
                            
                            if let pickAddress = self.ApiRequest.OrderDetailsResponse?.pickingLocation{
                                
                                self.deliveryTitle.text = "Collection Point"
                                self.lblDelAddress.text = pickAddress
                            }
                        }else{
                            
                            if let shipAddres = self.ApiRequest.OrderDetailsResponse?.shipAddress{
                                
                                 self.deliveryTitle.text = "Delivery Address"
                                 self.lblDelAddress.text = shipAddres
                                
                            }
                            
                        }
                        
                    }
                     
                   
                    
                    if self.orderData?.count ?? 0 > 0{
                        
                        if let discount = self.orderDetail?.orderPromoDiscount{
                            
                            self.lblPromoDiscount.text = currency + discount
                            self.discount = Double(discount) ?? 0.00
                        }
                        
                        if let method = self.orderDetail?.paymentMethod{
                            
                            if method == "cash_od"{
                                
                                self.lblPaymentType.text = cod
                            }else{
                                
                                self.lblPaymentType.text = cad
                            }
                            
                           
                        }
                        
                        if let delFee = self.orderDetail?.deliveryCharge{
                            
                            self.lblShippingFee.text = currency + delFee
                            self.delFee = Double(delFee) ?? 0.00
                        }
                       
                        
                        self.lblSubTotal.text = currency + "\(self.totalValue.roundToDecimal(2))"
                        
                        let total = (self.totalValue - self.discount) + self.delFee
                        self.lblTotalAmount.text = currency + "\(total.roundToDecimal(2))"
                        
                        
//                        if let subTotal = self.orderDetail?.netAmount{
//
//                            self.lblTotalAmount.text =  currency + subTotal
//                            self.lblSubTotal.text = currency + subTotal
//
//                            if let discount = self.orderDetail?.orderPromoDiscount{
//
//                                let sub = (Double(subTotal)! + Double(discount)!)
//
//                                self.lblSubTotal.text = currency + "\(sub)"
//                                let total = (Double(subTotal)! - Double(discount)!) + self.delFee
//                                self.lblTotalAmount.text = currency + "\(total)"
//                            }else{
//
//                                if let subTotal = self.orderDetail?.netAmount{
//
//                                    self.lblSubTotal.text = currency + subTotal
//
//                                    let total = Double(subTotal)! + self.delFee
//
//                                    self.lblTotalAmount.text =  currency + "\(total)"
//                                }
//
//                            }
//
//                         }

                        
                        if let orderId = self.orderDetail?.orderId{
                            
                            self.lblOrderId.text = "Order ID - " + orderId
                        }
                        
                        
                        self.cartListTableView.delegate = self
                        self.cartListTableView.dataSource = self
                        self.cartListTableView.reloadData()
                        self.cartListTableView.isHidden = false
                        
                        
                    }
                
                        
                       
                        
                    }else{
                        
                        
                        if let info = self.ApiRequest.OrderDetailsResponse{
                            
                            self.orderDetail = info
                            self.orderProducts = self.ApiRequest.OrderDetailsResponse?.orderDetails
                            
                            if self.orderProducts?.count ?? 0 > 0{
                            
                            for product in self.orderProducts!{
                            
                            if let price = product.price {
                                
                                if let qty = product.qty{
                                    
                                    let priceValue = Double(price)! * Double(qty)!
                                    self.totalValue = self.totalValue + priceValue
                                }
                                }else{
                                    
                                    if let price = product.productPrice {
                                        
                                        if let qty = product.qty{
                                            
                                            let priceValue = Double(price)! * Double(qty)!
                                            self.totalValue = self.totalValue + priceValue
                                          
                                            
                                        }
                                    }
                                    

                                }
                            }
                                
                            }
                            
                            if let status = self.orderDetail?.orderStatus{
                                
                                if status == "1" {
                                    
                                    self.btnCancel.isHidden = false
                                    
                                }else{
                                    
                                    self.btnCancel.isHidden = true
                                }
                            
                            }else{
                                
                                self.btnCancel.isHidden = true
                            }
                     
                            if let method = self.ApiRequest.OrderDetailsResponse?.deliveryMethod{
                                
                                if method == "3"{
                                    
                                    if let pickAddress = self.ApiRequest.OrderDetailsResponse?.pickingLocation{
                                        
                                        self.deliveryTitle.text = "Collection Point"
                                        self.lblDelAddress.text = pickAddress
                                    }
                                }else{
                                    
                                    if let shipAddres = self.ApiRequest.OrderDetailsResponse?.shipAddress{
                                        
                                         self.deliveryTitle.text = "Delivery Address"
                                         self.lblDelAddress.text = shipAddres
                                        
                                    }
                                    
                                }
                                
                            }
                            
       
                            if self.orderProducts?.count ?? 0 > 0{
                                
                                if let discount = info.orderPromoDiscount{
                                    
                                    self.lblPromoDiscount.text = currency + discount
                                    self.discount = Double(discount) ?? 0.00
                                }
                                
                                if let delFee = info.deliveryCharge{
                              
                                        self.lblShippingFee.text = currency + delFee
                                        self.delFee = Double(delFee) ?? 0.00
                                                                  }
                                
                                if let method = info.deliveryMethod{
                                    
                                    if method == "cash_od"{
                                        
                                        self.lblPaymentType.text = cod
                                    }else{
                                        
                                        self.lblPaymentType.text = cad
                                    }
                                    
                                   
                                }
                                
                                self.lblSubTotal.text = currency + "\(self.totalValue.roundToDecimal(2))"
                                
                                let total = (self.totalValue - self.discount) + self.delFee
                                self.lblTotalAmount.text = currency + "\(total.roundToDecimal(2))"
                                
                                
//                                if let subTotal = info.netAmount{
//
//
//                                    if let delFee = info.deliveryCharge{
//
//                                        self.lblShippingFee.text = currency + delFee
//                                        self.delFee = Double(delFee) ?? 0.00
//                                    }
//
//
//
//                                    self.lblSubTotal.text = currency + subTotal
//
//
//                                        if let discount = self.orderDetail?.orderPromoDiscount{
//
//                                            let sub = (Double(subTotal)! + Double(discount)!)
//
//                                            self.lblSubTotal.text = currency + "\(sub)"
//
//                                            let total = (sub - Double(discount)!) + self.delFee
//
//
//
//                                            self.lblTotalAmount.text =  currency + "\(total.roundToDecimal(2))"
//
//
//                                        }else{
//                                            if let subTotal = self.orderDetail?.netAmount{
//
//                                                self.lblSubTotal.text = currency + subTotal
//
//                                                let total = Double(subTotal)! + self.delFee
//
//                                                self.lblTotalAmount.text =  currency + "\(total.roundToDecimal(2))"
//                                            }
//
//
//                                        }
//                                }

                                
                                if let orderId = info.orderId{
                                    
                                    self.lblOrderId.text = "Order ID - " + orderId
                                }
                                
                                
                                self.cartListTableView.delegate = self
                                self.cartListTableView.dataSource = self
                                self.cartListTableView.reloadData()
                                self.cartListTableView.isHidden = false
                                
                                
                            }
                            
                            
                            
                            
                        }
                        
                        
                    
                      
                       // self.cartListTableView.isHidden = true
                      
                        
                    }
                    
                    
                }
                
        
        }
        
    }

}

extension OrderDetailsViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.orderData?.count ?? 0 > 0 {
            
            return self.orderData?.count ?? 0
        }else{
            
            return 1
        }
        
       
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.cartListTableView.dequeueReusableCell(withIdentifier: detailCell) as! OrderDetailsCell
        
        

        
        if let product = self.orderData?[indexPath.row]{
            
            cell.cartData = product.products
            cell.deliveryType = self.orderDetail?.deliveryMethod
            
    
            if product.deliveryStatus! != nil{
            
                
                
                let status = product.deliveryStatus!
                cell.deliveryStatus = status
            
                if status == "1" {
                    
                    if let date = product.deliveryDate{
                    
                        cell.lblDelveryDate.text = "Order Placed on " + date
                  }
                }else if status == "4"{
                    
                    if let date = product.deliveryDate{
                    
                        cell.lblDelveryDate.text = "Delivered on " + date
                    }
                    
                }else{
                    
                    if let date = product.deliveryDate{
                    cell.lblDelveryDate.text =  date
                    }

                    cell.lblShipment.isHidden = false
                    cell.lblShipment.text = "Shipment " + "\(indexPath.row + 1)"
             
                    
                }

                
            }
            
        }else{
            
           // print(self.orderData?.count)
           
            cell.lblShipment.isHidden = true
            cell.deliveryType = self.orderDetail?.deliveryMethod
            
            if let status = self.orderDetail?.orderStatus{
                
                cell.deliveryStatus = status
            
            
            if let date = self.deliveryDate{
                
                if status == "1" {
                    
                    
                        cell.lblDelveryDate.text = "Order Placed on " + date
                }else if status == "4"{
                    
            
                        cell.lblDelveryDate.text = "Delivered on " + date
                
                    
                }else{
                    
                    cell.lblDelveryDate.text =  date
                   
                //cell.lblDelveryDate.text = "Delivery expected by " + date
          }
        
        }
            cell.cartData = self.orderProducts
            
        }
        }
        
        
       
       
    
        return cell
    }
    
    
    
}
