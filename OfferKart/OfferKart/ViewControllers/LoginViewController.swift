//
//  LoginViewController.swift
//  OfferKart
//
//  Created by Sajin M on 01/12/2020.
//

import UIKit


class LoginViewController: BaseViewController {
    
    @IBOutlet weak var txtEmail: TextfiledWithImage!
    @IBOutlet weak var txtPassword: TextfiledWithImage!
    
    @IBOutlet weak var txtSubCode: TextfiledWithImage!
    
    
    var fcmStr:String?
    var customerId:String?
    
    var newPasswordSet:Bool = false
    var subCodePicker = UIPickerView()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.txtSubCode.inputView = subCodePicker
        subCodePicker.delegate = self
        self.txtEmail.delegate = self
    
        
    }
    
    @IBAction func registerPressed(_ sender: Any) {
        
        
        let SignScene = SignUpViewController.instantiate(fromAppStoryboard: .Main)

        if let navigator = self.navigationController {

            navigator.pushViewController(SignScene, animated: true)
            
            
            
        }
        
        
        
    }
    
    
    
    @IBAction func loginPressed(_ sender: Any) {
        
//        txtEmail.resignFirstResponder()
//        txtPassword.resignFirstResponder()
        
        
               
        if isConnected(){
            
           
                   
                   guard let textMobile = self.txtEmail.text, let textPwd = self.txtPassword.text,!textMobile.isEmpty,!textPwd.isEmpty else {
                      
                    self.showNotification(message: notValidCredentials)
                      
                       return
                   }
            
               
            if textMobile.count < 5 {
                
                 self.showNotification(message: notValidPhone)
                
            }else{
                
                let mobile = self.txtSubCode.text! + textMobile
                
                self.doLogin(username: mobile, password: textPwd)
                
                
            }
            
            

            
            
        }
        
        
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
        
    }
    
    
    func registerFcm(userId:String,fcm:String){
        
        let params = FcmRegisterParam(fcm_regId: fcm, user_id: userId).Values
                             
                             ApiRequest.LogInFCM(withParameter: params) { (isSuccess,message) in
                              
                               self.STProgress.dismiss()
                                             
                                             if isSuccess {
                                                 
                                                
                                                return

                                                   
                                        
                                             } else{
                                               
                                               
                                              return
                                               
                               }
        }
        
    }
    
    func doLogin(username:String,password:String){
        
        
        self.STProgress.show()
        
               
        let params = LoginParameters(username: username, password: password).Values
                      
                      ApiRequest.goLogin(withParameter: params) { (isSuccess,message) in
                       
                        self.STProgress.dismiss()
                                      
                                      if isSuccess {
                                          
                                        if let data = self.ApiRequest.LoginResponse?.customer_details{
                                            
                                       
                                            
                                            if let customerId = data[0].id {
                                                
                                                if let token = data[0].Authorization{
                                                    
                                                    Defaults.set(token,forKey:"token")
                                                }
                                                
                                                Defaults.set(customerId, forKey: "customerId")
                                                
                                                if let fcm = Defaults.string(forKey: "fcmToken"){
                                                    
                                                    self.registerFcm(userId: "\(customerId)", fcm: fcm)
                                                    
                                                }
                                                
                                             
                                            }
                                            
                                            if let address = data[0].adress {
                                                
                                                Defaults.set(address, forKey: "address")
                                                
                                                
                                            }
                                            if let name = data[0].firstname{
                                                
                                                Defaults.set(name, forKey: "name")
                                                
                                                if let lastName = data[0].lastname{
                                                    
                                                    Defaults.set(lastName, forKey: "lastName")
                                                    
                                                }
                                                
                                            }
                                            
                                            
                                            if self.newPasswordSet{
                                                
                                                self.navigationController?.popToViewController(ofClass: HomeViewController.self)
                                            }else{
                                                
                                                self.backNavigation()
                                            }
                         
                                        
                                        }
                                      }else{
                                        
                                        
                                        self.showAlert(message: message)
                                        
                        }
        
        }
    }
    
    
    @IBAction func forgotPressed(_ sender: Any) {
        
        
        let ForgotScene = ForgotPasswordViewController.instantiate(fromAppStoryboard: .Main)
               
               if let navigator = self.navigationController {
                  
                
                 //  ForgotScene.isfo = true
                   navigator.pushViewController(ForgotScene, animated: true)
                   
                   
                   
               }
        
        
        
   }
    

}

extension LoginViewController:UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
       return subCodes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if subCodes[row] != nil{
            self.txtSubCode.text = subCodes[row]
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {


        return subCodes[row]
       

    }
    
    
    

func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    // get the current text, or use an empty string if that failed
    let currentText = textField.text ?? ""

    // attempt to read the range they are trying to change, or exit if we can't
    guard let stringRange = Range(range, in: currentText) else { return false }

    // add their new text to the existing text
    let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

    // make sure the result is under 16 characters
    return updatedText.count <= 7
}
}


