//
//  MyOrdersViewController.swift
//
//
//  Created by Sajin M on 03/07/2020.
//  Copyright © 2020 Datadorf. All rights reserved.
//

import UIKit

class MyOrdersViewController: BaseViewController {
    
    @IBOutlet weak var orderTableView: UITableView!
    
    var orderList:[OrderData] = []
    var totalcount:Int = 0
    var page:Int = 0
    var customerId:String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()

        // Do any additional setup after loading the view.
    }
    
    
    
    
    func initialSetup(){
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.CancelOrderNotification(notification:)), name: Notification.Name(MyOrderReload), object: nil)
        
        guard let custId = Defaults.string(forKey: "customerId") else{
            return
            
        }
        self.customerId = custId
        self.orderTableView.tableFooterView = UIView(frame:.zero)
        getOrderList(customerId: custId, limit: "0" )
        
    }
    
    
    
      @objc func CancelOrderNotification(notification: Notification) {
        
        guard let custId = Defaults.string(forKey: "customerId") else{
                   return
                   
               }
        
        self.orderList.removeAll()
        
         getOrderList(customerId: custId, limit: "0" )
        
    }
    
    @IBAction func startShopping(_ sender: Any) {
        
         self.backNavigation()
    }
    
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
    }
    
   
    func getOrderList(customerId:String,limit:String){
    
            
             self.STProgress.show()
            
            let Param = CustomerParams(customerId:customerId,limit: "0").Values
                        self.ApiRequest.getOrderHistory(withParameter:Param) { (isSuccess,message) in
                              
                               self.STProgress.dismiss()
                                             
                                             if isSuccess {
                                             
                                                if let info = self.ApiRequest.OrderHistoryResponse?.customerHistory{
                                                    
                                                    
                                                    self.orderList.append(contentsOf: info)
                                                    if let totalCount = self.ApiRequest.OrderHistoryResponse?.totalCount{
                                                        
                                                        self.totalcount = Int(totalCount) ?? 0
                                                        
                                                    }
                                                    
                                                    if self.orderList.count > 0 {
                                                        
                                                        self.orderTableView.delegate = self
                                                        self.orderTableView.dataSource = self
                                                        DispatchQueue.main.async {
                                                            self.orderTableView.reloadData()
                                                        }
                                                       
                                                        self.orderTableView.isHidden = false
                                                            
                                                    }else{
                                                        self.orderTableView.isHidden = true
                                                        
                                                    }
                                                        
                          
                                                        }
                                                        
                                                        
                                            
                                              
                                             }else{
                                                
                                                
                                                self.showAlert(message: wentWrong)
                                                
                                                                                                                                                                                    }
                }
               
               }
}


extension MyOrdersViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.orderList.count
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        let info = self.orderList[indexPath.row]
        
        if let status = info.orderStatus {
                  
//            if status != "0" {
//
//                if status != "5"{

                                            let OrderScene = OrderDetailsViewController.instantiate(fromAppStoryboard: .Main)

                                            if let navigator = self.navigationController {

                                        

                                                OrderScene.orderId = info.orderId
                                                OrderScene.orderSysId = info.orderIdSys
                                                OrderScene.deliveryDate = info.deliveryDate
                                                OrderScene.status = info.orderStatus

                                                navigator.pushViewController(OrderScene, animated: true)

//                                            }
//
//                                            }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.orderTableView.dequeueReusableCell(withIdentifier: orderCell, for: indexPath) as! OrderCell
        
        if orderList[indexPath.row] != nil{
        
         let info = orderList[indexPath.row]
            
//            if let title = info.storeName{
//
//                cell.lblOrderTitle.text = title
//
//            }
            if let orderId = info.orderIdSys{
                
                cell.lblOrderId.text = orderId
                
            }
            if let status = info.orderStatus{
                
                cell.lblOrderStatus.text = Utils.orderStatus(type: status)
                cell.lblOrderStatus.textColor = Utils.orderStatusColor(type: status)
                
                if let date = info.deliveryDate{
               
                                       cell.lblOrderTitle.text = "Ordered on " + date.getMonth() + " " + date.getDay()
                                 }
                
                
                
//                if status == "1" || status == "2" || status == "3" || status == "5"{
//
//                    if let date = info.deliveryDate{
//
//                        cell.lblOrderTitle.text = "Delivery expected by " + date.getMonth() + null + date.getDay()
//                  }
//                }
//                    if status == "4"{
//
//                        if let date = info.deliveryDate{
//
//                            cell.lblOrderTitle.text = "Delivered on " + date.getMonth() + null + date.getDay()
//                    }
//
//            }
                
            }
//            if let amount = info.netAmount{
//
//                cell.lblOrderAmount.text =  currency + amount
//
//            }
            if let count = info.totItems{
                
                cell.lblOrderCount.text = count
                
                
            }
            
//            if let date = info.deliveryDate{
//
//
//
//                cell.lblDay.text = date.getDay()
//                cell.lblMonth.text = date.getMonth()
//                cell.lblYear.text = date.getYear()
//            }
//
            
            
            if indexPath.row == self.orderList.count - 1 {
                
                if self.orderList.count < self.totalcount{
                    
                   
                    
                    let limit = self.page + 1
                    self.getOrderList(customerId: self.customerId!, limit: "\(limit)")
                    
                    
                }
                
                
            }
            
         
            
        }
        
        return cell
        
    }
    
    
    
    
    
}
