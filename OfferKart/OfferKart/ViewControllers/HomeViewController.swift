//
//  HomeViewController.swift
//  OfferKart
//
//  Created by Sajin M on 08/11/2020.
//

import UIKit
import BadgeSwift
import CoreLocation
import Kingfisher

class HomeViewController: BaseViewController {
    
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var lblBadge: BadgeSwift!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var updateView: UIView!
    
    
    private var locationManager:CLLocationManager?
   
    
    var homeData:Home?
    var customerId:String = ""
    var uuidToken:String = ""
    var weight:String = "1 Pc"
    var totalSearchCount:String?
    
    
    var searchFullArray = [Product]()
   
    override func viewDidLoad() {
        super.viewDidLoad()
     
        initialSetup()
    }
    
    
    func initialSetup() {
        
  
        self.lblBadge.isHidden = true
        self.updateView.isHidden = true
        self.btnMenu.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControl.Event.touchUpInside)
        self.searchView.isHidden = true
        self.searchTableView.tableFooterView = UIView(frame:.zero)
        self.txtSearch.delegate = self
        
        if isConnected(){
            
            loadHomeData()
            
            guard let fcm = Defaults.string(forKey: "fcmToken") else{
                
                return
            }
            
            if let id = Defaults.string(forKey: "customerId"){
                
                self.customerId = id
                
            }else{
                
                registerFcm(userId: null, fcm: fcm)
            }
            
           
        }
        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
    

        if isConnected(){
            checkForUpdation()
            
        }else{
            
            self.updateView.isHidden = true
        }
       setBadge()
        
    }
    
   
    
    
    @IBAction func searchCancelPressed(_ sender: Any) {
        
        self.txtSearch.text = null
        self.searchView.isHidden = true
        self.txtSearch.resignFirstResponder()
        
    }
    
    func registerFcm(userId:String,fcm:String){
        
        let params = FcmRegisterParam(fcm_regId: fcm, user_id: userId).Values
                             
                             ApiRequest.signInFCM(withParameter: params) { (isSuccess,message) in
                              
                               self.STProgress.dismiss()
                                             
                                             if isSuccess {
                                                 
                                                
                                                return

                                                   
                                        
                                             } else{
                                               
                                               
                                              return
                                               
                               }
        }
        
    }
    
    func search(product:String,limit:String){
        
 
        
        self.STProgress.show()
        
        
        let params = SearchParam(searchKey: product, limit: limit, customerId:self.customerId).Values
        
        ApiRequest.searchItem(withParameter: params) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                
                if let info = self.ApiRequest.MainCategoryProductsResponse?.products{
                    
                    
                    
                    if info.count > 0{
                        
                        self.totalSearchCount = self.ApiRequest.MainCategoryProductsResponse?.totalCount
                        
                        self.searchFullArray = info
                        self.searchTableView.delegate = self
                        self.searchTableView.dataSource = self
                        self.searchTableView.reloadData()
                        self.searchView.isHidden = false
                        
                        
                    }
                    
                    
                    
                }
                
            
                
            }
            
        }
        
        
    }
    
    
    func loadHomeData(){
        
        
        
        self.STProgress.show()
        
        var params = HomeParams().Values
        
        if let uid = Defaults.string(forKey: UID){
            
            params = HomeParams(customerId:null, uuId: uid).Values
        }
        
        ApiRequest.getHomeDataRequest(withParameter: params) { (isSuccess,message) in
            
         
            
            if isSuccess {
                
                
                if let info = self.ApiRequest.HomeResponse{
                    
                    if let count = info.cartCount{
                        
                        if let countStr = Defaults.string(forKey: "badge"){
                            
                            if countStr != "0"{
                                
                                self.setBadge()
                                
                            }else{
                                
                                Defaults.set(count, forKey:"badge")
                                self.setBadge()

                            }
                            
                        }
                        
                      
                    }
                    
                    if let chatNumber = info.chat_no{
                        
                        Defaults.set(chatNumber, forKey:"chatNumber")
                    }
                    
                    if let callNumber = info.callno{
                        
                        Defaults.set(callNumber, forKey:"callNumber")
                    }
                    
                    self.homeData = info
                    self.homeTableView.delegate = self
                    self.homeTableView.dataSource = self
                    self.homeTableView.reloadData()
                    
                    self.STProgress.dismiss()
                    
                }
            }else{
                
                self.STProgress.dismiss()
                self.showAlert(message: message)
                
            }
            
        }
    }
    
    func setBadge(){
        
        if let count = Defaults.string(forKey: "badge"){
            
            if count != "0"{
                
                self.lblBadge.isHidden = false
                self.lblBadge.text = count
                
            }else{
                self.lblBadge.isHidden = true
                
            }
        }
    }
    
    func addToCart(params:[String:String]){
        
        self.STProgress.show()
        
        
        ApiRequest.addTocart(withParameter: params) { (isSuccess,cartCount,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                
                DispatchQueue.main.async {
                    
                    
                    self.lblBadge.text = cartCount
                    self.lblBadge.isHidden = false
                    Defaults.set(cartCount, forKey: "badge")
                    
                    self.showToast(message: productAdded)
                    
                    
                    
                }
                
                
                
                
            }else{
                
                self.showAlert(message: message)
                
            }
            
        }
        
        
        
        
    }
    
    
    @IBAction func updatePressed(_ sender: Any) {
        
        
        if let url = URL(string: "itms-apps://apple.com/us/app/offerkart/id1548318451") {
            UIApplication.shared.open(url)
        }
    }
    
    
    
    @IBAction func viewCart(_ sender: Any) {
        
        
        let CartScene = CartViewController.instantiate(fromAppStoryboard: .Main)
        
        if let navigator = self.navigationController {
            
            navigator.pushViewController(CartScene, animated: true)
        }
        
        
    }
    
    
    func checkForUpdation() {
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        guard let version = appVersion else{
            
            return
        }
        
        let parms = VersionParams(version: version).Values
        ApiManager.shared.checkVersion(fromUrl:URL(string: Api.versionUpdate)!,  withParameter: parms) { (isSuccess) in
            
            if isSuccess{
            
                    
                    self.updateView.isHidden = true
                    
                }else{
                    
                    self.updateView.isHidden = false
                    
                }
                
        }
        
    }
    
    
    
    
}

extension HomeViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.searchTableView{
            return self.searchFullArray.count
        }
        return self.homeData?.itemKeys?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        if tableView == searchTableView{
            
          
            let info = searchFullArray[indexPath.row]
            
           
        
            let ProductDetailsScene = ProductDetailsVC.instantiate(fromAppStoryboard: .Main)
            
            if let navigator = self.navigationController {

                ProductDetailsScene.product = info
                ProductDetailsScene.isSearch = true
                navigator.pushViewController(ProductDetailsScene, animated: true)



            }
            
            DispatchQueue.main.async {
                
                self.txtSearch.text = null
                self.searchView.isHidden = true
                self.txtSearch.resignFirstResponder()
                
            }
            
               
           
               
            
        }
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if tableView == searchTableView{
            
            
            let cell = searchTableView.dequeueReusableCell(withIdentifier: searchCell, for: indexPath) as! SearchCell
            
            
            
            let info = self.searchFullArray[indexPath.row]
            
            cell.lblTitle.text = info.productName
            
            if let imgUrl = info.productImage{
                
                let url = URL(string: imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? null)
                let phImage = UIImage(named: ph)
                
                cell.itemPic?.kf.setImage(with: url, placeholder: phImage)
                
                
            }
            
            
            if (indexPath.row == self.searchFullArray.count - 1) {
            
                var count = 10
                let totalCount = Int(self.totalSearchCount ?? "10")!
                if self.searchFullArray.count < totalCount {
                    count = self.searchFullArray.count + 10
                    self.search(product: self.txtSearch.text!, limit: "\(count)")
                    
                }
            }
            
            return cell
            
        }
        
        
        switch self.homeData?.itemKeys?[indexPath.row] {
        case viewType.banners:
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: BannerTableCell, for: indexPath) as! BannersTableViewCell
            
            cell.banners = self.homeData?.banners
            cell.delegate = self
            
            return cell
            
        case viewType.special:
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: specialCell, for: indexPath) as! HorizontalTableCell
            
            cell.productData = self.homeData?.specialOffers?.data
            cell.title = self.homeData?.specialOffers?.titleHead
            cell.isOffer = true
            cell.index = indexPath.row
            cell.subCatId = self.homeData?.specialOffers?.subCategoryId
            cell.isCampaign = self.homeData?.specialOffers?.isCampaign
            cell.delegate = self
           
            return cell
            
        case viewType.productOne:
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: specialCell, for: indexPath) as! HorizontalTableCell
            
            cell.productData = self.homeData?.productsOne?.data
            cell.title = self.homeData?.productsOne?.titleHead
            cell.index = indexPath.row
            cell.subCatId = self.homeData?.productsOne?.subCategoryId
            cell.isCampaign = self.homeData?.productsOne?.isCampaign
            cell.delegate = self
            return cell
            
        case viewType.productTwo:
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: specialCell, for: indexPath) as! HorizontalTableCell
            
            cell.productData = self.homeData?.productsTwo?.data
            cell.title = self.homeData?.productsTwo?.titleHead
            cell.index = indexPath.row
            cell.subCatId = self.homeData?.productsTwo?.subCategoryId
            cell.isCampaign = self.homeData?.productsTwo?.isCampaign
            cell.delegate = self
            return cell
            
        case viewType.productThree:
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: specialCell, for: indexPath) as! HorizontalTableCell
            
            cell.productData = self.homeData?.productsThree?.data
            cell.title = self.homeData?.productsThree?.titleHead
            cell.index = indexPath.row
            cell.subCatId = self.homeData?.productsThree?.subCategoryId
            cell.isCampaign = self.homeData?.productsThree?.isCampaign
            cell.delegate = self
            return cell
        
        case viewType.productFour:
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: specialCell, for: indexPath) as! HorizontalTableCell
            
            cell.productData = self.homeData?.productsFour?.data
            cell.title = self.homeData?.productsFour?.titleHead
            cell.index = indexPath.row
            cell.subCatId = self.homeData?.productsFour?.subCategoryId
            cell.isCampaign = self.homeData?.productsFour?.isCampaign
            cell.delegate = self
            return cell
            
        case viewType.productFive:
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: specialCell, for: indexPath) as! HorizontalTableCell
            
            cell.productData = self.homeData?.productsFive?.data
            cell.title = self.homeData?.productsFive?.titleHead
            cell.index = indexPath.row
            cell.subCatId = self.homeData?.productsFive?.subCategoryId
            cell.isCampaign = self.homeData?.productsFive?.isCampaign
            cell.delegate = self
            return cell
        
        case viewType.productSix:
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: specialCell, for: indexPath) as! HorizontalTableCell
            
            cell.productData = self.homeData?.productsSix?.data
            cell.title = self.homeData?.productsSix?.titleHead
            cell.index = indexPath.row
            cell.subCatId = self.homeData?.productsSix?.subCategoryId
            cell.isCampaign = self.homeData?.productsSix?.isCampaign
            cell.delegate = self
            return cell
        
        case viewType.campaignOne:
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: specialCell, for: indexPath) as! HorizontalTableCell
            
            cell.productData = self.homeData?.campaignsOne?.data
            cell.title = self.homeData?.campaignsOne?.titleHead
            cell.index = indexPath.row
            cell.subCatId = self.homeData?.campaignsOne?.subCategoryId
            cell.isCampaign = self.homeData?.campaignsOne?.isCampaign
            cell.delegate = self
            return cell
            
        case viewType.campaignTwo:
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: specialCell, for: indexPath) as! HorizontalTableCell
            
            cell.productData = self.homeData?.campaignsTwo?.data
            cell.title = self.homeData?.campaignsTwo?.titleHead
            cell.index = indexPath.row
            cell.subCatId = self.homeData?.campaignsTwo?.subCategoryId
            cell.isCampaign = self.homeData?.campaignsTwo?.isCampaign
            cell.delegate = self
            return cell
            
        case viewType.campaignThree:
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: specialCell, for: indexPath) as! HorizontalTableCell
            
            cell.productData = self.homeData?.campaignsThree?.data
            cell.title = self.homeData?.campaignsThree?.titleHead
            cell.index = indexPath.row
            cell.subCatId = self.homeData?.campaignsThree?.subCategoryId
            cell.isCampaign = self.homeData?.campaignsThree?.isCampaign
            cell.delegate = self
            return cell
            
        case viewType.campaignFour:
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: specialCell, for: indexPath) as! HorizontalTableCell
            
            cell.productData = self.homeData?.campaignsFour?.data
            cell.title = self.homeData?.campaignsFour?.titleHead
            cell.index = indexPath.row
            cell.subCatId = self.homeData?.campaignsFour?.subCategoryId
            cell.isCampaign = self.homeData?.campaignsFour?.isCampaign
            cell.delegate = self
            return cell
            
        case viewType.campaignFive:
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: specialCell, for: indexPath) as! HorizontalTableCell
            
            cell.productData = self.homeData?.campaignsFive?.data
            cell.title = self.homeData?.campaignsFive?.titleHead
            cell.index = indexPath.row
            cell.subCatId = self.homeData?.campaignsFive?.subCategoryId
            cell.isCampaign = self.homeData?.campaignsFive?.isCampaign
            cell.delegate = self
            return cell
            
        case viewType.productNine:
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: specialCell, for: indexPath) as! HorizontalTableCell
            
            cell.productData = self.homeData?.productsNine?.data
            cell.title = self.homeData?.productsNine?.titleHead
            cell.index = indexPath.row
            cell.subCatId = self.homeData?.productsNine?.subCategoryId
            cell.isCampaign = self.homeData?.productsNine?.isCampaign
            cell.delegate = self
            return cell
            
        case viewType.productTen:
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: specialCell, for: indexPath) as! HorizontalTableCell
            
            cell.productData = self.homeData?.productsTen?.data
            cell.title = self.homeData?.productsTen?.titleHead
            cell.index = indexPath.row
            cell.subCatId = self.homeData?.productsTen?.subCategoryId
            cell.isCampaign = self.homeData?.productsTen?.isCampaign
            cell.delegate = self
            return cell
            
        case viewType.subCategoryBanner:
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: subBannerCell, for: indexPath) as! SubBannerTableViewCell
            
            cell.bannerData = self.homeData?.subCategoryBanner
            cell.delegate = self
            return cell
            
        case viewType.bannerOne:
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: subBannerCell, for: indexPath) as! SubBannerTableViewCell
            
            cell.bannerData = self.homeData?.HomescreenBannerOne
            cell.delegate = self
            return cell
            
        case viewType.bannerTwo:
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: subBannerCell, for: indexPath) as! SubBannerTableViewCell
            
            cell.bannerData = self.homeData?.HomescreenBannerTwo
            cell.delegate = self
            return cell
            
        case viewType.bannerThree:
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: subBannerCell, for: indexPath) as! SubBannerTableViewCell
            
            cell.bannerData = self.homeData?.HomescreenBannerThree
            cell.delegate = self
            return cell
            
        case viewType.brands:
            
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: BrandTablesCell, for: indexPath) as! BrandTableCell
            
            cell.title = self.homeData?.brands?.titleHead
            cell.brandData = self.homeData?.brands
            cell.index = indexPath.row
            cell.delegate = self
            
            return cell
            
        
        case viewType.subCategroyBottom:
            
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: subBottomCategroyCell, for: indexPath) as! BottomCategoryCell
            
            cell.subCatData = self.homeData?.SubcategoryBottom
            cell.index = indexPath.row
            cell.delegate = self
            
            return cell
            
        case viewType.mainCat:
            let cell = self.homeTableView.dequeueReusableCell(withIdentifier: horizontalMainTableCell, for: indexPath) as! HorizontalCategoryCell
            
            cell.categoryData = self.homeData?.mainCategory?.data
            cell.title = self.homeData?.mainCategory?.titleHead
            cell.delegate = self
            return cell
            
        case .none:
            break
        case .some(_):
            break
        }
        
        
        let cell = UITableViewCell()
        
        return cell
        
    }
    
    
    
}


extension HomeViewController:subBannerDelegate{
    func tappedSubBanner(data: SubBannersData) {
    
        
        if let isCampain = data.isCampaign {
            
            if isCampain == 1 {
                
                let ListScene = ProductListingViewController.instantiate(fromAppStoryboard: .Main)
                
                ListScene.isCampaign = true
                ListScene.isSubCategroy = false
                ListScene.categoryTitle = data.titleHead
                
                guard let id = data.campaignId else {
                    return
                }
                
                ListScene.subCatId = id
                if let navigator = self.navigationController {
                    
                    navigator.pushViewController(ListScene, animated: true)
                }
                
                
            }else{
                
                
                guard let id = data.subCategoryId else{
                    
                    return
                }
                
                let ListScene = ListingWithBannerViewController.instantiate(fromAppStoryboard: .Main)
                if let navigator = self.navigationController {
                    
                    
                    ListScene.brandTitle = data.titleHead
                    ListScene.bannerUrl = data.data
                    ListScene.Id = id
                    navigator.pushViewController(ListScene, animated: true)
                    
                }
                
                
                
                
            }
            
       

            
            
        }else{
        
        
        guard let id = data.subCategoryId else{
            
            return
        }
        
        let ListScene = ListingWithBannerViewController.instantiate(fromAppStoryboard: .Main)
        if let navigator = self.navigationController {
            
            
            ListScene.brandTitle = data.titleHead
            ListScene.bannerUrl = data.data
            ListScene.Id = id
            navigator.pushViewController(ListScene, animated: true)
            
        }
        
        }
    }
    

}


extension HomeViewController:BrandDelegate,PagerBannerDelegate{
    func bannerTapped(data: BannerData) {
        
        if data.isCategoryBanner == "0" && data.ProductID != "0"{
            
            let ProductDetailsScene = ProductDetailsVC.instantiate(fromAppStoryboard: .Main)
            
            if let navigator = self.navigationController {
                
                
                
                let prod = Product.init(productId: data.ProductID, id: data.ProductID, brandName: null, articleID: null, productName: data.title, offerPrice: null, offerPercentage: null, qtyWeight: null, weight: null, descriptions: null, isFavourite: null, productImage: null, itemCondition: null, price: null, groceryProductPrice: null, qty: null, productPrice: null)

                ProductDetailsScene.product = prod
                ProductDetailsScene.isSearch = false
                navigator.pushViewController(ProductDetailsScene, animated: true)

            }
            
            
        }else if data.isCampaign == "1" {
            
            
            let ListScene = ProductListingViewController.instantiate(fromAppStoryboard: .Main)

                
            ListScene.isCampaign = true
            ListScene.isSubCategroy = false
           // ListScene.categoryTitle = heading
            
            guard let id = data.campaignId else{
                return
            }
                
            ListScene.subCatId = id
                       
            if let navigator = self.navigationController {
                           
            navigator.pushViewController(ListScene, animated: true)
            }
          
            
        }
        
 
        else{
            
            
            guard let id = data.subCategoryId else{
                
                return
            }
            
            let ListScene = ListingWithBannerViewController.instantiate(fromAppStoryboard: .Main)
            if let navigator = self.navigationController {
                
                
                ListScene.brandTitle = data.title
                ListScene.bannerUrl = data.bannerImage
                ListScene.Id = id
                navigator.pushViewController(ListScene, animated: true)
                
            }
            
            
            
        }
        

        
    }
    

    
    func tappedBrand(brandCode:String, brandUrl:String, name:String) {
        
        
        let BrandScene = BrandLisitingViewController.instantiate(fromAppStoryboard: .Main)
        if let navigator = self.navigationController {
            
            
            
            BrandScene.brandId = brandCode
            BrandScene.brandTitle = name
            BrandScene.bannerUrl = brandUrl
            navigator.pushViewController(BrandScene, animated: true)
            
        }

        
    }
    
   
}


extension HomeViewController:BottomCategoryDelegate{
    func tappedCategory(catId: String, brandUrl: String, name: String) {
        
        
        let ListScene = ProductListingViewController.instantiate(fromAppStoryboard: .Main)
    
        ListScene.subCatId = catId
        ListScene.isCampaign = false
        ListScene.isSubCategroy = true
        ListScene.categoryTitle = name
         
                   if let navigator = self.navigationController {
                       
                       navigator.pushViewController(ListScene, animated: true)
                   }
        
    }
    
    
}


extension HomeViewController:MainCategoryDelegate{
    func tappedMainCategory(category: mainCategoryData) {
    
        guard let catId = category.id else{
        
            return
        }
            
            guard let title = category.mainCategory else{
                return
                
            }
       
            let CategoryScene = SubCategoryViewController.instantiate(fromAppStoryboard: .Main)
            if let navigator = self.navigationController {
                
                CategoryScene.mainCatID = "\(catId)"
                CategoryScene.categoryTitle = title
                navigator.pushViewController(CategoryScene, animated: true)
                
            }

 
    }
    
    
    
}

extension HomeViewController:HorizontalCellDelegate{
    func viewAllList(at index: Int, isCampaign: Int, id: String, isOffer:Bool, heading:String) {
    
        
        let ListScene = ProductListingViewController.instantiate(fromAppStoryboard: .Main)
        
        if isCampaign == 1{
            
        ListScene.isCampaign = true
        ListScene.isSubCategroy = false
        ListScene.categoryTitle = heading
            
        }else if isOffer{
            
        ListScene.isOffer = true
        ListScene.isSubCategroy = false
        ListScene.categoryTitle = heading
            
        }else{
            
        ListScene.isCampaign = false
        ListScene.isSubCategroy = true
        ListScene.categoryTitle = heading
        }
        
        ListScene.subCatId = id
                   
                   if let navigator = self.navigationController {
                       
                       navigator.pushViewController(ListScene, animated: true)
                   }
        
    }
    
   
    
    func tappedProduct(product: Product) {
        
        let ProductDetailScene = ProductDetailsVC.instantiate(fromAppStoryboard: .Main)
        if let navigator = self.navigationController {
            
            ProductDetailScene.product = product
            navigator.pushViewController(ProductDetailScene, animated: true)
            
        }
        
    }
    
    func addProduct(product: Product) {
        
        if let productId = product.id{
            
            if let id = Defaults.string(forKey: "customerId"){
                
                self.customerId = id
            }
            if let uid = Defaults.string(forKey:UID){
                
                self.uuidToken = uid
            }
            
            if let weight = product.weight{
                
                self.weight = weight
            }
            
            
            let param = CartParams(customerId:self.customerId,uuId:self.uuidToken,weight:self.weight,quantity:"1",productId:productId).Values
            
            self.addToCart(params:param)
        }else{
            
            if let productId = product.productId{
                
                if let id = Defaults.string(forKey: "customerId"){
                    
                    self.customerId = id
                }
                if let uid = Defaults.string(forKey:UID){
                    
                    self.uuidToken = uid
                }
                
                if let weight = product.weight{
                    
                    self.weight = weight
                }
                
                
                let param = CartParams(customerId:self.customerId,uuId:self.uuidToken,weight:self.weight,quantity:"1",productId:productId).Values
                
                self.addToCart(params:param)
            }
            
            
        }
        
        
    }
    
    
    
}





extension HomeViewController:CLLocationManagerDelegate{
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        
 
         
         
     
//                              if let location = locations.last {
//
//                                let placeFields = GMSPlaceField(rawValue:
//                                  GMSPlaceField.name.rawValue | GMSPlaceField.formattedAddress.rawValue
//                                )!
//                                placesClient.findPlaceLikelihoodsFromCurrentLocation(withPlaceFields: placeFields) { [weak self] (placeLikelihoods, error) in
//                                  guard let strongSelf = self else {
//                                    return
//                                  }
//
//                                  guard error == nil else {
//
//                                    return
//                                  }
//
//                                  guard let place = placeLikelihoods?.first?.place else {
//
//                                    return
//                                  }
//
//                                  // print(place)
//
//                                    if let placeTitle = place.name{
//
//                                        self?.lblLocation.text = placeTitle
//
//                                        defaults.set(placeTitle, forKey: "currentLocation")
//
//                                    }
//
//
//
//
//                                }
      
                                self.locationManager?.stopUpdatingLocation()
                                self.locationManager?.delegate = nil

   //   }
    
    }
    
    
    
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
       
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("location manager authorization status changed")
        
        switch status {
        case .authorizedAlways:
            
            locationManager?.startUpdatingLocation()
            locationManager?.delegate = self
            
           // retriveCurrentLocation()
            print("user allow app to get location data when app is active or in background")
        case .authorizedWhenInUse:
            
            locationManager?.startUpdatingLocation()
            locationManager?.delegate = self
            
            print("user allow app to get location data only when app is active")
        case .denied:
            
             locationManager?.requestAlwaysAuthorization()
            
            print("user tap 'disallow' on the permission dialog, cant get location data")
        case .restricted:
            
           
            print("parental control setting disallow location data")
        case .notDetermined:
            
            
            print("the location permission dialog haven't shown before, user haven't tap allow/disallow")
        default:
            break
        }
    }
    
    
}

extension HomeViewController:UITextFieldDelegate{
    
 
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        
        if self.txtSearch.text?.count ?? 0 > 2{
            
            let count = 10
            
            self.search(product:self.txtSearch.text!, limit: "\(count)")
            return true
        }
        return true
    }
    

    
}
