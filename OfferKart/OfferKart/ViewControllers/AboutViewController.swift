//
//  AboutViewController.swift
//  OfferKart
//
//  Created by Sajin M on 28/01/2021.
//

import UIKit
import MessageUI

class AboutViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.backNavigation()
    }
    
    @IBAction func instaPressed(_ sender: Any) {
        openInstagram()
    }
    
    @IBAction func mailPressed(_ sender: Any) {
        sendEmail()
    }
    
    @IBAction func fbPressed(_ sender: Any) {
        
        openFacebook()
    }
    
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["support@offerkart.com"])
           

            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }

    
    func openFacebook() {
             guard let url = URL(string: "fb://profile/100164462096376")  else { return }
             if UIApplication.shared.canOpenURL(url) {
                 if #available(iOS 10.0, *) {
                     UIApplication.shared.open(url, options: [:], completionHandler: nil)
                 } else {
                     UIApplication.shared.openURL(url)
                 }
             }
         }
    

    
         func openInstagram() {
             guard let url = URL(string: "https://instagram.com/offerkartuae/?igshid=100h1ndi0pz6z")  else { return }
             if UIApplication.shared.canOpenURL(url) {
                 if #available(iOS 10.0, *) {
                     UIApplication.shared.open(url, options: [:], completionHandler: nil)
                 } else {
                     UIApplication.shared.openURL(url)
                 }
             }
         }

    

}

extension AboutViewController:MFMailComposeViewControllerDelegate{
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
}
