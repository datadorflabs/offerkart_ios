//
//  LoadFromCategoryViewController.swift
//
//
//  Created by Sajin M on 28/06/2020.
//  Copyright © 2020 Daradorf. All rights reserved.
//

import UIKit
import BadgeSwift

class LoadFromCategoryViewController: BaseViewController {
    
  
    var groupId:String?
    var category_Title : String?
    var flagKey:String?
    var mainCatCount : Int = 0
    var movetoIndex : Int = 0
    var customerId:String = "0"
    var groceryId:String = "2"
    var page:Int = 1
    var products:ProductListModel?
    var isBanner:Bool? = false
    var totalSearchCount:String?
    var searchFullArray = [Product]()
    var isSelectedIndex:Int = 0
    var isLoadMore:Bool = true
    
    @IBOutlet weak var noProductImgView: UIImageView!
  
    @IBOutlet weak var parentCollectionView: UICollectionView!
    @IBOutlet weak var categoryTitle: UILabel!
    @IBOutlet weak var titleListView: UICollectionView!
    @IBOutlet weak var sortTableView: UITableView!
    
    @IBOutlet weak var transparentView: UIView!
    @IBOutlet weak var sortView: UIView!
    @IBOutlet weak var btnSortCancel: UIButton!
    @IBOutlet weak var lblBadge: BadgeSwift!
    @IBOutlet weak var showSearchView: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var searchTableView: UITableView!

    @IBOutlet weak var searchView: UIView!
    
 
    var sortArray : [Sort] = [Sort(title:"A -- Z",selected:false),Sort(title:"Price -- Low to High",selected:false),Sort(title:"Price -- High to Low",selected:false)]
    var sortKey = ["","ASC","DESC"]
    
    var sortType = ""
    var currentCatId = ""
    var currentIndex = 0
    
    var categoryList:[CategoryListData]?
    var productListData:[[Product]]?
   
    
    
    private var selectedSort: Int? {
        didSet {
            sortTableView.reloadData()
        }
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.noProductImgView.isHidden = true
        self.showSearchView.isHidden = true
        self.searchView.isHidden = true
       // self.txtSearch.delegate = self
        self.transparentView.isHidden = true
        self.btnSortCancel.isHidden = true
        self.lblBadge.isHidden = true
        
      
        
        if let custId = Defaults.string(forKey: "customerId"){
                   
                   self.customerId = custId
               }
               
        
        
        if let category = self.category_Title{
            
            self.categoryTitle.text = category
            
        }
        
        
        if isConnected(){
            
            if let id = groupId{
                
                getCategories(groupId: id)
                

            }
            
            
        }
        
        self.sortTableView.delegate = self
        self.sortTableView.dataSource = self
        
    }
    
    
  
    
    override func viewWillAppear(_ animated: Bool) {
        
       
        
        if let count = Defaults.string(forKey: "badge"){
            
            if count != "0"{
                
                self.lblBadge.isHidden = false
                self.lblBadge.text = count
                
            }else{
                 self.lblBadge.isHidden = true
                
            }
            
    
        }else{
            
             self.lblBadge.isHidden = true
            
        }
        
}
    
    
    // MARK:- Search
    
//
//    func search(product:String,limit:String){
//
//
//
//
//        self.STProgress.show()
//
//
//        let params = SearchParam(searchKey: product, limit: limit).Values
//
//        ApiRequest.searchItem(withParameter: params) { (isSuccess,message) in
//
//            self.STProgress.dismiss()
//
//            if isSuccess {
//
//
//                if let info = self.ApiRequest.SearchResponse?.products{
//
//
//
//                    if info.count > 0{
//
//                        self.totalSearchCount = self.ApiRequest.SearchResponse?.totalCount
//
//                        self.searchFullArray = info
//                        self.searchTableView.delegate = self
//                        self.searchTableView.dataSource = self
//                        self.searchTableView.reloadData()
//                        self.searchView.isHidden = false
//
//
//                    }
//
//
//
//                }
//
//
//
//
//            }
//
//        }
//
//
//    }
    
    
    @IBAction func cartPressed(_ sender: Any) {
        
        if Defaults.value(forKey: "customerId") != nil{
            
                  if let id = Defaults.string(forKey: "customerId"){
                      
                      self.customerId = id
                      
                      let CartScene = CartViewController.instantiate(fromAppStoryboard: .Main)
                                 
                                 if let navigator = self.navigationController {
                                     
                                     navigator.pushViewController(CartScene, animated: true)
                                 }
                                 
                                 
                      
                  }
                  
              }else{
                  
                  
                  self.STAlert.alert(title:AppName , message: pleaseLogin )
                      .action(.destructive("Login")){
                          
                          let loginScene = LoginViewController.instantiate(fromAppStoryboard: .Main)
                          
                          if let navigator = self.navigationController {
                              
                              navigator.pushViewController(loginScene, animated: true)
                              
                              
                              
                          }
                          
                          
                  }.action(.cancel("Cancel"))
                      .show(on: self)
                  
                  
              }
              
    
        
    }
    
    
    
    @IBAction func searchCancelPressed(_ sender: Any) {
        
         self.txtSearch.text = null
         self.showSearchView.isHidden = true
         self.searchView.isHidden = true
        
        
    }
    
    @IBAction func searchPressed(_ sender: Any) {
        self.showSearchView.isHidden = false
        
    }
    
    
    
    @IBAction func searchClosePressed(_ sender: Any) {
        
        self.txtSearch.text = null
        self.showSearchView.isHidden = true
        self.searchView.isHidden = true
    }
    
    
    @IBAction func sortPressed(_ sender: Any) {
        
       
           self.transparentView.isHidden = false
           self.btnSortCancel.isHidden = false
              
              let screenSize = UIScreen.main.bounds.size
        self.sortView.frame = CGRect(x: 0, y: screenSize.height, width: screenSize.width, height: height)

        self.transparentView.alpha = 0
              
        self.transparentView.backgroundColor = UIColor.clear
                            
              UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
                
                self.transparentView.alpha = 1
                
                self.transparentView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.6698416096)
                   self.sortView.frame = CGRect(x: 0, y: screenSize.height - height, width: screenSize.width, height: height)
              }, completion: nil)
        
        
        
        
        
    }
    
    
    
    
    private func updateSelectedIndex(_ index: Int) {
        selectedSort = index
    }

    
    @IBAction func cancelSortPressed(_ sender: Any) {
        
        onClickTransparentView()
        
    }
    
    
    
    
         func onClickTransparentView() {
            
    
            let screenSize = UIScreen.main.bounds.size
           
            

            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
                self.transparentView.alpha = 0
                //self.transparentView.backgroundColor = UIColor.clear
                 self.sortView.frame = CGRect(x: 0, y: screenSize.height, width: screenSize.width, height: height)
                 
            }, completion: nil)
           // self.transparentView.isHidden = true
        }
        
 
//    func getSubCategoryData(id:String){
//
//        self.getProductList(id:id,page:"\(self.page)") { success, data in
//
//                                                 self.STProgress.dismiss()
//
//                                                if success{
//
//                                                    self.productListData = [[OfferProductData]]()
//                                                    self.productListData?.insert(data, at: 0)
//                                                    self.parentCollectionView.delegate = self
//                                                    self.parentCollectionView.dataSource = self
//                                                    self.parentCollectionView.reloadData()
//
//                                                }else{
//
//                                                    self.showAlert(message: wentWrong)
//
//                                                }
//
//                                            }
//
//
//
//
//    }
    
    

    func getCategories(groupId:String){

        self.STProgress.show()

        let catParam = CategoryParam(groupId:groupId).Values

        self.ApiRequest.getCategoryList(withParameter:catParam) { (isSuccess,message) in

           // self.STProgress.dismiss()

            if isSuccess {

                if let info = self.ApiRequest.CategoryListResponse{

                    self.categoryList = info.arrSubCategory


                    if self.categoryList?.count ?? 0 > 0 {

                        self.productListData = [[Product]](repeating: [], count: self.categoryList!.count)


                        self.titleListView.delegate = self
                        self.titleListView.dataSource = self
                        self.titleListView.reloadData()


                        if  let data = info.arrSubCategory?[0]{

                            self.getProductList(id:"\(data.id)",page:"\(self.page)") { success, data in

                                                                     self.STProgress.dismiss()

                                                                    if success{


                                                                        self.productListData?.insert(data, at: 0)
                                                                        self.parentCollectionView.delegate = self
                                                                        self.parentCollectionView.dataSource = self
                                                                        self.parentCollectionView.reloadData()

                                                                    }else{

                                                                        self.showAlert(message: wentWrong)

                                                                    }

                                                                }



                        }





                   //     let semaphore = DispatchSemaphore(value: 0)
                     //   let dispatchQueue = DispatchQueue.global(qos: .background)

//                        dispatchQueue.async {
//
//                            for data in info.arrSubCategory! {
//
//
//                                    self.getProductList(id:"\(data.id)") { success, data in
//
//                                        if success{
//
//                                             self.productListData.append(data)
//                                        }
//                                        semaphore.signal()
//
//                                    }
//                                      semaphore.wait()
//
//
//                                }
//
//                             self.STProgress.dismiss()
//
//                            DispatchQueue.main.async {
//
//                                if self.categoryList?.count == self.productListData.count{
//
//                                self.parentCollectionView.delegate = self
//                                self.parentCollectionView.dataSource = self
//                                self.parentCollectionView.reloadData()
//
//
//
//                                             }
//                            }
//
//
//
//
//
//
//                        }



                    }


                }


            }else{

                self.showAlert(message: wentWrong)


            }
        }

    }
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.noProductImgView.isHidden = true
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for cell: UICollectionViewCell in parentCollectionView.visibleCells {
            let indexPath: IndexPath? = parentCollectionView.indexPath(for: cell)
            
            self.isLoadMore = false
            movetoIndex = (indexPath?.row)!
            
            if scrollView == self.parentCollectionView{
                
                     let indexpath = IndexPath(item: movetoIndex, section: 0)
                       
                
                if let index = indexPath?.row{
                    
                    if (self.productListData?[index].count ?? 0) > 0{
                        self.noProductImgView.isHidden = true
                                   callProductList(value: index)
                              
                              }else{
                                
                               // self.noProductImgView.isHidden = false
                                  self.page = 1
                                  self.getCategoryProducts(index: index)
                                  
                              }
                    
                
                    titleListView.scrollToItem(at: indexpath, at:.centeredHorizontally, animated: true)
                    self.isSelectedIndex = index
                    self.titleListView.reloadData()
                }
                   
                
            }

                
            

        }
    }

    
    
    func callProductList (value:Int)
    {
        
       let indexPath = IndexPath(item: value, section: 0)
        parentCollectionView.scrollToItem(at: indexPath, at: .left, animated: false)
       // parentCollectionView.setContentOffset(.zero, animated: false)
        
    }
    
    
    
    func getCategoryProducts(index:Int){


        if let info = self.categoryList?[index]{


            self.getProductList(id:"\(info.id)",page:"\(self.page)") { success, data in

                                                     self.STProgress.dismiss()

                                                    if success{

                                                        self.noProductImgView.isHidden = true
                                                        self.productListData?.insert(data, at: index)


                                                        DispatchQueue.main.async {

                                                        self.parentCollectionView.delegate = self
                                                        self.parentCollectionView.dataSource = self
                                                        self.parentCollectionView.reloadData()
                                                        self.callProductList(value: index)

                                                        }


                                                    }else{
                                                        
                                                        if !self.isLoadMore{
                                                            
                                                            self.noProductImgView.isHidden = false
                                                        }
                                                   
                                                        self.callProductList(value: index)
                                                        self.showAlert(message: wentWrong)

                                                    }

                                                }



        }


    }
    
    
    
    
    func AddCategoryProducts(id:String,limit:String){



            self.getProductList(id:id,page:limit) { success, data in

                                                     self.STProgress.dismiss()

                                                    if success{

                                                        for item in data {

                                                            self.productListData?[self.isSelectedIndex].append(item)


                                                        }


                                                        DispatchQueue.main.async {

                                                        self.parentCollectionView.delegate = self
                                                        self.parentCollectionView.dataSource = self
                                                        self.parentCollectionView.reloadData()
                                               

                                                        }




                                                    }else{

                                                        self.showAlert(message: wentWrong)

                                                    }

                                                }






    }
    
    
  
    
    func getProductList(id:String,page:String, completion: @escaping ((Bool,[Product]) -> Void)){


        self.currentCatId = id

        let ProductParams = SubCatProductParams(subCategoryId:id, customerId:self.customerId, limit:page, sortKey:self.sortType).Values



        self.ApiRequest.getMainCategoryProducts(withParameter:ProductParams) { (isSuccess,message) in

            self.STProgress.dismiss()

            if isSuccess {

                self.noProductImgView.isHidden = true
                if let info = self.ApiRequest.MainCategoryProductsResponse{


                    if info.products?.count ?? 0 > 0{

                        self.products = info

                        completion(true,info.products!)
                      
                    }
                }


            }else{
                
                if !self.isLoadMore{
                    self.noProductImgView.isHidden = false
                }

               
//                if self.productListData?.count ?? 0 <= 0{
//
//                    self.noProductImgView.isHidden = false
//                }
               
               
                //self.STProgress.dismiss()
                //self.showAlert(message: message)


            }
        }


    }
    
    
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
        
    }
    
    
    
}


extension LoadFromCategoryViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if  collectionView == titleListView{
            
            return categoryList?.count ?? 0
            
        }
        
        if  collectionView == parentCollectionView{
            
            if self.isBanner ?? false {
                
                return 1
                
            }else{
                
                 return categoryList?.count ?? 0
                
            }
            
            
           
            
        }
        
        
        return 0
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView == titleListView{
            
           // self.STProgress.show()
            
            if (self.productListData?[indexPath.row].count ?? 0) > 0{
                
                
                self.noProductImgView.isHidden = true
                 callProductList(value: indexPath.row)
               // self.STProgress.dismiss()
                
            
            }else{
                
               // self.noProductImgView.isHidden = false
                self.STProgress.show()
                self.page = 1
                self.getCategoryProducts(index: indexPath.row)
                
            }
            
        
            self.isSelectedIndex = indexPath.row
            self.titleListView.reloadData()
            self.sortTableView.reloadData()
            self.isLoadMore = false
            
            callProductList(value: indexPath.row)
        }
        
     
        
    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if collectionView.tag == 0 {

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: titleCell, for: indexPath) as! TitleCollectionCell


            if let info =  self.categoryList?[indexPath.row]{

                cell.tag = indexPath.row

                cell.lblTitle.text = info.category

                if isSelectedIndex == indexPath.row {

                    cell.isSelected = true

                }else{
                    cell.isSelected = false

                    cell.viewHighlight.isHidden = true
                }


            }


            return cell

        }

        if collectionView == parentCollectionView{

            let cell = self.parentCollectionView.dequeueReusableCell(withReuseIdentifier: subCategorySubCell, for: indexPath) as! SubCategoryCell

            if let count = products?.totalCount{

                cell.totalProductCount = count
            }



            cell.isLoadMore = self.isLoadMore
            cell.productData = self.productListData?[indexPath.row]
            cell.delegate = self


            return cell
        }


        let  cell = UICollectionViewCell()
        return cell

    }
    
    

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        
        if collectionView == self.titleListView
        {
            
        

                let itemSizeWidth = (collectionView.frame.width / 3)
        
                           return CGSize(width: itemSizeWidth, height: collectionView.frame.height)



            }
//
//
//
//            let itemSizeWidth = (collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right + 10)) / 3
//            return CGSize(width: itemSizeWidth, height: collectionView.frame.height)
//
//        }
        
        
        if collectionView == self.parentCollectionView
        {
            
            
            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
            
        }
        
       
        return CGSize(width: 0, height: 0)
        
    }
    
}

extension LoadFromCategoryViewController:SubcategoryProductDelegate{
    
    func getProductDetails(product: Product) {
        
        let ProductDetailsScene = ProductDetailsVC.instantiate(fromAppStoryboard: .Main)
        
        if let navigator = self.navigationController {
            
            ProductDetailsScene.product = product
            
            if let title = product.productName{
                
                ProductDetailsScene.title = title
            }
                
         
            navigator.pushViewController(ProductDetailsScene, animated: true)
            
            
            
        }
        
        
        
    }
    

    func loadMoreProduts() {


        self.isLoadMore = true
        self.page = self.page + 1

        if isBanner ?? false{

           // self.AddCategoryProducts(id:self.currentCatId,limit:"\(self.page)")

        }else{

           if let data = self.categoryList?[self.isSelectedIndex] {

            self.currentCatId = "\(data.id)"
            self.AddCategoryProducts(id:self.currentCatId,limit:"\(self.page)")

                   }

        }



    }

}


extension LoadFromCategoryViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if tableView == searchTableView {

            return searchFullArray.count

        }

        return self.sortArray.count
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {



        if tableView == searchTableView{

            self.searchView.isHidden = true
            self.showSearchView.isHidden = true
            self.txtSearch.text = null

            let info = searchFullArray[indexPath.row]


            let ProductDetailsScene = ProductDetailsVC.instantiate(fromAppStoryboard: .Main)

            if let navigator = self.navigationController {


//                ProductDetailsScene.searchData = info
//                ProductDetailsScene.isSearch = true
//                ProductDetailsScene.similarProducts = searchFullArray
//                navigator.pushViewController(ProductDetailsScene, animated: true)



            }



        }else{


         updateSelectedIndex(indexPath.row)
         onClickTransparentView()

        self.page = 1
        self.sortType = sortKey[indexPath.row]

     

            self.productListData?.remove(at: self.isSelectedIndex)
            self.getCategoryProducts(index:  self.isSelectedIndex)

        }

   

    }



    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = self.sortTableView.dequeueReusableCell(withIdentifier: sortCell, for: indexPath) as! SortTableViewCell

        if tableView == searchTableView{


            let cell = searchTableView.dequeueReusableCell(withIdentifier: searchCell, for: indexPath) as! SearchCell



            let info = self.searchFullArray[indexPath.row]

            cell.lblTitle.text = info.productName

            if let imgUrl = info.productImage{

                let url = URL(string: imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? null)
                let phImage = UIImage(named: ph)

                cell.itemPic?.kf.setImage(with: url, placeholder: phImage)


            }


            if (indexPath.row == self.searchFullArray.count - 1) {
                var count = 10
                let totalCount = Int(self.totalSearchCount ?? "10")!
                if self.searchFullArray.count < totalCount {
                    count = self.searchFullArray.count + 10
                    //self.search(product: self.txtSearch.text!, limit: "\(count)")

                }
            }

            return cell


        }


        let sort = sortArray[indexPath.row]

        let currentIndex = indexPath.row

        let selected = currentIndex == selectedSort

        cell.lblTitle.text = (sort.title)

        cell.isSelected(selected)



            return cell


    }




}
extension LoadFromCategoryViewController:UITextFieldDelegate{
    
 
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        
        if self.txtSearch.text?.count ?? 0 > 2{
            
            let count = 10
            
            //self.search(product:self.txtSearch.text!, limit: "\(count)")
            
        }
        return true
    }
    
    
    
}

