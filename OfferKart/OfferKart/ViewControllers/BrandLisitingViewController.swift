//
//  BrandLisitingViewController.swift
//  OfferKart
//
//  Created by Sajin M on 13/01/2021.
//

import UIKit
import BadgeSwift

class BrandLisitingViewController: BaseViewController {

    
    @IBOutlet weak var lblBadge: BadgeSwift!
    @IBOutlet weak var bannerImg: CurvedImage!
    
    @IBOutlet weak var productCollectionView: UICollectionView!
    var offerProducts:Product?
    var ListProducts:[Product]?
    

    var brandTitle:String?
    var bannerUrl:String?
    
    var customerId:String? = "0"
    var index:Int? = 0
    var sortType = ""
    var page = 1
    var brandId:String?

     
     @IBOutlet weak var sortTableView: UITableView!
     
     var sortArray : [Sort] = [Sort(title:"A -- Z",selected:false),Sort(title:"Price -- Low to High",selected:false),Sort(title:"Price -- High to Low",selected:false)]
     
       var sortKey = ["","ASC","DESC"]
       
     private var selectedSort: Int? {
            didSet {
                sortTableView.reloadData()
            }
        }
    
    @IBOutlet weak var transparentView: UIView!
    @IBOutlet weak var sortView: UIView!
    @IBOutlet weak var btnSortCancel: UIButton!
    
    @IBOutlet weak var lblTitle: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        
       initialSetup()
        
        // Do any additional setup after loading the view.
    }
    
    
    func initialSetup(){
        
        self.transparentView.isHidden = true
        self.btnSortCancel.isHidden = true
        
        if let custId = Defaults.string(forKey: "customerId"){
            
             self.customerId = custId
        }
        
        
       
        if let title = self.brandTitle{
            
            self.lblTitle.text = title
       
        }
        
        if let imgUrl = self.bannerUrl {
            
            let url = URL(string: imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? null)
                            let phImage = UIImage(named: ph)
        
        bannerImg?.kf.setImage(with: url, placeholder: phImage)
        
    
                       }
        
        guard let id = self.brandId else{
            
            return
        }
        
        
        getProducts()

        

       
        self.sortTableView.delegate = self
        self.sortTableView.dataSource = self
   
        
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
     
         if let custId = Defaults.string(forKey: "customerId"){
         
         if let count = Defaults.string(forKey: "badge"){
             
             if count != "0"{
                 
                 self.lblBadge.isHidden = false
                 self.lblBadge.text = count
                 
             }else{
                  self.lblBadge.isHidden = true
                 
             }
             
            
          }
         }else{
            
             self.lblBadge.isHidden = true
            
        }
         
     }
    
    
    @IBAction func sortPressed(_ sender: Any) {
        
       
           self.transparentView.isHidden = false
           self.btnSortCancel.isHidden = false
              
              let screenSize = UIScreen.main.bounds.size
        self.sortView.frame = CGRect(x: 0, y: screenSize.height, width: screenSize.width, height: height)

        self.transparentView.alpha = 0
              
        self.transparentView.backgroundColor = UIColor.clear
                            
              UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
                
                self.transparentView.alpha = 1
                
                self.transparentView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.6698416096)
                   self.sortView.frame = CGRect(x: 0, y: screenSize.height - height, width: screenSize.width, height: height)
              }, completion: nil)
        
        
        
        
        
    }
    
    
    
    
    private func updateSelectedIndex(_ index: Int) {
        selectedSort = index
    }

    
    @IBAction func cancelSortPressed(_ sender: Any) {
        
        onClickTransparentView()
        
    }
    
    
    
    
         func onClickTransparentView() {
            
    
            let screenSize = UIScreen.main.bounds.size
           
            

            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
                self.transparentView.alpha = 0
                //self.transparentView.backgroundColor = UIColor.clear
                 self.sortView.frame = CGRect(x: 0, y: screenSize.height, width: screenSize.width, height: height)
                 
            }, completion: nil)
           // self.transparentView.isHidden = true
        }
    
    
    @IBAction func backPressed(_ sender: Any) {
        self.backNavigation()
    }
    

    
    
   @IBAction func cartPressed(_ sender: Any) {
        
    let CartScene = CartViewController.instantiate(fromAppStoryboard: .Main)
    
    if let navigator = self.navigationController {
        
        navigator.pushViewController(CartScene, animated: true)
    }
    

        
    }
    
    
    
    
    func getProducts(){
        
       
                
        guard let brandId = self.brandId else {
            
            return
        }
        
        self.STProgress.show()
                       
        let params = BrandParameters(sortKey: self.sortType, page: "\(self.page)", brandId:brandId).Values
   
         self.STProgress.dismiss()
                       
                              
                              ApiRequest.getBrandProducts(withParameter: params) { (isSuccess,message) in
                           
                                              if isSuccess {
                                                  
                                                  if let data = self.ApiRequest.MainCategoryProductsResponse {
                                                      
                                                    self.ListProducts = data.products
                                                    
                                                    if data.products?.count ?? 0 > 0{
                                                        
                                                        self.productCollectionView.delegate = self
                                                        self.productCollectionView.dataSource = self
                                                        self.productCollectionView.reloadData()
                                                        
                                                        
                                                    }
                                                      
                                                  }
                                              
                                         
                                             
                                               
                                              }
                
                }
                
                
             
             
         }

    

}

extension BrandLisitingViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    

        return self.ListProducts?.count ?? 0
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if let product = self.ListProducts?[indexPath.row]{
    
  
        let ProductDetailsScene = ProductDetailsVC.instantiate(fromAppStoryboard: .Main)
        
        if let navigator = self.navigationController {
            
            ProductDetailsScene.product = product
            
            if let title = product.productName{
                
                ProductDetailsScene.title = title
            }
                
         
            navigator.pushViewController(ProductDetailsScene, animated: true)
            
            
        }
        }
        
        
        
    }
      
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: horizontalCollectionCell, for: indexPath) as! HorizontalCollectionCell
        
    
        if let info = self.ListProducts?[indexPath.row]{
            
            if let title = info.productName{
                
                cell.lblProductName.text = title
                
            }
            
            if let weightType = info.weight{
                
                if let weightQty = info.qtyWeight{
                    
                    cell.lblWeight.text = weightQty + null + weightType
                }
                
            }
            
            
            if let off = info.offerPercentage {
                
                if Double(off)! > 0.0{
                    
                    cell.lblOff.text = off + "%"
                    cell.offerTagView.isHidden = false
                    cell.lblPrice.isHidden = false
                    

                    if let price = info.price {
                                    
                                    let priceStr =  currency + price
                                    
                                     cell.lblPrice.attributedText = priceStr.strikeThrough()
                                    
                                }
                    
                    if let offerPrice = info.offerPrice{
                                       
                                       cell.lblOfferPrice.text =  currency + offerPrice
                                       
                                   }
                    
                }else{
                    
                    cell.offerTagView.isHidden = true
                   
                    if let offerPrice = info.offerPrice{
                        
                        if Double(offerPrice)! > 0.0{
                            
                            cell.lblPrice.isHidden = false
                        
                        if let price = info.price{
                            
                            let priceStr =  currency + price
                            
                            cell.lblPrice.attributedText = priceStr.strikeThrough()
                            cell.lblOfferPrice.text = currency + offerPrice
                            
                        }
                            
                        }else{
                            
                            cell.lblPrice.isHidden = true
                            
                            if let price = info.price{
                            
                                cell.lblOfferPrice.text = currency + price
                            }
                            
                            
                        }
                        
                    }else{
                        
                        if let price = info.price{
                            
                            cell.lblOfferPrice.text = currency + price
                        }
                        
                        
                        
                    }
                    
              
                }
                
                
            }else{
                
                
                
                if let offerPrice = info.offerPrice{
                    
                    if let price = info.price{
                        
                        let priceStr =  currency + price
                        
                        cell.lblPrice.attributedText = priceStr.strikeThrough()
                        cell.lblOfferPrice.text = currency + offerPrice
                        
                    }
                    
                }else{
                    
                    if let price = info.price{
                        
                        cell.lblOfferPrice.text = currency + price
                    }
                    
                    cell.offerTagView.isHidden = true
                    cell.offerTagView.isHidden = true
                    
                }
                
                
                

                
            }
            

//                if let weight = info.weight {
//
//                    if let weightQty = info.qtyWeight{
//
//                        let weightStr = weightQty + " " + weight
//
//                        cell.lblWeight.text = weightStr
//
//
//                    }
//
//                }
            
            if let imgUrl = info.productImage{
                
               
                
                let url = URL(string: imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? null)
                 let phImage = UIImage(named: ph)
                
                 cell.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                
                
            }
            
            
        }
        
        return cell
       
  
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
               
              
               
               let width = (collectionView.frame.size.width - 5 * 2) / 2
        let height = width * 1.35 //ratio
               return CGSize(width: width, height: height)
               
           
    }
    
    
}

extension BrandLisitingViewController: UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sortArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let cell = self.sortTableView.dequeueReusableCell(withIdentifier: sortCell, for: indexPath) as! SortTableViewCell

            let sort = sortArray[indexPath.row]
            
            let currentIndex = indexPath.row
            
            let selected = currentIndex == selectedSort

            cell.lblTitle.text = (sort.title)
            
            cell.isSelected(selected)
        
            
                
                return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
                updateSelectedIndex(indexPath.row)
                onClickTransparentView()
                
               self.page = 1
               self.sortType = sortKey[indexPath.row]
               self.ListProducts?.removeAll()
            
               getProducts()
               
        
    }
    
    
    
    
}
