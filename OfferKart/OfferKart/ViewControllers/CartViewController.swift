//
//  CartViewController.swift
//  OfferKart
//
//  Created by Sajin M on 01/12/2020.
//


import UIKit
import BadgeSwift

class CartViewController: BaseViewController {
    
    @IBOutlet weak var cartListTableView: UITableView!
    @IBOutlet weak var bottomView: CurvedView!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var deliveryFee: UILabel!
    @IBOutlet weak var totalAmount: UILabel!
    @IBOutlet weak var specialInstructionView: UIView!
    @IBOutlet weak var txtInstruction: UITextField!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var lblSubtotalTxt: UILabel!
    
    
  
    var minPruchaseAmount:Double = 0.0
    var totalValue:Double = 0.0
    var delFee:Double = 0.00
    var customerId:String?
    var uuid:String = null
    var isVary:Bool = false
    var isInstantDeliveryAvailable = true
    var isPickUpAvailable = true
    var pickUpAddress = null
    
   // var specialInstruction:String = ""
    
    var cartData:[Product]?
    var deliveryMethod:[Methods]?
//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
        
        // Do any additional setup after loading the view.
    }
    
    
    func initialSetup(){
        
        self.bottomView.isHidden = true
        self.emptyView.isHidden = true
        self.specialInstructionView.isHidden = true
        self.cartListTableView.isHidden = true
        self.bottomView.isHidden = true
        
        self.cartListTableView.tableFooterView = UIView(frame:.zero)
     
       

    }
    
    override func viewDidAppear(_ animated: Bool) {
        getUserCart()
    }
    
    
    func getUserCart(){
        
        if let id = Defaults.string(forKey: "customerId") {
            
            if let uuid = Defaults.string(forKey: UID){
                
                self.uuid = uuid
                
                if isConnected(){
                    
                    
                                let idValue = id
                                self.customerId = id
                                   
                          getDeliveryMethod(customerId: idValue)
                          getCartData(customerId: idValue, uuid:uuid)
                         
                                   
                               }
                
    
        }
            
        }else{
            
            if let uuid = Defaults.string(forKey: UID){
                
                self.uuid = uuid
                
                if isConnected(){
                    
                
                          getCartData(customerId: null, uuid:uuid)
                         
                                   
                               }

                 }
            
            
        }
    }
    
    
    
    @IBAction func instructionOkPressed(_ sender: Any) {
        self.specialInstructionView.isHidden = true
    }
    
    
    
    
    
    @IBAction func addSpecialInstruction(_ sender: Any) {
        
        self.specialInstructionView.isHidden = false
        
    }
    
    
    
    @IBAction func continuePressed(_ sender: Any) {
        
        
        if Defaults.value(forKey: "customerId") != nil{
            
            if let id = Defaults.string(forKey: "customerId"){
                
                if id == ""{
                    

                        self.STAlert.alert(title:AppName , message: pleaseLogin)
                            .action(.destructive("Login")){
                                
                                let loginScene = LoginViewController.instantiate(fromAppStoryboard: .Main)
                                
                                if let navigator = self.navigationController {
                                    
                                    navigator.pushViewController(loginScene, animated: true)
                                    
                                    
                                    
                                }
                                
                                
                            }.action(.cancel("Cancel"))
                            .show(on: self)
                        
                        
                 
                    
                }else{
                    
                    self.customerId = id
                    continueToDetails()
                    
                }
                
         
            }
            
        }else{
            
            
            self.STAlert.alert(title:AppName , message: pleaseLogin)
                .action(.destructive("Login")){
                    
                    let loginScene = LoginViewController.instantiate(fromAppStoryboard: .Main)
                    
                    if let navigator = self.navigationController {
                        
                        navigator.pushViewController(loginScene, animated: true)
                        
                        
                        
                    }
                    
                    
                }.action(.cancel("Cancel"))
                .show(on: self)
            
            
        }
        

        
    }
    
    
    
    func continueToDetails(){
        
        DispatchQueue.main.async {
            
            if self.isVary{
                
                self.showAlert(message: varyMessage)
            }
            
            
        }
        
        
        
        let DeliveryScene = DeliveryDetailsViewController.instantiate(fromAppStoryboard: .Main)

        if let navigator = self.navigationController {

            DeliveryScene.deliveryMethod = self.deliveryMethod
            DeliveryScene.minPurchaseAmt = self.minPruchaseAmount
            DeliveryScene.subtotalAmt = self.totalValue
            DeliveryScene.itemsCount = self.lblSubtotalTxt.text
            DeliveryScene.instantDeliveryAvailable = self.isInstantDeliveryAvailable
            DeliveryScene.isPickUpDeliveryAvailable = self.isPickUpAvailable
            DeliveryScene.pickUpAddress = self.pickUpAddress
            navigator.pushViewController(DeliveryScene, animated: true)

        }
        
        
        
        
    }
    
    
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
    }
    
    func getCartData(customerId:String,uuid:String){
        
        
        self.STProgress.show()
        
        let cartParam = CartDetailsParams(customerId:customerId,uuId:uuid).Values
        
        self.ApiRequest.getCart(withParameter:cartParam) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                if let info = self.ApiRequest.CartListResponse?.products{
                    
                    
                    
                    
                    if self.cartData?.count ?? 0 > 0{
                        
                        self.cartData?.removeAll()
                    }
                    
                    self.cartData = info
                
                    if self.cartData?.count ?? 0 > 0 {
                        
                        if self.cartData?.count == 1{
                            
                            self.lblSubtotalTxt.text = sub + "(\(self.cartData?.count ?? 0) item)"
                            
                        }else{
                            
                            self.lblSubtotalTxt.text = sub + "(\(self.cartData?.count ?? 0) items)"
                            
                        }
                        
                    
                        if let isFresh = self.ApiRequest.CartListResponse?.isFreshItem{
                            
                            if isFresh == 0{
                                self.isVary = false
                            }else{
                                self.isVary = true
                            }
                            
                        }
                                
                        
                        if let delFee = self.ApiRequest.CartListResponse?.deliveryCharge{
                            
                            self.delFee = Double(delFee) ?? 0.00
                        }
                       
                        
                        if let minPurchase = self.ApiRequest.CartListResponse?.minPurchaseAmount{
                            
                            self.minPruchaseAmount = Double(minPurchase)!
                            
                        }
                        
                        if let count = self.cartData?.count{
                            
                            let count = "\(count)"
                           
                            Defaults.set(count, forKey: "badge")
                            
                            self.totalValue = 0.0
                        }
                        
                        
                        for i in self.cartData!{
                            
                            if let price = i.price {
                                
                                if let qty = i.qty{
                                    
                                    let priceValue = Double(price)! * Double(qty)!
                                    self.totalValue = self.totalValue + priceValue
                                  
                                    
                                }
                                
                                
                            }
                    
                            
                        }
                        
                      
                        
                        if self.totalValue < self.minPruchaseAmount{
                            
                          
                            DispatchQueue.main.async {
                            
                                
                                self.lblSubTotal.text = currency + "\(self.totalValue.roundToDecimal(3))"
                                self.deliveryFee.text = currency + " \(self.delFee)"
                                self.deliveryFee.textColor = UIColor.black
                                self.totalAmount.text = currency + "\((self.delFee + self.totalValue).roundToDecimal(3))"
                               

                            }
                            
                            
                        }else{
                            
                               DispatchQueue.main.async {
                            
                            self.deliveryFee.text = "FREE"
                            self.deliveryFee.font = self.deliveryFee.font.bold()
                            self.deliveryFee.textColor = Colors.OKGreen
                            self.lblSubTotal.text = currency + "\(self.totalValue.roundToDecimal(3))"
                            self.totalAmount.text = currency + "\((self.totalValue).roundToDecimal(3))"
                            }
                            
                        }
                
                        
                        self.cartListTableView.delegate = self
                        self.cartListTableView.dataSource = self
                        self.cartListTableView.reloadData()
                        self.cartListTableView.isHidden = false
                        self.bottomView.isHidden = false
                        
                    }else{
                        
                        Defaults.set("0", forKey: "badge")
                        self.cartListTableView.isHidden = true
                        self.emptyView.isHidden = false
                        self.bottomView.isHidden = true
                        
                    }
                    
                    
                }
                
                
            }else{
                
            }
        }
        
    }
    
    
    func getDeliveryMethod(customerId:String){
        
        
        self.STProgress.show()

        let Params = MethodParameters(customerId:customerId).Values

              self.ApiRequest.getDeliveryMethods(withParameter:Params) { (isSuccess,message) in

                  self.STProgress.dismiss()

                if isSuccess {
                
                    if let address = self.ApiRequest.DeliveryResponse{
                        
                        if let storeAddress = address.storeAddress{
                            
                            if let addOne = address.addressLine_1{
                                
                                self.pickUpAddress = storeAddress + "," + addOne
                            }
                            
                            
                            if let addTwo = address.addressLine_2{
                                
                                self.pickUpAddress = self.pickUpAddress + "," + addTwo
                            }
                            
                            if let addThree = address.addressLine_3{
                                
                                self.pickUpAddress = self.pickUpAddress + "," + addThree
                            }
                            
                          
                            
                            
                            
                        }
                        
                        
                    }
                    

                    if let info = self.ApiRequest.DeliveryResponse?.result{

                       
                        if info.count > 0 {

                             self.deliveryMethod = info

                            if info[0].isMethodActive{

                                    self.isInstantDeliveryAvailable = true

                            }else{
                                
                                self.isInstantDeliveryAvailable = false
                            }
                            
                            if info[2].isMethodActive{

                                self.isPickUpAvailable = true

                            }else{
                                
                                self.isPickUpAvailable = false
                            }


                            if let freeCharge =  info[1].additionalCharge{

                                self.delFee = Double(freeCharge)!

                            }
                            
                        
                            
                           

                        }


                      }


                  }
              }


    }
    
    
    func updateCartItem(productId:String,quantity:String,price:String){
        
        var custId = ""

        if let customerId = Defaults.string(forKey: "customerId") {
            custId = customerId
        }


        let cartParam = updateParam(id: productId, quantity: quantity, price: price, customerId:custId).Values

        self.ApiRequest.cartUpdate(withParameter:cartParam) { (isSuccess,message) in

           // self.STProgress.dismiss()

            if isSuccess {

                if message.contains("mum") {
                     self.showAlert(message: message)
                }


                if let id = self.customerId{

                    self.getCartData(customerId: id, uuid: self.uuid)

                }else{
                    
                    self.getCartData(customerId: custId, uuid: self.uuid)
                    
                }


            }else{

                self.showAlert(message: message)
            }
        }

    }
    
    
    func removeCartItem(productId:String){
        
        
        var custId = ""

        if let customerId = Defaults.string(forKey: "customerId") {
            custId = customerId
        }

        
        self.STProgress.show()
        
        let cartParam = ["cartId":productId,"customerId":custId]
        
        self.ApiRequest.removeCartItem(withParameter:cartParam) { (isSuccess,message) in

            self.STProgress.dismiss()

            if isSuccess {

           

                    self.getCartData(customerId: custId, uuid: self.uuid)

            

            }else{

                self.showAlert(message: message)
            }
        }
        
    }
    
    
    
    
}
extension CartViewController:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return self.cartData?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.cartListTableView.dequeueReusableCell(withIdentifier: cartCell) as! CartTableViewCell
        
        
        if let info = self.cartData?[indexPath.row] {
            
            if let title = info.productName{
                
                cell.lblTitle.text = title
                
            }
            
            
            if let quantity = info.qtyWeight{
                
                
                if let price = info.price{
                    
                    let priceValue = Double(price)!.roundToDecimal(3)
                    
                    cell.cellPrice = priceValue
                    
                    cell.lblPrice.text = currency + "\(priceValue)"
                    
                    if let qty = info.qty{
                        
                        cell.lblCount.text = "x " + qty
                        
                        cell.txtCount.text = qty
                        let totalPrice = (Double(price)!.roundToDecimal(3) * Double(qty)!).roundToDecimal(3)
                        
                        cell.lblTotalAmount.text = currency + "\(totalPrice)"
                    }
                    
              
                  
                
                }
                
            }
            
            
            
            if let weight = info.qtyWeight{

                if let weightType = info.weight{

                    if weightType != "Pc"{

                        self.isVary = true
                    }

                    cell.lblWeight.text = weight + null + weightType


                }


            }
            
            if indexPath.row == 0{
                
                cell.lblMessage.isHidden = false
            }else{
                cell.lblMessage.isHidden = true
                
            }
            
            
            if let imgUrl = info.productImage{
                
                let url = URL(string: imgUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? null)
                let phImage = UIImage(named: ph)
                
                cell.imgProduct?.kf.setImage(with: url, placeholder: phImage)
                
                
            }
            
            
            
        }
        
        cell.cellIndex = indexPath.row
        cell.delegate = self
        
        return cell
    }
    
    
    
}

extension CartViewController:CartListDelegate{
    
    
    func updateCart(at index: Int, count: String) {
        
        
        if let info = cartData?[index]{
            
            if let price = info.price{
                
                if let id = info.id{
                    
                    self.updateCartItem(productId:"\(id)",quantity:count,price:price)
                    
                    
                }
                
               
            }
            
           
            
        }
        
        
    }
    
    func deleteItem(at index: Int) {
        
        if let info = cartData?[index]{
            
            if let id = info.id{
                
                self.removeCartItem(productId: "\(id)")
            }
            
            
        }
        
    }
    
    
    
    
}

