//
//  DeliveryDetailsViewController.swift
//  Offerkart
//
//  Created by Sajin M on 30/06/2020.
//  Copyright © 2020 Daradorf. All rights reserved.
//

import UIKit

class DeliveryDetailsViewController: BaseViewController {
  

    
       
    @IBOutlet weak var nonDeliverBarHeightConstriant: NSLayoutConstraint!
    
    @IBOutlet weak var addressView: CurvedView!
    @IBOutlet weak var paymentView: CurvedView!
    @IBOutlet weak var nonDeliverMessageBar: UIView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    
    @IBOutlet weak var addOne: UILabel!
    @IBOutlet weak var addTwo: UILabel!
    
    @IBOutlet weak var addThree: UILabel!
    @IBOutlet weak var addFour: UILabel!
    @IBOutlet weak var imgCOD: UIImageView!
    @IBOutlet weak var imgCAD: UIImageView!
    @IBOutlet weak var addressScrollView: UIScrollView!
    @IBOutlet weak var specialInstructionView: UIView!
    @IBOutlet weak var btnAddress: CMButton!
    @IBOutlet weak var txtInstruction: UITextField!
    @IBOutlet weak var lblAddInstruction: UILabel!
    @IBOutlet weak var btnRemove: CMButton!
    
    @IBOutlet weak var timeSlotTableview: UITableView!
    @IBOutlet weak var slotView: UIView!
    
    @IBOutlet weak var lblInstantFee: UILabel!
    @IBOutlet weak var imgRadioFreeDelivery: UIImageView!
    @IBOutlet weak var imgRadioInstantDelivery: UIImageView!
    @IBOutlet weak var lblSubtotal: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblInstantDelivery: UILabel!
    @IBOutlet weak var btnInstantDelivery: UIButton!
    @IBOutlet weak var imgRadioPickUp: UIImageView!
    
    @IBOutlet weak var btnConfirmOrder: CMButton!
    
    @IBOutlet weak var addressViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var addressShowView: CurvedView!
    @IBOutlet weak var pickUpAddressView: CurvedView!
    @IBOutlet weak var lblDelivery: UILabel!
    @IBOutlet weak var txtPromo: UITextField!
    
    @IBOutlet weak var lblPickupAddress: UILabel!
    
    var isCOD:Bool = true
    var paymentMethod:String = "cash_od"
    var pickUpAddress:String?
    
    
    
    
    var shipRoomNo:String =  ""
    var shipBuildingName:String = ""
    var shipLatitude:String = ""
    var shipLongitude:String = ""
    var shipCityId:String = ""
    var specialInstruction:String =  ""
    var shipContact:String = ""
    var deliveryOption = "1"
    var subtotalAmt:Double?
    var minPurchaseAmt:Double?
    var addressData:[Addresses]?
    var selectedAddress:Addresses?
    var timeslots = [Slots]()
    var slotDay = [String]()
    var deliveryMethod:[Methods]?
    var itemsCount:String?
    var instantFee:Double = 50.00
    var timeSlot:String = ""
    var addressId:String?
    var instantDeliveryAvailable = true
    var isPickUpDeliveryAvailable = true
    var custId:String?
    var discountAmount:String?
    var promoId:String?
    
    @IBOutlet weak var lblisFree: UILabel!
    @IBOutlet weak var lblDeliveryTime: UILabel!
    
    @IBOutlet weak var instantDelView: UIStackView!
    @IBOutlet weak var pickUpView: UIStackView!
    @IBOutlet weak var pickUpSpotView: UIView!
    
    @IBOutlet weak var lblMethodOne: UILabel!
    @IBOutlet weak var lblMethodTwo: UILabel!
    @IBOutlet weak var lblMethodThree: UILabel!
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()

        // Do any additional setup after loading the view.
    }
    
    
    func initialSetup(){
 
        self.nonDeliverMessageBar.isHidden = true
        self.nonDeliverBarHeightConstriant.constant = 0
        self.pickUpAddressView.isHidden = true
        
        guard let id = Defaults.string(forKey: "customerId") else{
            
            return
        }
        
       
       
        
        if let methodOne = deliveryMethod?[0].method{
        
            self.lblMethodOne.text = methodOne
        
        }
        
        if let methodTwo = deliveryMethod?[1].method{
        
            self.lblMethodTwo.text = methodTwo
        
        }
        
        if let methodThree = deliveryMethod?[2].method{
        
            self.lblMethodThree.text = methodThree
        
        }
        
        if let pickUpAdd = self.pickUpAddress{
            
            self.lblPickupAddress.text = pickUpAdd
        }
        
        self.custId = id
        self.getTimeSlots(customerId: id, methodId: "1")
        

        
        self.lblisFree.isHidden = true

        if let items = self.itemsCount{
            self.lblSubtotal.text = items + " : "
        }
        
       
        
        if let total = self.subtotalAmt {
            self.lblTotalAmount.text = currency + "\(total.roundToDecimal(3))"
            if let minValue = self.minPurchaseAmt{

                if total >= minValue{

                    let valueStr = "FREE Delivery"

                    let message = "* Your are eligible for FREE Delivery"

                     self.lblisFree.attributedText = message.attributedString(subStr: valueStr, color:Colors.OKGreen)
                    self.lblisFree.isHidden = false




                }else{

                     let value = minValue - total
                     let valueStr = currency + "\(value.roundToDecimal(3))"
                     let message = "* To qualify for FREE Delivery, add " +  currency + "\(value.roundToDecimal(3))" + " of eligible items"


                    self.lblisFree.attributedText = message.attributedString(subStr: valueStr, color:Colors.OKGreen)

                     self.lblisFree.isHidden = false

                        }


            }


        }
        
        
        self.specialInstructionView.isHidden = true
        self.nonDeliverBarHeightConstriant.constant = 0
        self.addressScrollView.isHidden = true
        self.btnAddress.isHidden = true
        self.btnRemove.isHidden = true
//
//        guard let cityId = defualts.string(forKey: "cityId") else{
//
//               return
//           }
//        self.shipCityId = cityId

        
//        if defualts.value(forKey: "customerId") != nil{
//
//            if let id = defualts.string(forKey: "customerId"){
//                if let groceryId = defualts.string(forKey: "groceryId"){
//                let idValue = id
//                    if isConnected(){
//                        getTimeSlots(customerId:idValue,groceryId:groceryId,methodId:"2")
//
//                    }
//                }
//
//                   }
//
//               }
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.AddressSelectedNotification(notification:)), name: Notification.Name(addressSelectNotification), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(self.AddressEmptyNotification(notification:)), name: Notification.Name(addressEmptyNotification), object: nil)

        
        
        
  
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
   
     

        if let Address = self.selectedAddress{


            if let addressId = Address.addressId{
 
                self.addressId = addressId
            }

           // self.addressScrollView.isHidden = false

                let address = Address

            if let name = address.customerName{

                self.lblName.text = name

                                }


            if let addressTitle = address.addressTitle{

                    self.lblType.text = addressTitle

                                }

            if let building = address.houseNoVillaNo{

                self.shipBuildingName = building

                                }


            if let houseNo = address.houseNoVillaNo{

                        self.addOne.text = houseNo


                                }

            
         

            if let houseName = address.houseName {

        
                    self.addTwo.text = houseName
               
            }

            if let area = address.customerArea{
                
               if let emirate = address.customerEmirate{
                    
                    self.addThree.text = area + "," + emirate
                }
                
    
            }

            if let longitude = address.longitude{

                                    self.shipLongitude = longitude
                                }

            if let latitude = address.latitude{

                                    self.shipLatitude = latitude
                                }



            if let phone = address.customerMobile{

                                    self.addFour.text = mob + phone
                                    self.shipContact = phone
                                    }







        }
        
        else{


            if Defaults.value(forKey: "customerId") != nil{

                if let id = Defaults.string(forKey: "customerId"){


                    let idValue = id

                        if isConnected(){

                            getAddressList(customerId:idValue)



                    }

                       }

                   }


        }

    }


     @objc func AddressEmptyNotification(notification: Notification) {

         self.selectedAddress = nil

    }


    @objc func AddressSelectedNotification(notification: Notification) {



        if let addressData = notification.userInfo?["address"] as? Addresses{

            self.selectedAddress = addressData



        if let Address = self.selectedAddress{


            self.addressScrollView.isHidden = false

                let address = Address

            if let addressId = address.addressId{

                self.addressId = addressId
            }

            if let name = address.customerName{

                self.lblName.text = name

                                }


            if let addressTitle = address.addressTitle{

                    self.lblType.text = addressTitle

                                }

            if let building = address.houseNoVillaNo{

                self.addOne.text = building
                
                                }


            if let houseName = address.houseName{

                        self.shipBuildingName = houseName


                                }


//            if let landmark = address.landMark {
//
//                if landmark == null{
//
//                    self.addThree.text = address.customerEmirate
//                }else{
//
//                    self.addThree.text = landmark + landMark
//                }
//
//
//
//            }
            
            if let area = address.customerArea{
                
               if let emirate = address.customerEmirate{
                    
                    self.addThree.text = area + "," + emirate
                }
                
    
            }

            if let houseName = address.houseName{

                    self.addTwo.text = houseName

                                }

            if let longitude = address.longitude{

                self.shipLongitude = longitude
                                }

            if let latitude = address.latitude{

                self.shipLatitude = latitude
                                }



            if let phone = address.customerMobile{

                                    self.addFour.text = mob + phone
                                    self.shipContact = phone
                                    }



        }

        }else{

        

                if let id = Defaults.string(forKey: "customerId"){


                    let idValue = id

                        if isConnected(){

                            getAddressList(customerId:idValue)


                        }
                    }

                   }


         }
    
    
    
    
    func getTimeSlots(customerId:String,methodId:String){
        
        
        let timeParam = TimeSlotParameters(methodId:methodId,customerId:customerId).Values

        self.ApiRequest.getTimeSlots(withParameter:timeParam) { (isSuccess,message) in

                           self.STProgress.dismiss()

                                         if isSuccess {
                                            
                                            self.slotDay.removeAll()
                                            self.timeslots.removeAll()

                                            if let info = self.ApiRequest.DeliveryResponse?.arrAvailableTimeSlotes{

                                                self.timeslots = info


                                                let slots = Dictionary(grouping: self.timeslots, by: { $0.slotDate })


                                                let componentArray = Array(slots.keys)

                                                for key in componentArray{

                                                    self.slotDay.append(key!)
                                                }


                                                if self.timeslots.count > 0 {

                                                    self.timeSlotTableview.delegate = self
                                                    self.timeSlotTableview.dataSource = self
                                                    self.timeSlotTableview.reloadData()
                                                    self.timeSlotTableview.isHidden = false

                                                }else{

                                                    self.timeSlotTableview.isHidden = true

                                                }


                                            }


                                         }else{

                                             self.timeSlotTableview.isHidden = true


            }


            }

        
        
    }
    
    
    @IBAction func btnRemovePressed(_ sender: Any) {

        self.lblAddInstruction.text = "Add Special Instructions"
        self.specialInstruction = ""


    }
    
    
    @IBAction func orderConfirmPressed(_ sender: Any) {
        

        guard let custId = Defaults.string(forKey: "customerId") else {
            return
        }
       
        if self.shipLatitude != "" && self.shipLongitude != ""{

             self.confirmOrder(customerId: custId)

        }else{

            showAlert(message: "Please select delivery address")
        }

 
        
    }
    
    
    @IBAction func instructionOkPressed(_ sender: Any) {
          self.specialInstructionView.isHidden = true

        if self.specialInstruction != "" {

            self.lblAddInstruction.text = self.specialInstruction
            self.btnRemove.isHidden = false

        }
      }





      @IBAction func addSpecialInstruction(_ sender: Any) {

          self.specialInstructionView.isHidden = false

      }



    @IBAction func addInstructions(_ sender: UIButton) {

        switch sender.tag {
        case 1:

                self.txtInstruction.text = "Do not ring bell"
                self.specialInstruction = "Do not ring bell"

            break

        case 2:


                self.txtInstruction.text = "Call for directions"
                self.specialInstruction = "Call for directions"

            break
        case 3:


                self.txtInstruction.text = "Just Knock"
                 self.specialInstruction = "Just Knock"


            break
        case 4:


                self.txtInstruction.text = "Please Come fast"
                 self.specialInstruction = "Please Come fast"

            break
        case 5:


                self.txtInstruction.text = "Bring swipe machine"
                 self.specialInstruction = "Bring swipe machine"


            break
        case 6:


                 self.txtInstruction.text = "Bring change"
                 self.specialInstruction = "Bring change"


            break

        default:
            break
        }




    }



    @IBAction func backPressed(_ sender: Any) {

        navigationController?.popToViewController(ofClass: CartViewController.self)

    }
    
    
    @IBAction func pickUpPressed(_ sender: Any) {
        
        self.imgRadioPickUp.image = UIImage(named:"ic_radio_sel")
        self.imgRadioInstantDelivery.image = UIImage(named:"ic_radio")
        self.imgRadioFreeDelivery.image = UIImage(named:"ic_radio")
        self.lblDeliveryTime.text = self.lblMethodOne.text
        
       
       
//        self.slotDay.removeAll()
//        self.timeslots.removeAll()
        
        self.deliveryOption = "3"
        
        self.lblDelivery.text = "Pick up by"
        
        guard let id = self.custId else {
            return
        }
        self.getTimeSlots(customerId:id, methodId: "3")
        
        self.addressViewHeightConstant.constant = 85
        self.addressShowView.isHidden = true
        self.pickUpAddressView.isHidden = false
        
        self.nonDeliverMessageBar.isHidden = true
        self.nonDeliverBarHeightConstriant.constant = 0
        self.btnConfirmOrder.isUserInteractionEnabled = true

        
        
    }
    

    @IBAction func instantDeliveryPressed(_ sender: Any) {

        self.imgRadioPickUp.image = UIImage(named:"ic_radio")
        self.imgRadioFreeDelivery.image = UIImage(named:"ic_radio")
        self.imgRadioInstantDelivery.image = UIImage(named:"ic_radio_sel")
        self.lblDeliveryTime.text = self.lblMethodOne.text
        
//        self.slotDay.removeAll()
//        self.timeslots.removeAll()
        
        self.addressViewHeightConstant.constant = 184
        self.addressShowView.isHidden = false
        self.pickUpAddressView.isHidden = true
        
        self.lblDelivery.text = "Delivery by"

        self.deliveryOption = "2"
        guard let id = self.custId else {
            return
        }
        self.getTimeSlots(customerId:id, methodId: "2")
       // self.slotView.isHidden = true

    }
    @IBAction func freeDeliveryPressed(_ sender: Any) {

        
        self.imgRadioFreeDelivery.image = UIImage(named:"ic_radio_sel")
        self.imgRadioInstantDelivery.image = UIImage(named:"ic_radio")
        self.imgRadioPickUp.image = UIImage(named:"ic_radio")
        
//        self.slotDay.removeAll()
//        self.timeslots.removeAll()
        
        self.lblDelivery.text = "Delivery by"
        
        self.addressViewHeightConstant.constant = 184
        self.addressShowView.isHidden = false
        self.pickUpAddressView.isHidden = true
        self.deliveryOption = "1"
        
        guard let id = self.custId else {
            return
        }
        self.getTimeSlots(customerId:id, methodId: "1")
    }

    @IBAction func changeSlotPressed(_ sender: Any) {

        self.slotView.isHidden = false
    }



    @IBAction func codPressed(_ sender: Any) {

        self.imgCAD.image = UIImage(named: "ic_radio")
        self.imgCOD.image = UIImage(named: "ic_radio_sel")
        self.isCOD = true
        self.paymentMethod = "cash_od"
    }


    @IBAction func cadPressed(_ sender: Any) {

        self.imgCAD.image = UIImage(named: "ic_radio_sel")
        self.imgCOD.image = UIImage(named: "ic_radio")
        self.isCOD = false
        self.paymentMethod = "card_od"
    }
    
    
    @IBAction func applyPromotionPressed(_ sender: Any) {
        
        guard let code = self.txtPromo.text, !code.isEmpty else {
            
           showAlert(message: "Promocode not valid")
            
            return
            
        }
        
        guard let id = Defaults.string(forKey: "customerId") else{
            
            return
        }
        
        guard let amount = self.subtotalAmt else{
            return
        }

        let param = PromoParams(promoCode:code , customerId: id, orderTotal:"\(amount)").Values



             self.STProgress.show()


            self.ApiRequest.getPromotion(withParameter:param) { (isSuccess,message) in

                               self.STProgress.dismiss()

                                             if isSuccess {

                                                if let info = self.ApiRequest.ForgotPasswordResponse{
                                                    
                                                    if let discount = info.discount{
                                                        
                                                        self.discountAmount = discount
                                                        self.promoId = info.promoCodeId
                                                        
                                                        let totalAmount = Double(self.subtotalAmt!)
                                                       
                                                        if Double(discount) ?? 0 > 0{
                                                            
                                                            if totalAmount > Double(discount)!{
                                                                
                                                                let total = totalAmount - Double(discount)!
                                                                
                                                                self.lblTotalAmount.text = currency + "\(total.roundToDecimal(3))"
                                                                
                                                                self.showAlertCustomTitle(title:"Ohh..Yay" , message: message)
                                                                
                                                            }
                                                            
                                                            
                                                            
                                                        }
                                                        
                                                        
                                                    }else{
                                                        
                                                        self.showAlertCustomTitle(title:"Sorry" , message: message)
                                                        
                                                    }
                                                    
                                                }
                                             }else{


                                                self.showAlert(message: message)

                                                        }


                                                    }
        
        
    }
    
    


    @IBAction func changeAddressPressed(_ sender: Any) {

        let AddressScene = AddressListingViewController.instantiate(fromAppStoryboard: .Main)

            if let navigator = self.navigationController {

                AddressScene.addressData = self.addressData

            navigator.pushViewController(AddressScene, animated: true)



        }


    }


  
    @IBAction func addAddressPressed(_ sender: Any) {
        

        let AddressScene = UpdateAddressViewController.instantiate(fromAppStoryboard: .Main)

                        if let navigator = self.navigationController {

                            AddressScene.isEdit = false

                        navigator.pushViewController(AddressScene, animated: true)

        }
        
    }
    
    
    
    func confirmOrder(customerId:String){
    
     

        guard let addressId = self.addressId else {return}

        let param = CartCheckOut(paymentMethod: self.paymentMethod, specialInstructions: self.txtInstruction.text ?? null , addressId:addressId, customerId: customerId, timeSlotId:self.timeSlot, deliveryTypeId:self.deliveryOption, promoCodeId: self.promoId ?? null,promoDiscount: self.discountAmount ?? null).Values



             self.STProgress.show()


            self.ApiRequest.checkOutOrder(withParameter:param) { (isSuccess,message) in

                               self.STProgress.dismiss()

                                             if isSuccess {


                                                DispatchQueue.main.async {


                                                    let HomeScene = HomeViewController.instantiate(fromAppStoryboard: .Main)

                                                             if let navigator = self.navigationController {


                                                             navigator.pushViewController(HomeScene, animated: true)


                                                         }


                                                    Defaults.set("0",forKey: "badge")
                                                    self.showAlert(message: "Order Placed Successfully")

                                                }


                                             }else{


                                                self.showAlert(message: wentWrong)




                                                        }


                                                    }


                                                    
                                                }
                                            
        
   
    func getAddressList(customerId:String){


             self.STProgress.show()


     

            let params = MethodParameters(customerId:customerId).Values

            self.ApiRequest.getDeliveryAddress(withParameter:params) { (isSuccess,message) in

                               self.STProgress.dismiss()

                                             if isSuccess {

                                                if let info = self.ApiRequest.DeliveryAdressResponse?.arrAdress{

                                                    self.addressData = info

//                                                    if let area = self.ApiRequest.DeliveryAddressResponse?.deliverableKm{
//
//                                                        defualts.setValue(area,forKey:"deliverableArea")
//
//
//                                                    }

                                                    if self.addressData?.count ?? 0 > 0 {

                                                       self.addressScrollView.isHidden = false


                                                        let address = self.addressData?[0]

                                                        if let name = address?.customerName{


                                                            self.lblName.text = name

                                                        }


                                                        if let addressId = address?.addressId{

                                                            self.addressId = addressId
                                                        }



                                                        if let addressTitle = address?.addressTitle{


                                                            self.lblType.text = addressTitle

                                                        }

                                                        if let houseNo = address?.houseNoVillaNo{

                                                                    self.addOne.text = houseNo

                                                                   // self.shipRoomNo = roomNo

                                                                            }


                                                        if let houseName = address?.houseName {

                                                            self.addTwo.text = houseName


                                                        }

                                                        if let customerArea = address?.customerArea{

                                                                self.addThree.text = customerArea
                                                            
                                                            if let customerCity = address?.customerEmirate{
                                                                
                                                                self.addThree.text = customerArea + "," + customerCity
                                                                
                                                            }
                                                            

                                                                            }





                                                        if let longitude = address?.longitude{

                                                            self.shipLongitude = longitude
                                                        }

                                                        if let latitude = address?.latitude{

                                                            self.shipLatitude = latitude
                                                        }



                                                        if let phone = address?.customerMobile{

                                                            self.addFour.text = mob + phone
                                                            self.shipContact = phone
                                                            }


                                                    }else{

                                                         self.btnAddress.isHidden = false
                                                         self.addressView.isHidden = true


                                                    }



                                                }


                                             }else{


                                                 self.nonDeliverMessageBar.isHidden = true
                                                 self.nonDeliverBarHeightConstriant.constant = 0
                                                 self.addressScrollView.isHidden = true
                                                 self.btnAddress.isHidden = false

                                                //self.showAlert(message: wentWrong)



                           }
                }

              }


}
extension DeliveryDetailsViewController: UITableViewDelegate,UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {


        let slots = Dictionary(grouping: self.timeslots, by: { $0.slotDate })

        let timeSlots = slots.filter { $0.key == self.slotDay[section]}
        return timeSlots[self.slotDay[section]]?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = self.timeSlotTableview.dequeueReusableCell(withIdentifier: slotCell, for: indexPath) as! CommonTableCell


        let slots = Dictionary(grouping: self.timeslots, by: { $0.slotDate })
        let timeslots = slots.filter { $0.key == self.slotDay[indexPath.section]}


       // print(timeslots[self.slotDay[indexPath.section]]?[indexPath.row].timeslotes!)
        
        cell.lblSlot.text  = timeslots[self.slotDay[indexPath.section]]?[indexPath.row].timeslotes
        return cell
    }



    func numberOfSections(in tableView: UITableView) -> Int {

        return self.slotDay.count
      }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

         let slots = Dictionary(grouping: self.timeslots, by: { $0.slotDate })
         let timeslots = slots.filter { $0.key == self.slotDay[indexPath.section]}


        if let time = timeslots[self.slotDay[indexPath.section]]?[indexPath.row].timeslotes{
        let date = self.slotDay[indexPath.section].fullDate()




            self.lblDeliveryTime.text = date + ", " + time

            if let slotId = timeslots[self.slotDay[indexPath.section]]?[indexPath.row].id{

               self.timeSlot = slotId
            }
//            if let message = timeslots[self.slotDay[indexPath.section]]?[indexPath.row].alert_message{
//
//                if message != null && message != "null"{
//
//                    showAlert(message: message)
//                }
//
//           }


        }

             self.slotView.isHidden = true

    }


    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){

        let header = view as! UITableViewHeaderFooterView

          view.tintColor = .white
          header.textLabel?.font =  header.textLabel?.font.withSize(14)
          header.textLabel?.textColor =  #colorLiteral(red: 0, green: 0.5820022225, blue: 0.2493254542, alpha: 1)

      }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if self.deliveryOption == "2"{
            
            return "Tomorrow - " + self.slotDay[section].fullDate()
        }
        
        if self.deliveryOption == "1"{
            
            return "Today - " + self.slotDay[section].fullDate()
        }
        
        
        return  self.slotDay[section].fullDate()
       }

      func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

          return 35.0
      }



}


