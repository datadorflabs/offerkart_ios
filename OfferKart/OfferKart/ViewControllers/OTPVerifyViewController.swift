//
//  OTPVerifyViewController.swift
//  Offerkart
//
//  Created by Sajin M on 02/07/2020.
//  Copyright © 2020 Datadorf. All rights reserved.
//

import UIKit

class OTPVerifyViewController: BaseViewController {
    
    @IBOutlet weak var txtOtp: STextField!
    
    var phoneNumber:String?
    var otpNumber:String?
    var customerId:String?
    
    
    
    
    var isForgotPassword:Bool? = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.txtOtp.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func confirmPressed(_ sender: Any) {
        
        
        if isForgotPassword! {
            
            if let otpNumber = self.otpNumber {
                
                if  let recivedOtp = self.txtOtp.text{
                    
                    if recivedOtp == otpNumber{
                        
                        guard let phone = self.phoneNumber else{
                            return
                        }
                        
                        
                        guard let customerId = self.customerId else{
                            
                            return
                            
                        }
                        
                        let NewPassScene = NewPasswordViewController.instantiate(fromAppStoryboard: .Main)
                        
                        if let navigator = self.navigationController {
                            
                            NewPassScene.customerId = customerId
                            navigator.pushViewController(NewPassScene, animated: true)
                            
                        }
                        
                    }else{
                        
                        self.showAlert(message: "Invalid OTP, Please try again")
                        
                    }
                    
                    
                    
                }
                
                
                
                
            }
            
            
            
        }else{
            
            
            if let token =  UserDefaults.standard.value(forKey: "fcmToken") as? String{
                
                
                
                if let customerId = self.customerId{
                    
                    
                    guard let otpNumber = self.txtOtp.text , !otpNumber.isEmpty else {
                        
                        showAlert(message: "Invalid OTP, Please try again")
                        
                        return
                        
                    }
                    
                    self.STProgress.show()
                    
                    let params = FcmRegisterParams(fcmToken: token, customerId: customerId, otpVerify: otpNumber).Values
                    
                    self.ApiRequest.verifyUser(withParameter:params) { (isSuccess,message) in
                        
                        self.STProgress.dismiss()
                        
                        if isSuccess {
                            
                            
                            
                            if let data = self.ApiRequest.UserDetailsResponse{
                                
                                if let customerId = data.customerId {
                                    
                                    Defaults.set(customerId,forKey: "customerId")
                                    
                                    if let name = data.customerName{
                                        
                                        
                                        Defaults.set(name,forKey:"name")
                                        
                                    }
                                    
                                    if let email = data.customerEmailId{
                                        
                                        Defaults.set(email,forKey:"email")
                                        
                                    }
                                    
                                    if let phone = data.customerPhone{
                                        
                                        Defaults.set(phone,forKey:"phone")
                                        
                                    }
                                    
                                    if let token = data.authorization{
                                        
                                        Defaults.set(token,forKey:"token")
                                        
                                    }
                                    
                                    
                                }
                       
                                
                            }
                            
                            
                            
                            let HomeScene = HomeViewController.instantiate(fromAppStoryboard: .Main)
                            
                            if let navigator = self.navigationController {
                                
                                
                                navigator.pushViewController(HomeScene, animated: true)
                                
                                
                            }
                            
                        }else{
                            
                            
                            
                            self.showAlert(message: message)
                            
                        }
                    }
                    
                    
                    
                }else{
                    
                    self.showAlert(message: wentWrong)
                }
                
            }
            
            
        }
        
        
    }
    
    
    
    
    @IBAction func resendPressed(_ sender: Any) {
        
        
        guard let phone = self.phoneNumber, !phone.isEmpty else{
            
            
            return
        }
        
        
        
        resendOtp(phone: phone)

    }
    
    
    
    
    
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
    }
    
    
    func resendOtp(phone:String){
        
        if isForgotPassword ?? false {
            
            guard let phone = self.phoneNumber else {
                return
            }
            
            
            getOtp(phone: phone)
            
            
        }else{
            
            
            self.STProgress.show()
            
            let params = SendSms(mobile:phone).Values
            
            self.ApiRequest.sendOTP(withParameter:params) { (isSuccess,message,OtpString) in
                
                self.STProgress.dismiss()
                
                if isSuccess {
                    
                    if OtpString != nil{
                        
                        self.otpNumber = OtpString
                    }
                    
                    
                    
                }else{
                    
                    self.showAlert(message: message)
                    
                }
                
            }
        }
    }
    
    
    func getOtp(phone:String){
        
        
        
        let params = SendSms(mobile:phone).Values
        
        self.ApiRequest.forgotPassword(withParameter:params) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                if let info = self.ApiRequest.ForgotPasswordResponse{
                    
                    
                    
                    self.showAlert(message: message)
                    
                    
                    
                    
                }
                
                
                
                
            }else{
                
                
                self.showAlert(message: message)
                
            }
            
        }
    }
    
    
    func random(digits:Int) -> String {
        var result = ""
        repeat {
            // create a string with up to 4 leading zeros with a random number 0...9999
            result = String(format:"%06d", arc4random_uniform(10000) )
            // generate another random number if the set of characters count is less than four
        } while (result.count) < 6
        return result
        
        
        
    }
    
}


extension OTPVerifyViewController:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // get the current text, or use an empty string if that failed
        let currentText = textField.text ?? ""
        
        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        // add their new text to the existing text
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        
        // make sure the result is under 16 characters
        return updatedText.count <= 6
    }
    
    
    
    
}


