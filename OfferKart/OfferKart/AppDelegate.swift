//
//  AppDelegate.swift
//  OfferKart
//
//  Created by Sajin M on 28/10/2020.
//

import UIKit
import GoogleMaps
import IQKeyboardManagerSwift
import Firebase
import Kingfisher

@main
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        if Defaults.string(forKey:UID) == nil{
            
            getDeviceId()
        }
        
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
          UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        } else {
          let settings: UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }

        UIApplication.shared.applicationIconBadgeNumber = 0
        application.registerForRemoteNotifications()

        KingfisherManager.shared.cache.clearCache()
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        GMSServices.provideAPIKey(GoogleKey)
        IQKeyboardManager.shared.enable = true
        
       
        return true
    }

    
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        KingfisherManager.shared.cache.clearCache()
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
           let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
         //  print(deviceTokenString)
       }

    
    func getDeviceId(){
        
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            Defaults.set(uuid,forKey:UID)
        }
        
    }

}

extension AppDelegate: MessagingDelegate {

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        //print("Firebase registration token: \(fcmToken)")

        let dataDict: [String: String] = ["token": fcmToken]

        UserDefaults.standard.set(fcmToken, forKey: "fcmToken")

        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)

        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }

    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
       // print(remoteMessage)
    }

}
