//
//  UIColorExt.swift
//  OfferKart
//
//  Created by Sajin M on 30/11/2020.
//

import Foundation
import UIKit

extension UIColor {
  struct ProductSelection {
    static var isSelected: UIColor  { return Colors.lightBlue }
    static var notSelected: UIColor { return UIColor.white }
  }
}
