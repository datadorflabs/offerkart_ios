//
//  Constants.swift
//  Nesto OMAN
//
//  Created by Sajin M on 18/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation
import UIKit







//Cell Identifiers


let bannerCell:String = "cell"
let homeCell:String = "homeTableCell"
let homeCollectionCell:String = "homeCollectionCell"
let mainCategoryCell:String = "MainCatCell"
let cartCell:String = "cartListCell"
let subCatCell:String = "SubCategoryCell"
let titleCell:String = "titleTextCell"
let subCategorySubCell:String = "subCatSubCell"
let addressCell:String = "AddressCell"
let orderCell:String = "OrderCell"
let searchCell:String = "SearchCell"
let sortCell:String = "SortCell"
let slotCell:String = "SlotCell"
let BrandTablesCell:String = "brandTableCell"
let BrandCell:String = "brandCollectionCell"
let BannerTableCell:String = "bannerTableCell"
let horizontalCollectionCell:String = "horizontalCell"
let specialCell:String = "specialOfferCell"
let horizontalMainCell = "horizontalCatCell"
let horizontalMainTableCell = "horizontalCategoryCell"
let subBannerCell = "bannerSubTableCell"
let genericCollectionCell = "genericCell"
let detailCell = "detailsCell"
let subBottomCategroyCell = "subCatBottomCell"



//App Alert
let AppName = "Offerkart"

let noInternetTitle = "No Internet Connection"
let noInternetMessage = "Please check your internet connection."
let notValidEmail = "Please enter a valid email id"
let notValidCredentials = "Credentials entered not valid"
let cannotConnect = "Can't connect to server. Please try again later."
let fillAllFields = "Please fill all fields"
let passwordNotSame = "Password and Confirm Password should be same"
let paymentFailed = "Payment Failed"
let tryAgain = "Please try again!"
let pleaseLogin = "Please Log In to Continue"
let wentWrong = "Oops.. Something Went wrong"
let selectHotel = "Please select a hotel"
let roomSelection = "You have already made a selection"
let noHotels = "No Results Found"
let reashedLimit = "Reached Maximum Limit"
let selectLocation = "Please Select a Location"
let nameMandatory = "Name and Phone number should not be empty"
let selectGovernerate = "Please select a Governerate,Area and Street"
let selectGovCity = "Please select a Governerate,Area and Street"
let productAdded = "Product added to cart successfully"
let notValidPhone = "Phone number is not valid"
let passwordMessage = "Password should contain at least 5 characters"
let confirm = "Confirm Cancellation"
let confimMessage = "Are you sure you want to cancel this order?"
let varyMessage = "Please remember the delivered quantity may vary from the actual ordered quantity"
let addressUpdated = "Address Updated Successfully"
let selectEmirate = "Please select an emirate"



let cod = "Cash on delivery"
let cad = "Card at delivery"

//type def

let DashBoardReload:String = "HomeReload"
let MyOrderReload = "OrderReaload"
let ph:String = "ph"
let phSmall:String = "phSmall"

let currency:String = "AED "
let countryCode:String = "+971"
let null:String = ""
let landMark:String = " (Landmark)"
let mob:String = "Mobile: "
let sub:String = "Subtotal "
let addressSelectNotification = "nq.selectAddress"
let addressEmptyNotification = "nq.emptyAddress"
let subCodes = ["50","51","52","53","54","55","56","57","58"]

let UID:String = "uuid"


//Google API key

let GoogleKey:String = "AIzaSyDV40h4drksUHL_JiZLyZcJp_k7wSCw1HU"


//User Defaults

let Defaults = UserDefaults.standard

let height: CGFloat = 230


enum Colors {

    static let lightBlue =  UIColor(red: 208/255, green: 235/255, blue: 250/255, alpha: 1.0)
    static let OKBlue =  UIColor(red: 25/255, green: 63/255, blue: 90/255, alpha: 1.0)
    static let OKYellow =  UIColor(red: 248/255, green: 204/255, blue: 70/255, alpha: 1.0)
    static let OKGreen =  UIColor(red: 72/255, green: 158/255, blue: 89/255, alpha: 1.0)
}



//API Urls

let BaseUrl:String = "https://staging.offerkart.com/API/" //"https://offerkart.com/API/"
let tcUrl:String = "https://offerkart.com/terms_and_conditions.html"


enum viewType{
    
   static let banners = "banners"
   static let special = "specialOffers"
   static let mainCat = "mainCategory"
   static let productOne = "productsOne"
   static let productTwo = "productsTwo"
   static let productThree = "productsThree"
   static let productFour = "productsFour"
   static let productFive = "productsFive"
   static let productSix = "productsSix"
   static let bannerOne = "HomescreenBannerOne"
   static let bannerTwo = "HomescreenBannerTwo"
   static let bannerThree = "HomescreenBannerThree"
   static let subCategoryBanner = "subCategoryBanner"
   static let productNine = "productsNine"
   static let productTen = "productsTen"
   static let brands = "brands"
   static let subCategoryBannerBottom = "subCategoryBanner"
   static let subCategroyBottom = "SubcategoryBottom"
   static let campaignOne = "campaignsOne"
   static let campaignTwo = "campaignsTwo"
   static let campaignThree = "campaignsThree"
   static let campaignFour = "campaignsFour"
   static let campaignFive = "campaignsFive"
    
}

enum Api{


static let homeApi = BaseUrl + "appHome"
static let category = BaseUrl + "customer_grocery_main_products_category"
static let offerProduct =  BaseUrl + "offerProducts"
static let campaignProduct = BaseUrl + "campaignProducts"
static let homeSubCategory = BaseUrl + "customer_grocery_category_first_15_products"
static let getState = BaseUrl + "getGroceryStates"
static let getStateCity = BaseUrl + "getStateCity"
static let getStateCityStreet = BaseUrl + "getStateCityStreets"
static let getCustomerGrocery = BaseUrl + "customer_grocery_search"
static let getNearestGrocery = BaseUrl + "customer_nearest_grocery_search"
static let Login = BaseUrl + "customerSignIn"
static let SignUp = BaseUrl + "customerSignUp"
static let addCart = BaseUrl + "addToCart"
static let fcmRegistration = BaseUrl + "fcmRegister"
static let cartdetails = BaseUrl + "viewCart"
static let subMainCategory = BaseUrl + "customer_grocery_category_groups"
static let subCategory = BaseUrl + "customer_grocery_category_groups_subcategory"
static let subCategoryProducts = BaseUrl + "customerGroceryBaseProductsCategory"
static let listProducts = BaseUrl + "subCategoryProducts"
static let getDeliveryAddress = BaseUrl + "list_customer_delivery_addresses"
static let myOrders = BaseUrl + "customerOrderHistory"
static let myWishList = BaseUrl + "customerFavouriteProductsList"
static let cartCheckout = BaseUrl + "cartCheckOut"
static let removeCart = BaseUrl + "removeCartItems"
static let cartUpdate = BaseUrl + "cartUpdate"
static let updateAddress =  BaseUrl + "updateCustomerAddress"                      
static let searchProduct = BaseUrl + "productSearch"
static let logout = BaseUrl + "customerLogout"
static let search = BaseUrl + "productsearchofgrocery"
static let deleteAddress = BaseUrl + "deleteCustomerAddress"
static let fcmRegister = BaseUrl + "fcmRegister"
static let orderDetails = BaseUrl + "customerOrderHistoryDetails"
static let refundPolicy = BaseUrl + "grocery_return_policy"
static let makeFavorite = BaseUrl + "customerFavouriteProducts"
static let makeUnFavorite = BaseUrl + "customerUnfavouriteProducts"
static let forgotPassword = BaseUrl + "forgotPassword"
static let resendOtp = BaseUrl + "resendOtp"
static let fcmSignIn = BaseUrl + "fcmSignIn"
static let updatePassword = BaseUrl + "passwordChange"
static let versionUpdate = BaseUrl + "appUpdate"
static let singleProduct = BaseUrl + "interactive_banner_list_product"
static let brandList = BaseUrl + "brandProducts"
static let byBrands = BaseUrl + "grocery_products_lists_based_on_brands"
static let cancelOrder = BaseUrl + "orderCancel"
static let currentAddress = BaseUrl + "customer_shipping_details"
static let addAddress = BaseUrl + "addCustomerAddress"
static let timeSlots = BaseUrl + "listTimeSlotes"
static let deliveryMethod = BaseUrl + "listDeliveryMethods"
static let productDetails = BaseUrl + "productDetails"
static let changeProducts = BaseUrl + "viewProductDetails"
static let subCategories = BaseUrl + "categoryGroups"
static let getAddressList = BaseUrl + "listCustomerDeliveryAddresses"
static let emirates = BaseUrl + "getEmirates"
static let applyPromo = BaseUrl + "applyPromocode"

    
   
}
