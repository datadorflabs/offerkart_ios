//
//  ParameterModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 18/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation




struct LoginParameters {
    
    
    var username:String = ""
    var password:String = ""
   
 
    var Values: [String: String] {
        return [

            "userName":username,
            "password":password
        ]
    }
  
}


struct groupParameters {

    var mainCatId:String = ""
    
    var Values: [String: String] {
        return [

            "mainCatId":mainCatId,
            "groceryId":"2"
        ]
    }
  
}


struct ChangeProduct{
    
    var articleID:String = ""
    var productColorId:String = ""
    var productSizeId:String = ""
    
    
    var Values: [String: String] {
        return [

            "articleID":articleID,
            "productColorId":productColorId,
            "productSizeId":productSizeId
        ]
    }
    
    
}


struct HomeParams{
    
    var customerId:String = ""
    var uuId:String = ""
    
    
    var Values: [String: String] {
        return [

            "customerId":customerId,
            "uuId":uuId,
            "groceryId":"2"
        ]
    }
 
}


struct CartParams{
    
    var customerId:String = ""
    var uuId:String = ""
    var weight:String = ""
    var quantity:String = ""
    var productId:String = ""
    
    
    var Values: [String: String] {
        return [

            "customerId":customerId,
            "uuId":uuId,
            "groceryId":"2",
            "productId":productId,
            "weight":weight,
            "quantity":quantity
        ]
    }
  

}


struct CartDetailsParams{
    
    var customerId:String = ""
    var uuId:String = ""
   
    
    var Values: [String: String] {
        return [

            "customerId":customerId,
            "uuId":uuId,
            "groceryId":"2",
           
        ]
    }
  

}




struct TimeSlotParameters {
    
    
   
    var methodId:String = ""
    var customerId:String = ""
   
    
    
    var Values: [String: String] {
        return [

            "groceryId":"2",
            "customerId":customerId,
            "methodId":methodId
        ]
    }
  
}


struct MethodParameters {
    

    var customerId:String = ""
   
    
    var Values: [String: String] {
        return [

            "groceryId":"2",
            "customerId":customerId
            
        ]
    }
  
}




struct AddressParams {
    
   
    
    var customerId:String = ""
    var customer_name:String = ""
    var house_name:String = ""
    var land_mark:String = ""
    var customer_area:String = ""
    var latitude:String = ""
    var longitude:String = ""
    var customer_city:String = ""
    var customer_mobile:String = ""
    var addressTitle:String = ""
    var roomNo:String = ""
    
      var Values: [String: String] {
          return [

              "customerId":customerId,
              "customerName":customer_name,
              "houseName":house_name,
              "landMark":land_mark,
              "customerArea":customer_area,
              "latitude":latitude,
              "longitude":longitude,
              "customerEmirateId":customer_city,
              "customerMobile":customer_mobile,
              "addressTitle":addressTitle,
              "houseNoVillaNo":roomNo
          ]
      }
    
}




struct UpdateAddressParams {
    
    var addressId:String = ""
    var customerId:String = ""
    var customer_name:String = ""
    var house_name:String = ""
    var land_mark:String = ""
    var customer_area:String = ""
    var latitude:String = ""
    var longitude:String = ""
    var customer_city:String = ""
    var customer_mobile:String = ""
    var addressTitle:String = ""
    var roomNo:String = ""
    
      var Values: [String: String] {
          return [
            
              "addressId":addressId,
              "customerId":customerId,
              "customerName":customer_name,
              "houseName":house_name,
              "landMark":land_mark,
              "customerArea":customer_area,
              "latitude":latitude,
              "longitude":longitude,
              "customerEmirateId":customer_city,
              "customerMobile":customer_mobile,
              "addressTitle":addressTitle,
              "houseNoVillaNo":roomNo
          ]
      }
    
}






struct FcmRegisterParam {
    
    var fcm_regId:String = ""
     var user_id:String = ""
    

     
     var Values: [String: String] {
           return [

               "deviceTypeId":"2",
               "userTypeId":"1",
               "fcmRegId":fcm_regId,
               "userId":user_id
               
               
           ]
       }
    
 
}

struct SearchParam {
    
   
     var searchKey:String = ""
     var limit:String = ""
     var customerId:String = ""
     
     var Values: [String: String] {
           return [

               "groceryId":"2",
               "searchKey":searchKey,
               "status":"1",
               "limit":limit,
               "customerId":customerId
               
               
           ]
       }
    
 
    
}

struct DeleteParam {
    

       var customerId:String = ""
        var addressId:String = ""
       
        var Values: [String: String] {
              return [

                  "customerId":customerId,
                  "addressId":addressId
                  
                 

              ]
          }
       
    
    
}






struct updateParam {
    
    var id:String = ""
    var quantity:String = ""
    var price:String = ""
    var customerId:String = ""
    
    var Values: [String: String] {
          return [

              "cartId":id,
              "quantity":quantity,
              "price":price,
              "customerId":customerId
              
              
          ]
      }
}


struct CustomerParams {
    
    var customerId:String = ""
    var limit:String = ""
    
    var Values: [String: String] {
          return [

              "customerId":customerId,
              "limit":limit
              
              
          ]
      }
}

struct FcmRegisterParams{
    
    
    var fcmToken:String = ""
    var customerId:String = ""
    var otpVerify:String = ""

  
    
    var Values: [String: String] {
          return [

              "fcmRegId":fcmToken,
              "userId":customerId,
              "userTypeId":"1",
              "deviceTypeId":"2",
              "otpVerify":otpVerify
              
          ]
      }
    
  
    
}


struct PromoParams{
    
    
    var promoCode:String = ""
    var customerId:String = ""
    var orderTotal:String = ""

  
    
    
    var Values: [String: String] {
          return [

              "promoCode":promoCode,
              "customerId":customerId,
              "orderTotal":orderTotal
             
          ]
      }
    
  
    
}


struct SendSms{
    
    
    var mobile:String = ""
    
    
    var Values: [String: String] {
          return [

              "phone":mobile
              
          ]
      }
    
  
    
}




struct BrandProductParams {
    
     var groceryId:String
     var brand:String
     var customerId:String
     var limit:String
     var sortKey:String = ""
    
        
        var Values: [String: String] {
              return [
                
                "groceryId":groceryId,
                "brand":brand,
                "customeId":customerId,
                "page":limit,
                "sortKey":sortKey
                
                
                
              ]
          }
        

    
}



struct SubCatProductParams {
    
   
     var subCategoryId:String
     var customerId:String
     var limit:String
     var sortKey:String = ""
    
        
        var Values: [String: String] {
              return [
                
                "groceryId":"2",
                "subCategoryId":subCategoryId,
                "customeId":customerId,
                "page":limit,
                "sortKey":sortKey
                
                
                
              ]
          }
        

    
}


struct VersionParams {
    
    var version:String
  
    var Values: [String: String] {
                 return [
                   
                   "version":version,
                   "deviceType":"2",
                  
                 ]
             }
    
    
}


struct CategoryParam {
    
    var groupId:String
   
       var Values: [String: String] {
             return [
               
               "groupId":groupId
               
               
             ]
         }
       

}





struct FavParams{
    
    
    var customerId:String = ""
    var productId:String = ""
    
    
    var Values: [String: String] {
          return [

              "customerId":customerId,
              "groceryId":"2",
              "productId":productId
          ]
      }
    
}


struct WishListParams{
    
    
    var customerId:String = ""
    
    var Values: [String: String] {
          return [

              "customerId":customerId,
              "groceryId":"2"
          ]
      }
    
}

struct SignUpParameters {
    
    
    var firstname:String = ""
    var email_id:String = ""
    var password:String = ""
    var phone:String = ""
    
    
    var Values: [String: String] {
        return [

            "firstName":firstname,
            "lastName":"",
            "emailId":email_id,
            "password":password,
            "phoneNo":phone,
        ]
    }
  
}

struct LogOutParams {
    
    
    var customerId:String
    var fcmId:String
    
    var Values: [String: String] {
        return [

            "customerId":customerId,
            "fcmId":fcmId
            
        ]
    }
    
    
}



struct NearestParams{
    
    
    var lattitude:String = ""
    var longitude:String = ""
    
    
    var Values: [String: String] {
          return [
            
            "lattitude":lattitude,
            "longitude":longitude
            
          ]
      }
    
    
}


struct LocationParameter {
    
    var stateId:String = ""
    var cityId:String = ""
    var streetId:String = ""
    
    var Values: [String: String] {
          return [
            
            "stateId":stateId,
            "cityId":cityId,
            "streetId":streetId
            
          ]
      }
    
    
}



struct AddtoCartParam{
    
    var productId:String = ""
    var groceryId:String = ""
    var quantity:String = ""
    var weight:String = ""
    var price:String = ""
    var article:String = ""
    var customerId:String = ""
    var description:String = ""
    
    var Values: [String: String] {
        return [
          
          "productId":productId,
          "groceryId":groceryId,
          "quantity":quantity,
          "weight":weight,
          "price":price,
          "article":article,
          "customerId":customerId,
          "description":description
          
        ]
    }
    
    
}


struct BannerParameters {
    
    var groceryId:String
    
    var Values: [String: String] {
          return [
            
            "groceryId":groceryId
            
          ]
      }
    
}


struct CategoryParameters {
    
    var groceryId:String
    
    
    var Values: [String: String] {
          return [
            
            "groceryId":groceryId
            
          ]
      }
    
}



struct CartCheckOut{
    
    
    var paymentMethod:String
    var specialInstructions:String
    var addressId:String
    var customerId:String
    var timeSlotId:String
    var deliveryTypeId:String
    var promoCodeId:String
    var promoDiscount:String 
    
        
    var Values: [String: String] {
        
          return [
            
            "paymentMethod":paymentMethod,
            "specialInstruction":specialInstructions,
            "addressId":addressId,
            "groceryId":"2",
            "customerId":customerId,
            "timeSlotId":timeSlotId,
            "deliveryTypeId":deliveryTypeId,
            "promoCodeId":promoCodeId,
            "promoDiscount":promoDiscount
            
          ]
      }
    
    
}

struct CategoryWithCutomerId{
    
    
    var groceryId:String
    var customerId:String
    var sortKey:String = ""
    
    var Values: [String: String] {
          return [
            
            "groceryId":groceryId,
            "customerId":customerId,
            "sortKey":sortKey
            
          ]
      }
    
    
}

struct OfferProductParameters {
    
   
  
    var sortKey:String = ""
    var page:String = "1"
    var catId:String = ""
    
    
    var Values: [String: String] {
          return [
            
            "groceryId":"2",
            "sortKey":sortKey,
            "page":page,
            "subCategoryId":catId
           
          ]
      }
    
}

struct CampaignProductParameters {
    
   
  
    var sortKey:String = ""
    var page:String = "1"
    var catId:String = ""
    
    
    var Values: [String: String] {
          return [
            
            "groceryId":"2",
            "sortKey":sortKey,
            "page":page,
            "campaignId":catId
           
          ]
      }
    
}


struct BrandParameters {
    
   
  
    var sortKey:String = ""
    var page:String = "1"
    var brandId:String = ""
    
    
    var Values: [String: String] {
          return [
            
            "groceryId":"2",
            "sortKey":sortKey,
            "page":page,
            "brandId":brandId
           
          ]
      }
    
}


struct CancelProductParameters {
    
    var orderId:String
    var customerId:String
   
    
    var Values: [String: String] {
          return [
            
            "orderId":orderId,
            "customerId":customerId
           
          ]
      }
    
}




