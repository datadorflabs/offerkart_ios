//
//  Utils.swift
//  Nesto OMAN
//
//  Created by Sajin M on 03/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation
import UIKit

class Utils{
    
    static func orderStatusColor(type:String) -> UIColor{
        
        switch type {
        case "0":
            return UIColor.red
        case "1":
            return UIColor.gray
        case "2":
            return Colors.OKBlue
        case "3":
            return UIColor.orange
        case "4":
            return UIColor(red: 0/255, green: 168/255, blue: 89/255, alpha: 1)
        case "5":
            return UIColor.red
        case "6":
            return UIColor.blue
        default:
            return UIColor(red: 170/255, green: 179/255, blue: 187/255, alpha: 1)
        }
    }
    
    static func orderStatus(type:String) -> String{
        
        switch type {
        case "0":
            return "Cancelled"
        case "1":
            return "Ordered"
        case "2":
            return "Processing"
        case "3":
            return "Ready for delivery"
        case "4":
            return "Delivered"
        case "5":
            return "Cancelled"
        case "6":
            return "Out for delivery"
        default:
            return ""
        }
    }
    
    
    
    
}




struct Sort {
     let title: String
     var selected: Bool
 }
 
