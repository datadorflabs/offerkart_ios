//
//  BaseViewController.swift
//
//
//  Created by Sajin M on 08/05/2020.
//  Copyright © 2020 Netstager. All rights reserved.
//

import UIKit
import Alertift
import SVProgressHUD
import CRNotifications
import Toast_Swift


fileprivate struct CustomCRNotification: CRNotificationType {
    var textColor: UIColor
    var backgroundColor: UIColor
    var image: UIImage?
}

class BaseViewController: UIViewController, SlideMenuDelegate{
    
    let STProgress = SVProgressHUD.self
    let STAlert = Alertift.self
    var ApiRequest = ResponseHandler()
    
    
    
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.dismissKey()

        // Do any additional setup after loading the view.
    }
    
    func isConnected() -> Bool{
        
        
        let customNotification = CustomCRNotification(textColor: UIColor.white, backgroundColor: .darkGray, image: UIImage(named: "ic_internet"))
           
           if Reachability.isConnectedToNetwork(){
               
             return true
               
           }else{
               
               CRNotifications.showNotification(type: customNotification, title: noInternetTitle , message: noInternetMessage, dismissDelay: 10)
               
               return false
           }
           
           
           
           
       }
    
    
    
    func isLoggedIn() -> Bool{
        
         let token = Defaults.value(forKey: "userToken") as? String
        
        if token != nil{
            
            return true
            
        }else{
            
            return false
        }
        
        
    }
    
    
    func showToast(message:String){
        
        var style = ToastStyle()
        
        style.cornerRadius = 0
        style.horizontalPadding = 15
        
        self.view.makeToast(message, duration: 3.0, position: .bottom, style:style)
        
    }
    
    
    func showNotification(message:String){
        
        let customNotification = CustomCRNotification(textColor: UIColor.white, backgroundColor: .darkGray, image: UIImage(named: "ic_error"))
        
         CRNotifications.showNotification(type: customNotification, title: AppName , message: message, dismissDelay: 3)
        
        }
    
    func showNotificationAddProduct(message:String){
    
        let customNotification = CustomCRNotification(textColor: UIColor.white, backgroundColor: .darkGray, image: UIImage(named: "ic_cart"))
    
     CRNotifications.showNotification(type: customNotification, title: AppName , message: message, dismissDelay: 2)
    
    }
    
    func showAlert(message:String) {
        
        
        self.STAlert.alert(title:AppName , message: message )
        .action(.default("OK"))
        .show(on: self)
        
    }
    
    
    
    
   
    
    func showAlertCustomTitle(title:String,message:String) {
          
          
          self.STAlert.alert(title:title , message: message )
          .action(.default("OK"))
          .show(on: self)
          
      }
    

    
    func slideMenuItemSelectedAtIndex(_ index: Int32,_ title: String )  {
        
        
        
            let topViewController : UIViewController = self.navigationController!.topViewController!
            print("View Controller is : \(topViewController) \n", terminator: "")
            switch(index){
                
                
            case 0:
                
                self.openViewControllerBasedOnIdentifier("HomeViewController")
                
                
                break
           
            case 1:
                
                if title == "My Orders"{
                 
                     self.openViewControllerBasedOnIdentifier("MyOrdersViewController")
                    
                }else{
                    
                    self.openViewControllerBasedOnIdentifier("TCViewController")
                    
                    
                }
                
                

               break
        
            case 2:
                
                if title == "My Wishlist"{
                    
                     self.openViewControllerBasedOnIdentifier("WishlistViewController")
                }else{
                    

                    self.openViewControllerBasedOnIdentifier("AboutViewController")
                }
                
            case 3:
                
                if title == "Sign In"{
                                  
                                   self.openViewControllerBasedOnIdentifier("LoginViewController")
                                  
                }else{
                    
          
                        self.openViewControllerBasedOnIdentifier("TCViewController")
               
                }
                
                
            case 4:
                
                self.openViewControllerBasedOnIdentifier("AboutViewController")
                
                break
                
            case 5:
                
                if title == "Sign In"{
                    
                     self.openViewControllerBasedOnIdentifier("LoginViewController")
                    
                }
                else{
                   
                 logoutApp()

                    
                }
                
                
               
          
                
                break
                
//                defualts.set(nil, forKey:Token)
//                 self.openViewControllerBasedOnIdentifier(storyBoard:AppStoryboard.Login.instance ,"LoginViewController")
                
            default:
                print("default\n", terminator: "")
            }
        }
    
    
    
    func logoutApp(){

        var fcmStr = ""
        guard let customerId = Defaults.string(forKey: "customerId") else {
            return
        }

        if let fcm = Defaults.string(forKey: "fcmToken"){
            fcmStr = fcm
               }



        self.STProgress.show()

        let params = LogOutParams(customerId: customerId, fcmId: fcmStr).Values


               self.ApiRequest.logOut(withParameter:params) { (isSuccess,message) in

                   self.STProgress.dismiss()

                   if isSuccess {


                    Defaults.set(nil, forKey: "fcmToken")
                    Defaults.set(nil,forKey: "customerId")
                    Defaults.set("0", forKey:"badge")


                       let HomeScene = HomeViewController.instantiate(fromAppStoryboard: .Main)

                       if let navigator = self.navigationController {


                           navigator.pushViewController(HomeScene, animated: true)

                       }




                   }else{



                       self.showAlert(message: wentWrong)

                   }
               }




    }
    
    
    
    
        
    
    func openViewControllerBasedOnIdentifier(_ strIdentifier:String){
        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: strIdentifier)
        
        let topViewController : UIViewController = self.navigationController!.topViewController!
        
        if (topViewController.restorationIdentifier! == destViewController.restorationIdentifier!){
           
        } else {
            self.navigationController!.pushViewController(destViewController, animated: true)
        }
    }
    
    func addSlideMenuButton(){
        let btnShowMenu = UIButton(type: UIButton.ButtonType.system)
        btnShowMenu.setImage(self.defaultMenuImage(), for: UIControl.State())
        btnShowMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnShowMenu.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControl.Event.touchUpInside)
        let customBarItem = UIBarButtonItem(customView: btnShowMenu)
        self.navigationItem.leftBarButtonItem = customBarItem;
    }
    
    func defaultMenuImage() -> UIImage {
        var defaultMenuImage = UIImage()
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 30, height: 22), false, 0.0)
        
        UIColor.black.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 3, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 10, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 17, width: 30, height: 1)).fill()
        
        UIColor.white.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 4, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 11,  width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 18, width: 30, height: 1)).fill()
        
        defaultMenuImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        
        return defaultMenuImage;
    }
    
    @objc func onSlideMenuButtonPressed(_ sender : UIButton){
        if (sender.tag == 10)
        {
            // To Hide Menu If it already there
            self.slideMenuItemSelectedAtIndex(-1, "");
            
            sender.tag = 0;
            
            let viewMenuBack : UIView = view.subviews.last!
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                viewMenuBack.removeFromSuperview()
            })
            
            return
        }
        
        sender.isEnabled = false
        sender.tag = 10
        
        let menuVC : MenuViewController = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.addChild(menuVC)
        menuVC.view.layoutIfNeeded()
        
        
        menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
        }, completion:nil)
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

